//---------------------------------------------------------------------------------------
// Copyright (c) 2001-2020 by PDFTron Systems Inc. All Rights Reserved.
// Consult legal.txt regarding legal and license information.
//---------------------------------------------------------------------------------------

#import <Tools/PTTool.h>

NS_ASSUME_NONNULL_BEGIN

/**
 * Crops image stamp annotations, created by the `PTImageStampCreate` tool.
 */
@interface PTImageCropTool : PTTool

@end

NS_ASSUME_NONNULL_END
