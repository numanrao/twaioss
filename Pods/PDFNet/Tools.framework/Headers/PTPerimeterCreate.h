//---------------------------------------------------------------------------------------
// Copyright (c) 2001-2020 by PDFTron Systems Inc. All Rights Reserved.
// Consult legal.txt regarding legal and license information.
//---------------------------------------------------------------------------------------

#import <Tools/PTPolylineCreate.h>

NS_ASSUME_NONNULL_BEGIN

/**
 * Creates perimeter annotations.
 */
@interface PTPerimeterCreate : PTPolylineCreate

@end

NS_ASSUME_NONNULL_END
