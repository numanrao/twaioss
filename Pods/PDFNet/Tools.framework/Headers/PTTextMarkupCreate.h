//---------------------------------------------------------------------------------------
// Copyright (c) 2001-2020 by PDFTron Systems Inc. All Rights Reserved.
// Consult legal.txt regarding legal and license information.
//---------------------------------------------------------------------------------------

#import <Tools/PTTool.h>

NS_ASSUME_NONNULL_BEGIN

/**
 * Abstract base class for creating text markup annotations, such as highlights,
 * underlines, strikeouts and squigglies.
 */
@interface PTTextMarkupCreate : PTTool

@end

NS_ASSUME_NONNULL_END
