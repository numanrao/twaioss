//---------------------------------------------------------------------------------------
// Copyright (c) 2001-2020 by PDFTron Systems Inc. All Rights Reserved.
// Consult legal.txt regarding legal and license information.
//---------------------------------------------------------------------------------------

#import <Tools/PTLineCreate.h>

NS_ASSUME_NONNULL_BEGIN

/**
 * Creates ruler annotations.
 */
@interface PTRulerCreate : PTLineCreate

@end

NS_ASSUME_NONNULL_END
