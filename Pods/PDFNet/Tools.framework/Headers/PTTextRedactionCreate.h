//---------------------------------------------------------------------------------------
// Copyright (c) 2001-2020 by PDFTron Systems Inc. All Rights Reserved.
// Consult legal.txt regarding legal and license information.
//---------------------------------------------------------------------------------------

#import <Tools/PTTextMarkupCreate.h>

NS_ASSUME_NONNULL_BEGIN

/**
 * Creates text redaction annotations.
 */
@interface PTTextRedactionCreate : PTTextMarkupCreate

@end

NS_ASSUME_NONNULL_END
