//
//  ServerHelper.swift
//  TWA
//
//  Created by MascSoft on 7/7/19.
//  Copyright © 2019 Telecare. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

enum ErrorStatusCode {
    case defaultFailure
    case networkLost
    case internalServer(Int)
}

struct ApiEndpoint {
    
    // User Management
    static let LOGIN = "/user/login"
    static let SIGNUP = "/user/signup"
    static let USER_UPDATE = "/user/update"
    static let NEARBY_USERS = "/user/nearby"
    
    static let USER_CHECK_PASSWORD = "/user/checkPassword"
    static let USER_CHECK_SECURITY = "/user/checkSecurity"
    
    
    // Contact Management
    static let CONTACT_LIST = "/contact/list"
    static let CONTACT_REQUEST = "/contact/request"
    static let CONTACT_ACCEPT = "/contact/accept"
    static let CONTACT_DECLINE = "/contact/decline"
    static let CONTACT_DELETE = "/contact/delete"
    
    // Communication
    static let CONFERENCE_REQUEST = "/conference/request"
    static let CONFERENCE_ACCEPT = "/conference/accept"
    static let CONFERENCE_DECLINE = "/conference/decline"
    
    static let URGENCY_ALERT_REQUEST = "/urgency/request"
    static let URGENCY_ALERT_ACCEPT = "/urgency/accept"
    
    static let SEND_PHOTO_ID = "/communication/sendPhotoID"
    static let ISSUE_TICKET = "/communication/issueTicket"
    static let ISSUE_SUMMON = "/communication/issueSummon"
    
    // Recordings Management
    static let RECORDING_START = "/recording/start"
    static let RECORDING_STOP = "/recording/stop"
    static let RECORDING_USERS = "/recording/users"
    static let RECORDING_GET_ALL = "/recording/myAllRecordings"
    static let RECORDING_RECORDINGS = "/recording/userRecordings"
    static let RECORDING_DETAILS = "/recording/details"
    static let RECORDING_REMOVE_ALL = "/recording/removeAll"
    static let RECORDING_REMOVE_USER = "/recording/removeUser"
    static let RECORDING_REMOVE_USER_RECORDING = "/recording/removeUserRecording"
    static let RECORDING_REMOVE_RECORDING = "/recording/removeRecording"
    
    static let RECORDING_SAVE_EVENT = "/recording/saveReceiveEvent"
    
    static let GROUP_CHAT_HISTORY = "/chat/groupChatHistory"
    
}

class ServerHelper: NSObject {

    static let shared = ServerHelper()

    private let ApiBaseURL = AppInfo.ApiServerURL + "/api"
    
    typealias ErrorCallback = (String, ErrorStatusCode) -> Void
    typealias SuccessCallback = (JSON) -> ()
    

    func callPostAPI(_ strAPI:String, parameters:Parameters?, success: SuccessCallback?, failure: ErrorCallback?){
        self.callAPI(strAPI, method: .post, parameters: parameters, success: success, failure: failure)
    }
    
    func callGetAPI(_ strAPI:String, parameters:Parameters?, success: SuccessCallback?, failure: ErrorCallback?){
        self.callAPI(strAPI, method: .get, parameters: parameters, success: success, failure: failure)
    }
    
    func callPostAPIWithRecording(_ strAPI:String, parameters:Parameters?, success: SuccessCallback?, failure: ErrorCallback?){
        var params: [String: Any] = [:]
        if let temp = parameters {
            params = temp
        }
        
        if let recordingId = AppData.shared.recordingId, recordingId.isEmpty == false {
            params["recordingId"] = recordingId
        }
        
        self.callAPI(strAPI, method: .post, parameters: params, success: success, failure: failure)
    }
    
    private func getRequestHeader() -> HTTPHeaders {
        
        var headers: HTTPHeaders = [:]
        headers["Content-Type"] = "application/json"
        headers["Accept"] = "application/json"
        
        if let strToken = AppData.shared.userToken {
            let authStr = "Bearer " + strToken
            headers["Authorization"] = authStr
        }
        
        return headers
    }
    
    private func callAPI(_ strAPI:String, method: HTTPMethod, parameters:Parameters?, success: SuccessCallback?, failure:  ErrorCallback?){
        
        if NetworkReachabilityManager()?.isReachable == false {
            failure?(ValidationMessage.NETWORK_OFFLINE, .networkLost)
            return
        }
        
        print("Request API: ", strAPI)
        
        let requestURL = ApiBaseURL + strAPI;
        
        let headers: HTTPHeaders = getRequestHeader()
        
        AF.request(requestURL, method: method, parameters: parameters, encoding: JSONEncoding.default , headers: headers)
            .responseJSON{ response in
                print("Response is: ",response)
            switch response.result{
            case let .success(value):

                if let resValue = value as? NSDictionary{
                    let json = JSON(resValue)
                    print("Response Value: \(json)")

                    let data = json["data"]
                    if response.response?.statusCode == 200 {
                        success?(data)
                        return;
                    } else if let msg = json["message"].string {
                        failure?(msg, .internalServer(response.response!.statusCode))
                        return;
                    }
                }
                failure?("Server Error", .defaultFailure)
            case let .failure(error):
                print("Response Failed")
                if response.response?.statusCode == 401 { // invalid Token
                    AppData.shared.logout()
                    return
                }
                
                failure?(error.localizedDescription, ErrorStatusCode.defaultFailure)
                //failure?("Server Error", .defaultFailure)
            }
        }
    }
    
    func callAPIForUploadImage(apiName:String, image:UIImage?, imageName: String, parameters: [String: String], success: SuccessCallback?, failure:  ErrorCallback?) {
        
        if NetworkReachabilityManager()?.isReachable == false {
            failure?(ValidationMessage.NETWORK_OFFLINE, .networkLost)
            return
        }
        
        let method: HTTPMethod = .post
        let url = ApiBaseURL + apiName;
        let headers: HTTPHeaders = getRequestHeader()
        
        
        
        let webURLRequest = try! URLRequest(url: url, method: method, headers: headers)
        
        AF.upload(multipartFormData: {  MultipartFormData in
            
            for (k, v) in parameters {
                MultipartFormData.append(v.data(using: String.Encoding.utf8)!, withName: k)
            }

            let imageData :Data = (image?.jpegData(compressionQuality: 0.8))!

                       MultipartFormData.append(imageData, withName: imageName , fileName: "image.jpg" , mimeType: "image/jpg")
        }, to: url)
            .responseJSON { response in
                print("Response is: ",response)
            switch response.result{
            case let .success(value):

                if let resValue = value as? NSDictionary{
                    let json = JSON(resValue)
                    print("Response Value: \(json)")

                    let data = json["data"]
                    if response.response?.statusCode == 200 {
                        success?(data)
                        return;
                    } else if let msg = json["message"].string {
                        failure?(msg, .internalServer(response.response!.statusCode))
                        return;
                    }
                }
                failure?("Server Error", .defaultFailure)
            case let .failure(error):
                print("Response Failed")
                if response.response?.statusCode == 401 { // invalid Token
                    AppData.shared.logout()
                    return
                }
                
                failure?(error.localizedDescription, ErrorStatusCode.defaultFailure)
                //failure?("Server Error", .defaultFailure)
            }
        }
        /*
        AF.upload(multipartFormData: { (multipartFormData) in
            
            for (k, v) in parameters {
                multipartFormData.append(v.data(using: String.Encoding.utf8)!, withName: k)
            }
            
            if let tempData = imageData {
                multipartFormData.append(tempData, withName: imageName, fileName: "image.jpg", mimeType: "image/jpg")
            }
        }, with: webURLRequest as URLRequestConvertible, encodingCompletion: { (result) in
            
            switch result {
            case .success(let upload, _,  _):
                
//                upload.responseString(completionHandler: { (str) in
//                    print(str);
//                    failure?("Server Error", .defaultFailure)
//                })
//                return;
                upload.responseJSON { response in
                    
                    if let resValue =  response.result.value {
                        let json = JSON(resValue)
                        print("Response Value: \(json)")
                        
                        let data = json["data"]
                        if response.response?.statusCode == 200 { // , data != JSON.null
                            success?(data)
                            return
                        } else if let msg = json["message"].string {
                            failure?(msg, .internalServer(response.response!.statusCode))
                                return
                        }
                    }
                    
                    failure?("Server Error", .defaultFailure)
                }
            case .failure(let error):
                failure?(error.localizedDescription, ErrorStatusCode.defaultFailure)
                //failure?("Server Error", .defaultFailure)
            }
        })*/
    }
    
    func callAPIForUpload(apiName:String, files:[String: URL], parameters: [String: String], success: SuccessCallback?, failure:  ErrorCallback?) {
        
        if NetworkReachabilityManager()?.isReachable == false {
            failure?(ValidationMessage.NETWORK_OFFLINE, .networkLost)
            return
        }
        
        let method: HTTPMethod = .post
        let url = ApiBaseURL + apiName;
        let headers: HTTPHeaders = getRequestHeader()
        var params = parameters
        
        
        let webURLRequest = try! URLRequest(url: url, method: method, headers: headers)
        AF.upload(multipartFormData: {  MultipartFormData in
            
            for (k, v) in files {
                MultipartFormData.append(v, withName: k)
            }
            
            for (k, v) in params {
                MultipartFormData.append(v.data(using: String.Encoding.utf8)!, withName: k)
            }
        }, to: url)
            .response { resp in
            print(resp)
            }
        /*
        AF.upload(multipartFormData: { (multipartFormData) in
            for (k, v) in files {
                multipartFormData.append(v, withName: k)
            }
            
            for (k, v) in params {
                multipartFormData.append(v.data(using: String.Encoding.utf8)!, withName: k)
            }
            
        }, with: webURLRequest as URLRequestConvertible, encodingCompletion: { (result) in
            
            switch result {
            case .success(let upload, _,  _):
                
                upload.responseJSON { response in
                    
                    let json = JSON(response.result.value!)
                    print("Response Value: \(json)")
                    
                    if response.response?.statusCode == 200 {
                        success?(json)
                    } else {
                        if let msg = json["message"].string {
                            failure?(msg, .internalServer(response.response!.statusCode))
                        }else{
                            failure?("Server Error", .defaultFailure)
                        }
                    }
                }
            case .failure(let error):
                failure?(error.localizedDescription, ErrorStatusCode.defaultFailure)
                //failure?("Server Error", .defaultFailure)
            }
        })*/
    }
}
