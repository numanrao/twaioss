//
//  User.swift
//  TWA
//
//  Created by MascSoft on 7/7/19.
//  Copyright © 2019 Telecare. All rights reserved.
//

import UIKit
import SwiftyJSON

class User: NSObject {

    var userId: String = ""
    var username: String = ""
    var email: String = ""
    var name: String = ""
    var gender: Int = 0 // 0- Male,  1- Female
    var photo: String = ""
    var phoneNumber: String = ""
    var precinct: String = ""
    var badgeNumber: String = ""
    var rank: String = ""
    
    var role: String = ""
    var security: [String: String] = [:]
    
    var longitude: Double = 0
    var latitude: Double = 0
    var distance: Float = 0
    
    var autoRecording: Bool = true
    var greeting: String = ""
    
    var isContacted: Bool = false
    var isProfessional: Bool = false
    
    init(_ dict: [String: Any]) {
        super.init()
        
        let json = JSON(dict)
        initObject(json)
    }
    
    init(_ json: JSON) {
        super.init()
        
        initObject(json)
    }
    
    private func initObject(_ json: JSON) {
        
        userId = json["id"].stringValue
        username = json["username"].stringValue
        email = json["email"].stringValue
        name = json["name"].stringValue
        gender = json["gender"].intValue
        photo = json["photo"].stringValue
        phoneNumber = json["phoneNumber"].stringValue
        precinct = json["precinct"].stringValue
        
        rank = json["rank"].stringValue
        badgeNumber = json["badgeNumber"].stringValue
        
        role = json["role"].stringValue
        
        if let temp = json["security"].dictionary {
            for (key, value) in temp {
                security[key] = value.stringValue
            }
        }
        
        if let temp = json["location"].array, temp.count == 2 {
            longitude = temp[0].doubleValue
            latitude = temp[1].doubleValue
        }
        
        distance = json["distance"].floatValue
        
        greeting = json["greeting"].stringValue
        autoRecording = json["autoRecording"].boolValue
        
        isContacted = json["isContacted"].boolValue
        
        isProfessional = (role == "professional") ? true : false

    }
    
    func genderImage() -> UIImage? {
        if gender == 1 {
            return AppImages.genderFemale
        }
        return AppImages.genderMale
    }
    
    func toJSON() -> [String : Any] {
    
        var json: [String : Any] = [:]
        
        json["id"] = userId
        json["username"] = username
        json["name"] = name
        json["email"] = email
        json["phoneNumber"] = phoneNumber
        json["gender"] = gender
        json["photo"] = photo
        json["rank"] = rank
        json["badgeNumber"] = badgeNumber
        json["role"] = role
        json["security"] = security
        json["location"] = [longitude, latitude]
        json["greeting"] = greeting
        json["autoRecording"] = autoRecording
        json["precinct"] = precinct
        
        return json
    }
    
    func nameInitials() -> String {
        
        let temp = name.split(separator: " ", maxSplits: 5, omittingEmptySubsequences: true)
        if temp.count == 1 {
            let firstname = temp[0]
            let initial1 = (firstname.count != 0) ? firstname.prefix(1) : ""
            return String(initial1)
        }else if temp.count >= 2 {
           
            let firstname = temp[0]
            let lastname = temp[1]
            let initial1 = (firstname.count != 0) ? firstname.prefix(1) : ""
            let initial2 = (lastname.count != 0) ? lastname.prefix(1) : ""
            
            return "\(initial1)\(initial2)"
        }
        
        return ""
    }
}
