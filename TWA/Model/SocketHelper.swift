//
//  SocketManager.swift
//  TWA
//
//  Created by MascSoft on 7/7/19.
//  Copyright © 2019 Telecare. All rights reserved.
//

import UIKit
import SocketIO
import SwiftyJSON

enum UserStatus:String
{
    case online, offline
}

struct SocketApiEndpoint {
    //Device
    static let AddDevice = "/device/add"
    static let UpdateDeviceStatus = "/device/update"
    
    // Chat
    static let SendMessage = "/chat/sendMessage"
    static let SendGroupMessage = "/chat/sendGroupMessage"
    static let UserTyping = "/chat/userTyping"
    static let ChatHistory = "/chat/history"
    static let JoinGroupChat = "/chat/joinGroupChat"
    static let LeaveGroupChat = "/chat/leaveGroupChat"
}


@objc protocol SocketHelperDelegate : NSObjectProtocol
{
    @objc optional func receivePrivateMessage(_ data: NSDictionary)
    @objc optional func receiveTypingSocket(_ data: NSDictionary)
    @objc optional func receiveUserStatusUpdateSocket(_ data: NSDictionary)
    @objc optional func receiveOnlineUserListSocket(_ data: NSDictionary)
    @objc optional func receiveGroupMessage(_ data: NSDictionary)
    
    @objc optional func receiveContactRequestSocket(_ data: NSDictionary)
    @objc optional func receiveContactAcceptedSocket(_ data: NSDictionary)
    @objc optional func receiveContactDeclinedSocket(_ data: NSDictionary)
    
    @objc optional func receiveConferenceRequestSocket(_ data: NSDictionary)
    @objc optional func receiveConferenceAcceptedSocket(_ data: NSDictionary)
    @objc optional func receiveConferenceDeclinedSocket(_ data: NSDictionary)
    
    @objc optional func receiveUrgencyRequestSocket(_ data: NSDictionary)
    @objc optional func receiveUrgencyAcceptedSocket(_ data: NSDictionary)
    
    @objc optional func receivePhotoIDSocket(_ data: NSDictionary)
    @objc optional func receiveIssueTicketSocket(_ data: NSDictionary)
    @objc optional func receiveIssueSummonSocket(_ data: NSDictionary)
}


class SocketHelper: NSObject
{
    static let shared = SocketHelper()
    
    typealias SocketJSONCallback = (JSON?) -> ()
    
    var delegate:SocketHelperDelegate?
    
    var options:SocketIOClientConfiguration!
    var socket: SocketIOClient!
    var manager: SocketManager!
    
    private let serverurl = AppInfo.ApiServerURL
    private let apiPrefix = "/api"

    override init()
    {
        super.init()
    }

    func establishConnection()
    {
        if self.closeConnection()
        {
            if options != nil
            {
                options = nil;
            }
            if manager != nil
            {
                _ = self.closeConnection()
                manager = nil
            }
            if socket != nil
            {
                socket = nil
            }
            
            options = [] as SocketIOClientConfiguration
            options.insert(.connectParams(["__sails_io_sdk_version":"1.0.2"]))
            options.insert(.reconnectAttempts(1000))
            options.insert(.reconnectWait(3))
            options.insert(.log(false))
            options.insert(.compress)
            options.insert(.forceWebsockets(true))
            
            
            manager = SocketManager(socketURL: URL(string: serverurl)!, config: options)
            socket = manager.defaultSocket
            
            self.socket.on("disconnect"){data, ack in
                print("Discounnect")
            }
            self.socket.on("reconnect"){data, ack in
                print("reconnect")
            }
            self.socket.on("reconnectAttempt"){data, ack in
                print("reconnectAttempt")
            }
            self.socket.on("error"){data, ack in
                print("error")
            }
            self.socket.on(clientEvent:.connect) { (data, ack) in
                print("socket connected")
                if AppData.shared.isLogin() {
                    AppData.shared.registerDevice()
                }
            }
            
            self.addListnerEvents()
            
            socket.connect()
        }
    }
    
    func establishConnectionReturn(_ completionHandler: @escaping (_ returndata:AnyObject) -> Void) {
    }
    
    //# MARK: - Ride listener Methods
    //# MARK: -
    
    func addListnerEvents()
    {
        self.socket.on("device") { (data, ack) in
            print("User Event Listener Response: \(data)\(ack)")
            
            guard let resObj = data[0] as? NSDictionary, let actions = resObj["actions"] as? String else { return }
            
            if actions == "message"
            {
                //store message to global dict
                self.delegate?.receivePrivateMessage?(resObj)
            }
            else if actions == "typing"
            {
                self.delegate?.receiveTypingSocket?(resObj)
            }
            else if actions == "userstatusupdate"
            {
                self.delegate?.receiveUserStatusUpdateSocket?(resObj)
            }
            else if actions == "onlineuserlist"
            {
                self.delegate?.receiveOnlineUserListSocket?(resObj)
            }
            else if actions == "contactRequest"
            {
                self.delegate?.receiveContactRequestSocket?(resObj)
            }
            else if actions == "contactAccept"
            {
                self.delegate?.receiveContactAcceptedSocket?(resObj)
            }
            else if actions == "contactDecline"
            {
                self.delegate?.receiveContactDeclinedSocket?(resObj)
            }
            else if actions == "conferenceRequest"
            {
                self.delegate?.receiveConferenceRequestSocket?(resObj)
            }
            else if actions == "conferenceAccept"
            {
                self.delegate?.receiveConferenceAcceptedSocket?(resObj)
            }
            else if actions == "conferenceDecline"
            {
                self.delegate?.receiveConferenceDeclinedSocket?(resObj)
            }
            else if actions == "urgencyRequest"
            {
                self.delegate?.receiveUrgencyRequestSocket?(resObj)
            }
            else if actions == "urgencyAccept"
            {
                self.delegate?.receiveUrgencyAcceptedSocket?(resObj)
            }
            else if actions == "receivePhotoID"
            {
                self.delegate?.receivePhotoIDSocket?(resObj)
            }
            else if actions == "receiveTicket"
            {
                self.delegate?.receiveIssueTicketSocket?(resObj)
            }
            else if actions == "receiveSummon"
            {
                self.delegate?.receiveIssueSummonSocket?(resObj)
            }
        }
        
        self.socket.on("conference") { (data, ack) in
            print("Conference Event Listener Response: \(data)\(ack)")
            
            guard let resObj = data[0] as? NSDictionary, let actions = resObj["actions"] as? String else { return }
            
            if actions == "group_message"
            {
                self.delegate?.receiveGroupMessage?(resObj)
            }
        }
    }
    
    func closeConnection()->Bool
    {
        if self.socket != nil
        {
            print("socket disconnect")
            self.socket.off("user")
            socket.disconnect()
            self.socket.removeAllHandlers()
        }
        return true
    }
    
    // Emit Modes for SailsJS socket API call
    func emitPost() -> Void
    {
        let dictParameters = [ "url": String(serverurl + "/v1/socket-testPost")]
        self.socket.emitWithAck("post", with: [dictParameters]).timingOut(after: 10) {data in
            
            print(data);
        }
    }
    
    func emitGet() -> Void
    {
        let dictParameters = [ "url": String( serverurl + "/v1/socket-testGet")]
        self.socket.emitWithAck("post", with: [dictParameters]).timingOut(after: 10) {data in
            
            print(data);
        }
    }
    
    func emitPut() -> Void
    {
        let dictParameters = [ "url": String( serverurl + "/v1/socket-testPut")]
        self.socket.emitWithAck("post", with: [dictParameters]).timingOut(after: 10) {data in
            
            print(data);
        }
    }
    
    private func getRequestHeader() -> [String: Any] {
        
        var headers: [String: Any] = [:]
        if let strToken = AppData.shared.userToken {
            let authStr = "Bearer " + strToken
            headers["Authorization"] = authStr
        }
        
        return headers
    }
    
    private func callAPI(_ method: String, url: String, data: [String: Any], completion: SocketJSONCallback?) {
        
        var params: [String: Any] = [:]
        
        params["url"] = apiPrefix + url
        params["data"] = data
        params["headers"] = getRequestHeader()
        
        self.socket.emitWithAck(method, with: [params]).timingOut(after: 10) { (data) in
            if data.isEmpty == false, let res = data.first {
                let resObj = JSON(res)
                let json = resObj["body"]
                let statusCode = resObj["statusCode"].intValue
                
                print("Socket Response", json)
                completion?(json)
                
                if statusCode == 401 { // Invalid Token
                    //AppData.shared.logout()
                }
            }else{
                completion?(nil)
            }
        }
    }
    
    
    /// Public Functions
    func callPostAPI(_ url: String, data: [String: Any], completion: SocketJSONCallback?){
        self.callAPI("post", url: url, data: data, completion: completion)
    }
    
    func callGetAPI(_ url: String, data: [String: Any], completion: SocketJSONCallback?){
        self.callAPI("get", url: url, data: data, completion: completion)
    }
    
    // MARK: Device
    func addDevice(_ deviceToken: String, completion: SocketJSONCallback?) {
        let data = ["deviceType": "ios",
                    "deviceToken": deviceToken
        ]
        
        print("Adding Device")
        callPostAPI(SocketApiEndpoint.AddDevice, data: data, completion: completion)
    }
    
    func updateDeviceStatus(_ deviceId: String, deviceToken: String, isOnline: Bool, completion: SocketJSONCallback?) {
        let data = ["deviceId": deviceId,
                    "deviceToken": deviceToken,
                    "userstatus": isOnline ? "online" : "offline"
        ]
        
        print("Updating Device's User status")
        callPostAPI(SocketApiEndpoint.UpdateDeviceStatus, data: data, completion: completion)
    }
    
    // MARK: - Chatting
    
    func sendMessage(_ message: String, receiver: String, groupId: String, completion: @escaping (DBMessage?) -> Void ) {
        let data = ["receiver": receiver,
                    "groupId": groupId,
                    "message": message
        ]
        
        print("Send Private Message")
        callPostAPI(SocketApiEndpoint.SendMessage, data: data, completion: { (json) in
            if let messageObj = json?["data"].dictionaryObject {
                completion(DBMessage(messageObj))
            }else {
                completion(nil)
            }
        })
    }
    
    func sendGroupMessage(_ message: String, group: String, completion: @escaping (DBMessage?) -> Void) {
        let data = ["groupId": group,
                    "message": message
        ]
        
        print("Send Group Chat Message")
        callPostAPI(SocketApiEndpoint.SendGroupMessage, data: data, completion: { (json) in
            if let messageObj = json?["data"].dictionaryObject {
                completion(DBMessage(messageObj))
            }else {
                completion(nil)
            }
        })
    }
    
    func sendUserTyping(receiver: String, completion: SocketJSONCallback?){
        let data = ["receiver": receiver]
        
        print("Send User Typing Message")
        callPostAPI(SocketApiEndpoint.SendMessage, data: data, completion: completion)
    }
    
    func getPrivateChatHistory(receiver: String, messageDate: Int64?, completion: @escaping ([DBMessage]?) -> Void ) {
        var data: [String: Any] = ["receiver": receiver]
        
        if let date = messageDate {
            data["messageDate"] = date
        }
        
        print("Getting Private Chat history")
        callPostAPI(SocketApiEndpoint.ChatHistory, data: data, completion: { (json) in
            if let resultData = json?["data"].array {
                var messages: [DBMessage] = []
                for messageObj in resultData {
                    messages.append(DBMessage(messageObj))
                }
                completion(messages)
            }else{
                completion(nil)
            }
        } )
    }
    
    func getGroupChatHistory(groupId: String, messageDate: Int64?, completion: @escaping ([DBMessage]?) -> Void ) {
        var data: [String: Any] = ["groupId": groupId]
        
        if let date = messageDate {
            data["messageDate"] = date
        }
        
        print("Getting Group Chat history")
        callPostAPI(SocketApiEndpoint.ChatHistory, data: data, completion: { (json) in
            if let resultData = json?["data"].array {
                var messages: [DBMessage] = []
                for messageObj in resultData {
                    messages.append(DBMessage(messageObj))
                }
                completion(messages)
            }else{
                completion(nil)
            }
        } )
    }
    
    func joinGroupChat(groupId: String, completion: SocketJSONCallback?){
        let data = ["groupId": groupId]
        
        print("Send Group Chat Joining")
        callPostAPI(SocketApiEndpoint.JoinGroupChat, data: data, completion: completion)
    }
    
    func leaveGroupChat(groupId: String, completion: SocketJSONCallback?){
        let data = ["groupId": groupId]
        
        print("Send Group Chat Leave")
        callPostAPI(SocketApiEndpoint.LeaveGroupChat, data: data, completion: completion)
    }
}
