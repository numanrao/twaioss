//
//  Recording.swift
//  TWA
//
//  Created by MascSoft on 8/16/19.
//  Copyright © 2019 Telecare. All rights reserved.
//

import UIKit
import SwiftyJSON

class Recording: NSObject {
    
    var recordingId: String = ""
    var isByUser: Bool = false
    
    var videoURL: URL?
    
    var longitude: Double = 0
    var latitude: Double = 0
    
    var createdAt: String = ""
    
    init(_ dict: [String: Any]) {
        super.init()
        
        let json = JSON(dict)
        initObject(json)
    }
    
    init(_ json: JSON) {
        super.init()
        
        initObject(json)
    }
    
    private func initObject(_ json: JSON) {
        
        recordingId = json["id"].stringValue
        isByUser = json["isbyuser"].boolValue
        
        if let temp = json["location"].array, temp.count == 2 {
            longitude = temp[0].doubleValue
            latitude = temp[1].doubleValue
        }
        
        let createdTime = json["createdAt"].int64Value
        createdAt = Helper.timestampToDateString(timestamp: createdTime)
        
        let videoUrl = json["video"].stringValue
        videoURL = URL(string: videoUrl)
    }
}
