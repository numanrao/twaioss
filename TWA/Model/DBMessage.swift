//
//  DBMessage.swift
//  TWA
//
//  Created by MascSoft on 8/9/19.
//  Copyright © 2019 Telecare. All rights reserved.
//

import UIKit
import SwiftyJSON

class DBMessage: NSObject {
    
    var messageId: String = ""
    
    var senderId: String = ""
    var sender: User?
    
    var receiverId: String = ""
    var receiver: User?
    
    var groupId: String = ""
    
    var type: String = "" // text, image, audio, video,
    var text: String = "" // Message content
    
    var pictureWidth: Int = 0
    var pictureHeight: Int = 0
    
    var status: String = ""
    
    var createdAt: Int64 = 0
    var updatedAt: Int64 = 0
    
    init(_ dict: [String: Any]) {
        super.init()
        
        let json = JSON(dict)
        initObject(json)
    }
    
    init(_ json: JSON) {
        super.init()
        
        initObject(json)
    }
    
    private func initObject(_ json: JSON) {
        
        messageId = json["id"].stringValue
        
        groupId = json["groupid"].stringValue
        
        let fromUser = json["fromuser"]
        
        if  fromUser.type == .dictionary {
            self.sender = User(fromUser);
            self.senderId = self.sender?.userId ?? ""
        }else if fromUser.type == .string {
            self.senderId = fromUser.stringValue
        }
        
        let toUser = json["touser"]
        
        if  toUser.type == .dictionary {
            self.receiver = User(toUser);
            self.receiverId = self.receiver?.userId ?? ""
        }else if toUser.type == .string {
            self.receiverId = toUser.stringValue
        }
        
        type = json["mediatype"].stringValue
        text = json["mediadata"].stringValue
        
        pictureWidth = json["imgwidth"].intValue
        pictureHeight = json["imgheight"].intValue
        
        createdAt = json["createdAt"].int64Value
        updatedAt = json["updatedAt"].int64Value
    }
    
    
    func currentUserIsSender() -> Bool {

        if let appUser = AppData.shared.currentUser {
            if  senderId == appUser.userId {
                return true
            }
        }
        return false
    }
}
