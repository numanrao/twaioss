//
//  RecordingItem.swift
//  TWA
//
//  Created by MascSoft on 8/16/19.
//  Copyright © 2019 Telecare. All rights reserved.
//

import UIKit
import SwiftyJSON

class RecordingItem: NSObject {

    var itemId: String = ""
    var userId: String = ""
    
    var isSender: Bool = false
    var content: String = ""
    var createdAt: Int64 = 0
    var strCreatedAt: String = ""
    
    init(_ dict: [String: Any]) {
        super.init()
        
        let json = JSON(dict)
        initObject(json)
    }
    
    init(_ json: JSON) {
        super.init()
        
        initObject(json)
    }
    
    private func initObject(_ json: JSON) {
        
        itemId = json["id"].stringValue
        userId = json["userId"].stringValue
        
        isSender = json["isSender"].boolValue
        content = json["content"].stringValue
        
        createdAt = json["createdAt"].int64Value
        strCreatedAt = Helper.timestampToDateString(timestamp: createdAt)
        
    }
}
