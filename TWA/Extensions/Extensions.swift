//
//  Extensions.swift
//  TWA
//
//  Created by MascSoft on 7/1/19.
//  Copyright © 2019 Telecare. All rights reserved.
//

import Foundation
import UIKit
import SafariServices
import SVProgressHUD

extension UIViewController {
    
    func showPromptAlert(_ title:String?, message:String?, okTitle:String? = "OK" ) {
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: okTitle, style: .cancel, handler: nil)
        alertController.addAction(okAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    func pushVC(withIdentifier identifier: String, animated: Bool) {
        if let vc = self.storyboard?.instantiateViewController(withIdentifier: identifier) {
            self.navigationController?.pushViewController(vc, animated: animated)
        }
    }
    
    func pushVC(_ vc: UIViewController, animated: Bool) {
        self.navigationController?.pushViewController(vc, animated: animated)
    }
    
    func popVC(_ animated: Bool){
        self.navigationController?.popViewController(animated: animated)
    }
    
    func presentWebPage(_ link:String){
        
        if let url = URL(string: link) {
            let controller = SFSafariViewController(url: url, entersReaderIfAvailable: true)
            self.present(controller, animated: true, completion: nil)
        }
    }
    
    // MBProgressHUD
    
    func showProgressHUD(){
        SVProgressHUD.show()
        //MBProgressHUD.showAdded(to: self.view, withText: "", animated: true)
    }
    
    func hideProgressHUD(){
        SVProgressHUD.dismiss()
        //MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
    }
}

extension UITableView{
    
    func registerCell(forIdentifier cellIdentifier:String){
        register(UINib(nibName: cellIdentifier, bundle: nil), forCellReuseIdentifier: cellIdentifier)
    }
    
    func showBackgroundViewWhenNil(msg:String?) {
        if let showMessage = msg {
            let lblMsg = UILabel(frame: CGRect(x: 0, y:0, width: self.bounds.width, height: self.bounds.height))
            lblMsg.text = showMessage
            lblMsg.textAlignment = .center
            lblMsg.textColor = UIColor.gray
            lblMsg.font = UIFont.systemFont(ofSize: 14.0)
            lblMsg.sizeToFit()
            self.backgroundView = lblMsg
        } else {
            self.backgroundView = nil
        }
    }
    
    func showBackgroundViewWhenNil(msg:String?, yOffset:CGFloat = 0) {
        if let showMessage = msg {
            let lblMsg = UILabel(frame: CGRect(x: 0, y: yOffset, width: self.bounds.width, height: 30))
            lblMsg.text = showMessage
            lblMsg.textAlignment = .center
            lblMsg.textColor = UIColor.gray
             lblMsg.font = UIFont.systemFont(ofSize: 14.0)
             let _uiview = UIView(frame: CGRect(x: 0, y: 0, width: self.bounds.width, height: self.bounds.height))
             lblMsg.center.x = _uiview.center.x
            _uiview.addSubview(lblMsg)
            
            self.backgroundView = _uiview
        } else {
            self.backgroundView = nil
        }
    }
}

//UICollectionView
extension UICollectionView{
    func registerCell(forIdentifier cellIdentifier:String){
        register(UINib(nibName: cellIdentifier, bundle: nil), forCellWithReuseIdentifier: cellIdentifier)
    }
    
    func registerSupplementaryViewOfKind(_ kind:String, cellIdentifier:String){
        register(UINib(nibName: cellIdentifier, bundle: nil), forSupplementaryViewOfKind: kind, withReuseIdentifier: cellIdentifier)
    }
    
    func showBackgroundViewWhenNil(msg:String?) {
        if let showMessage = msg {
            let lblMsg = UILabel(frame: CGRect(x: 0, y:0, width: self.bounds.width, height: self.bounds.height))
            lblMsg.text = showMessage
            lblMsg.textAlignment = .center
            lblMsg.textColor = UIColor.gray
            lblMsg.font = UIFont.systemFont(ofSize: 14.0)
            lblMsg.sizeToFit()
            self.backgroundView = lblMsg
        } else {
            self.backgroundView = nil
        }
    }
}

//String
extension String
{
    func removeWhiteSpace() -> String  {
        return self.trimmingCharacters(in: CharacterSet.whitespaces)
    }
}

extension UIView{
    func viewroundCorners(_ corners:UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
    }
}


extension Bundle{
    
    struct Static{
        static var token : Int = 0
        static var currentBundle : Bundle?
    }
    
    class func getCurrentBundle() -> Bundle{
        
        return Static.currentBundle!
    }
    
}

extension UIButton{
    func roundedButton(){
        let maskPAth1 = UIBezierPath(roundedRect: self.bounds,
                                     byRoundingCorners: [.topLeft , .topRight],
                                     cornerRadii:CGSize(width: 20, height: 20))
        let maskLayer1 = CAShapeLayer()
        maskLayer1.frame = self.bounds
        maskLayer1.path = maskPAth1.cgPath
        self.layer.mask = maskLayer1
    }
}

extension UISearchBar{
    func setCancleBtnColor(isbtnShow isShow:Bool){
        if(isShow){
            self.setShowsCancelButton(true, animated: true)
        }else{
            self.setShowsCancelButton(false, animated: true)
            self.resignFirstResponder()
            return
        }
        for view in (self.subviews[0]).subviews{
            if let button = view as? UIButton{
                button.setTitleColor(UIColor.white, for: UIControl.State())
            }
        }
    }
}


extension DateFormatter {
    
    @nonobjc static let universalTimeZone:DateFormatter = {
        let df = DateFormatter()
        df.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ" //"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'" //2016-09-06T11:12:58.158Z
        df.timeZone = TimeZone.current
        return df
    }()
    
    @nonobjc static let chatDateFormat:DateFormatter = {
        let df = DateFormatter()
        df.dateFormat = "MM/dd/yyyy"
        return df
    }()
}


extension String {
    
    func timeInterval(with formatter:DateFormatter) -> TimeInterval {
        guard let date = formatter.date(from: self) else { return 0.0 }
        return date.timeIntervalSince1970
    }
    
    func getDateTZFormat() -> Date {
        guard self != "" else { return Date() }
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        formatter.timeZone = TimeZone.current
        return formatter.date(from: self)!
    }
    
    
}

extension Date {
    
    func getDateStringFromDate() -> String    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        let dateString = dateFormatter.string(from: self)
        return dateString as String
    }
    
    func getDateStringFromDateWithFormat() -> String    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let dateString = dateFormatter.string(from: self)
        return dateString as String
    }
    
    func getDateStringFmDate() -> String     {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .medium
        let dateString = dateFormatter.string(from: self)
        return dateString
    }
    
    
    func string(with format: DateFormatter) -> String {
        return format.string(from: self)
    }
    
    init?(timeinterval:String, formatter:DateFormatter) {
        if let num_timeinterval = Double(timeinterval) {
            if num_timeinterval > 0.0 {
                self.init(timeIntervalSince1970: num_timeinterval)
            }
        }
        return nil
    }
    
    func getDateToRelativeFormat() -> String {
        let date = Date()
    
        
        let timeFormatter = DateFormatter()
        timeFormatter.dateFormat = "h:mm a"
        timeFormatter.timeZone = TimeZone.current
        
        let calendar = Calendar.current
        let components = (calendar as NSCalendar).components([.year, .month, .day, .hour, .minute, .second], from: self, to: date, options: [])
        var str: String
        
        if components.year! >= 1 {
            components.year == 1 ? (str = "year") : (str = "years")
            return "\(components.year!) \(str) ago"
        } else if components.month! >= 1 {
            components.month == 1 ? (str = "month") : (str = "months")
            return "\(components.month!) \(str) ago"
        } else if components.day! >= 1 {
            if components.day! > 1{
                return "\(components.day!) days ago"
            }else{
                return "Yesterday, \(timeFormatter.string(from: self))"
            }
        }else {
            if Calendar.current.isDateInToday(self) {
                   return "Today, \(timeFormatter.string(from: self))"
            } else if Calendar.current.isDateInYesterday(self) {
                 return "Yesterday, \(timeFormatter.string(from: self))"
            }
            return "Today, \(timeFormatter.string(from: self))"
        }
    }
    
}

extension Notification.Name {
    public static let DidWithdrawEvent = Notification.Name(rawValue: "DidWithdrawEvent")
}
