//
//  ZZAppDelegate.h
//  TWA
//
//  Created by Mustafa Shaheen on 10/28/19.
//  Copyright © 2019 Telecare. All rights reserved.
//


#import <UIKit/UIKit.h>

@interface ZZAppDelegate : UIResponder <UIApplicationDelegate>

@property(strong, nonatomic) UIWindow *window;

@end


