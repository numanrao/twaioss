//
//  MessageConstant.swift
//  MarijWannaMeet
//
//  Created by Fernando on 06/11/19.
//  Copyright © 2019 Lexset. All rights reserved.
//
struct AlertMessage {
	
    static let ERROR:String	= "Error"
    static let LOADING:String	= "Loading..."
    static let NO_DATA = "No Data Available"
}

struct ValidationMessage {
    
    static let EMPTY_CONFIRM_PASSWORD	= "Please enter Confirm password"
    static let EMPTY_PASSWORD	= "Please enter password"
	static let EMPTY_NAME		= "Please enter name"
    static let EMPTY_USERNAME	= "Please enter username"
	
	static let EMPTY_FIELD = "All fields are required"
	static let INVALID_NAME:String = "Name or Surname must include at least two characters"
    static let INVALID_OLDPASSWORD:String = "You have entered a wrong password"
	static let INVALID_PASSWORD:String = "Password must contain at least 8 characters with one number, one alphabet and one special character"
    static let INVALID_EMAIL = "Please enter valid email address"
    
    static let LOG_OUT = "Are you sure want to Logout?"
    static let PREMIUM_MESSAGE = "This is for Premium user only"

    static let NETWORK_OFFLINE =  "The Internet connection appears to be offline"
    static let NETWORK_LOST = "Network connection was lost"
}
