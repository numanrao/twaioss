//
//  AppData.swift
//  TWA
//
//  Created by MascSoft on 7/1/19.
//  Copyright © 2019 Telecare. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import CoreLocation

class AppData: NSObject {

    static let shared = AppData()
    
    private let userDefaults = UserDefaults.standard
    
    var remoteNotificationData: NSDictionary?
    
    var recordingId: String?
    
    var userToken: String? {
        didSet{
            userDefaults.set(userToken, forKey: AppKeys.userToken)
            userDefaults.synchronize()
        }
    }
    var currentUser: User? {
        didSet{
            saveCurrentUser()
        }
    }
    
    var deviceFCMToken: String? {
        didSet{
            userDefaults.set(deviceFCMToken, forKey: AppKeys.deviceFCMToken)
            userDefaults.synchronize()
        }
    }
    var deviceID: String? {
        didSet{
            userDefaults.set(deviceID, forKey: AppKeys.deviceID)
            userDefaults.synchronize()
        }
    }
    
    var defaultQuestions: [String] = ["What was your favorite sport in high school?",
                                      "What is your pet's name?",
                                      "What is your mother’s (father's) first name?",
                                      "What is your mother's maiden name?",
                                      "What was the color of your first car?",
                                      "What is your favorite team?",
                                      "What is your favorite movie?",
                                      "What is the name of your favorite childhood friend?",
                                      "What was the name of your first stuffed animal?",
                                      "In what city or town was your first job?",
                                      "What was your dream job as a child?"]
    
    override init() {
        super.init()
        loadLocalData()
    }
    
    func loadLocalData(){
        userToken = userDefaults.string(forKey: AppKeys.userToken)
        deviceFCMToken = userDefaults.string(forKey: AppKeys.deviceFCMToken)
        deviceID = userDefaults.string(forKey: AppKeys.deviceID)
        
        if let userObj = userDefaults.object(forKey: AppKeys.currentUser) {
            let json = JSON(userObj)
            currentUser = User.init(json)
        }else{
            currentUser = nil
        }
    }
    
    func saveCurrentUser(){
        userDefaults.set(currentUser?.toJSON(), forKey: AppKeys.currentUser)
        userDefaults.synchronize()
    }
    
    func isProfessionalMode() -> Bool{
        if let user = currentUser {
            return user.isProfessional
        }
        return false
    }
    
    func isLogin() -> Bool {
        
        if let token = self.userToken , token.isEmpty == false {
            var params: [String : Any] = [:]
            
            params["status"] = true
            
            ServerHelper.shared.callPostAPI(ApiEndpoint.USER_UPDATE, parameters: params, success: { (json) in
                
            }) { (errMsg, errCode) in
                
            }
            return true
        }
        return false
    }
    
    func logout() {
        
        if userToken == nil {
            return ;
        }
        var params: [String : Any] = [:]
            
            params["status"] = false
            
            ServerHelper.shared.callPostAPI(ApiEndpoint.USER_UPDATE, parameters: params, success: { (json) in
                
            }) { (errMsg, errCode) in
                
            }
        
        if let idStr = deviceID {
            SocketHelper.shared.updateDeviceStatus(idStr, deviceToken: "", isOnline: false, completion: nil)
        }
        
        userToken = nil
        currentUser = nil
        
        if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
            appDelegate.gotoStartScreen()
        }
    }
    
    func updateCurrentUserLocation(_ location: CLLocation) {
        
        if self.isLogin()  {
            var params: [String : Any] = [:]
            
            params["longitude"] = location.coordinate.longitude
            params["latitude"] = location.coordinate.latitude
            
            ServerHelper.shared.callPostAPI(ApiEndpoint.USER_UPDATE, parameters: params, success: { (json) in
                
            }) { (errMsg, errCode) in
                
            }
        }
    }
    
    func registerDevice(){
        var fcmToken = ""
        if deviceFCMToken != nil {
            fcmToken = deviceFCMToken!
        }
        
        if let idStr = deviceID {
            SocketHelper.shared.updateDeviceStatus(idStr, deviceToken: fcmToken, isOnline: true, completion:{ (json) in
                if let res = json, let data = res["data"].dictionaryObject {
                    self.deviceID = data["id"] as? String
                }
            })
        }else {
            SocketHelper.shared.addDevice(fcmToken) { (json) in
                if let res = json, let data = res["data"].dictionaryObject {
                    self.deviceID = data["id"] as? String
                }
            }
        }
    }
}
