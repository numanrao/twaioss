//
//  Urls.swift
//  TWA
//
//  Created by MascSoft on 7/1/19.
//  Copyright © 2019 Telecare. All rights reserved.
//

import UIKit

struct AppKeys{
    static let userToken = "AppUserToken"
    static let currentUser = "AppCurrentUser"
    static let deviceFCMToken = "AppDeviceFCMToken"
    static let deviceID = "AppDeviceID"
}

struct ParamKeys{
    static let name = "name"
}
