//
//  Constants.swift
//  TWA
//
//  Created by MascSoft on 7/1/19.
//  Copyright © 2019 Telecare. All rights reserved.
//

import Foundation
import UIKit

struct AppInfo{
    static let title = "TWA"
    static let version = "1.0.0"
    static let testingMode = true
    
    static let JitsiMeetServerURL: String = "https://sixth.thirdwitness.net"
    
    static let RtmpServerLiveURLLEO: String = "rtmp://66.185.23.114:1941/live/"
    static let RtmpServerLiveURLEO_IN: String = "rtmp://66.185.23.114:1943/live/"
    static let RtmpServerLiveURLCITIZEN: String = "rtmp://66.185.23.114:1937/live/"
    static let RtmpServerLiveURLCITIZEN_IN: String = "rtmp://66.185.23.114:1939/live/"
    
    
    static let RtmpServerHttpURLLEO: String = "http://66.185.23.114/videos/leo/"
    static let RtmpServerHttpURLLEO_IN: String = "http://66.185.23.114/videos/leo/involuntary/"
    static let RtmpServerHttpURLCITIZEN: String = "http://66.185.23.114/videos/citizen/"
    static let RtmpServerHttpURLCITIZEN_IN: String = "http://66.185.23.114/videos/citizen/involuntary/"
    
    
   // #if PROD_MODE
        static let ApiServerURL: String = "http://sixth.thirdwitness.net:1342"
   // #else
     //   static let ApiServerURL: String = "http://172.16.1.71:1340"
    //#endif
}

struct AppColors {
    
    static let red = UIColor(red: 207/255 , green: 42/255, blue: 39/255, alpha: 1)
    static let blue = UIColor(red: 43/255 , green: 120/255, blue: 228/255, alpha: 1)
    static let green = UIColor(red: 0/255 , green: 150/255, blue: 0/255, alpha: 1)
    static let tabTint = UIColor(red: 0/255 , green: 255/255, blue: 255/255, alpha: 1)
    static let userSelected = UIColor(red: 161/255 , green: 196/255, blue: 202/255, alpha: 1)
}

struct AppImages {
    static let customer  = UIImage(named: "user-customer")
    static let professional  = UIImage(named: "user-professional")
    static let genderMale = UIImage(named: "gender_male")
    static let genderFemale = UIImage(named: "gender_female")
    static let avatarPlaceholder = UIImage(named: "avatar-placeholder")
}

extension Notification.Name {
    
    public static let AppRefreshContactList = Notification.Name(rawValue: "AppRefreshContactList")
    public static let AppRefreshUserList = Notification.Name(rawValue: "AppRefreshUserList")
    public static let AppRefreshRecordingList = Notification.Name(rawValue: "AppRefreshRecordingList")
}
