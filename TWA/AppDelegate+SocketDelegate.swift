//
//  AppDelegate+SocketDelegate.swift
//  TWA
//
//  Created by MascSoft on 7/29/19.
//  Copyright © 2019 Telecare. All rights reserved.
//

import UIKit
import SwiftyJSON
import AVFoundation
extension AppDelegate {
    
    //MARK: Handling Remote notification Data
    func handleRemoteMessage(){
        
        if AppData.shared.isLogin(), let msgData = AppData.shared.remoteNotificationData {
            
            let systemSoundID: SystemSoundID = 1315

            // to play sound
            AudioServicesPlaySystemSound (systemSoundID)
            guard  let actions = msgData["actions"] as? String else { return }
            
            if actions == "message"
            {
                self.receivePrivateMessage(msgData)
            }
            else if actions == "typing"
            {
                self.receiveTypingSocket(msgData)
            }
            else if actions == "userstatusupdate"
            {
                self.receiveUserStatusUpdateSocket(msgData)
            }
            else if actions == "onlineuserlist"
            {
                self.receiveOnlineUserListSocket(msgData)
            }
            else if actions == "contactRequest"
            {
                self.receiveContactRequestSocket(msgData)
            }
            else if actions == "contactAccept"
            {
                self.receiveContactAcceptedSocket(msgData)
            }
            else if actions == "contactDecline"
            {
                self.receiveContactDeclinedSocket(msgData)
            }
            else if actions == "conferenceRequest"
            {
                self.receiveConferenceRequestSocket(msgData)
            }
            else if actions == "conferenceAccept"
            {
                self.receiveConferenceAcceptedSocket(msgData)
            }
            else if actions == "conferenceDecline"
            {
                self.receiveConferenceDeclinedSocket(msgData)
            }
            else if actions == "urgencyRequest"
            {
                self.receiveUrgencyRequestSocket(msgData)
            }
            else if actions == "urgencyAccept"
            {
                self.receiveUrgencyAcceptedSocket(msgData)
            }
            else if actions == "receivePhotoID"
            {
                self.receivePhotoIDSocket(msgData)
            }
            else if actions == "receiveTicket"
            {
                self.receiveIssueTicketSocket(msgData)
            }
            else if actions == "receiveSummon"
            {
                self.receiveIssueSummonSocket(msgData)
            }
            
            AppData.shared.remoteNotificationData = nil
        }
    }
}

extension AppDelegate: SocketHelperDelegate {
    
    func convertAnyToObject(_ value: Any?) -> JSON? {
        let systemSoundID: SystemSoundID = 1315
        AudioServicesPlaySystemSound (systemSoundID)
        if let obj = value as? [String: Any] {
            return JSON(obj)
        }else if let str = value as? String {
            return JSON(parseJSON: str)
        }
        return nil
    }
    
    func receivePrivateMessage(_ data: NSDictionary){
        if let chatObj = convertAnyToObject(data["chat"]) {
            let systemSoundID: SystemSoundID = 1315
            AudioServicesPlaySystemSound (systemSoundID)
            let chat = DBMessage(chatObj)
            NotificationCenterX.post(notification: NOTIFICATION_RECEIVE_MESSAGE, object: chat)
        }
    }
    
    func receiveGroupMessage(_ data: NSDictionary) {
        if let chatObj = convertAnyToObject(data["chat"]) {
            let systemSoundID: SystemSoundID = 1315
            AudioServicesPlaySystemSound (systemSoundID)
            let chat = DBMessage(chatObj)
            
            if let userObj = convertAnyToObject(data["user"]) {
               chat.sender = User(userObj)
            }
            NotificationCenterX.post(notification: NOTIFICATION_RECEIVE_GROUP_MESSAGE, object: chat)
        }
    }
    
    func receiveTypingSocket(_ data: NSDictionary){
        
    }
    func receiveUserStatusUpdateSocket(_ data: NSDictionary) {
        
    }
    func receiveOnlineUserListSocket(_ data: NSDictionary) {
        
    }
    
    // Contact
    func receiveContactRequestSocket(_ data: NSDictionary) {
        
        if let userObj = convertAnyToObject(data["fromUser"]) {
            let systemSoundID: SystemSoundID = 1315
            AudioServicesPlaySystemSound (systemSoundID)
            let fromUser = User(userObj)
            
            var strTitle = "Alert"
            if !fromUser.greeting.isEmpty {
                strTitle = fromUser.greeting
            }
            let strMsg = "You received a contact request from \(fromUser.name)"
            
            let alert = UIAlertController(title: strTitle, message: strMsg, preferredStyle: .alert)
            let accept = UIAlertAction(title: "Accept", style: .default) { (action) in
                self.callAPIContactAccept(fromUser.userId)
            }
            let decline = UIAlertAction(title: "Reject", style: .default) { (action) in
                self.callAPIContactDecline(fromUser.userId)
            }
            
            alert.addAction(accept)
            alert.addAction(decline)
            
            let vc = getCurrentViewController()
            vc?.present(alert, animated: true, completion: nil)
            
            self.callAPISaveReceiveEvent("Sent contact request.", sender: fromUser)
        }
        
    }
    
    func receiveContactAcceptedSocket(_ data: NSDictionary) {
        if let userObj = convertAnyToObject(data["fromUser"]) {
            let systemSoundID: SystemSoundID = 1315
            AudioServicesPlaySystemSound (systemSoundID)
            let fromUser = User(userObj)
            let strMsg = "\(fromUser.name) accepted your contact request."
            
            let vc = getCurrentViewController()
            vc?.showPromptAlert("Alert", message: strMsg)
            
            self.callAPISaveReceiveEvent("Accepted contact request.", sender: fromUser)
        }
    }
    
    func receiveContactDeclinedSocket(_ data: NSDictionary) {
        if let userObj = convertAnyToObject(data["fromUser"]) {
            let systemSoundID: SystemSoundID = 1315
            AudioServicesPlaySystemSound (systemSoundID)
            let fromUser = User(userObj)
            let strMsg = "\(fromUser.name) declined your contact request."
            
            let vc = getCurrentViewController()
            vc?.showPromptAlert("Alert", message: strMsg)
            
            self.callAPISaveReceiveEvent("Declined contact request.", sender: fromUser)
        }
    }
    
    // Conference
    func receiveConferenceRequestSocket(_ data: NSDictionary) {
        if let userObj = convertAnyToObject(data["fromUser"]), let type = data["conferenceType"] as? String, let conferenceId = data["conferenceId"] as? String {
            let systemSoundID: SystemSoundID = 1315
            AudioServicesPlaySystemSound (systemSoundID)
            let fromUser = User(userObj)
            
            let strTitle = "Alert"
            var strMsg = "You received a conference request from \(fromUser.name)."
            
            if type == "video" {
                strMsg = "You received a Video conference request from \(fromUser.name)."
            }else if type == "audio" {
                strMsg = "You received a Audio conference request from \(fromUser.name)."
            }else if type == "private_chat" || type == "group_chat" {
                strMsg = "You received a Text chatting conference request from \(fromUser.name)."
            }
            
            let alert = UIAlertController(title: strTitle, message: strMsg, preferredStyle: .alert)
            let accept = UIAlertAction(title: "Accept", style: .default) { (action) in
                self.callAPIConferenceAccept(conferenceId, sender: fromUser, type: type)
            }
            let reject = UIAlertAction(title: "Reject", style: .default) { (action) in
                
            }
            
            alert.addAction(accept)
            alert.addAction(reject)
            
            let vc = getCurrentViewController()
            vc?.present(alert, animated: true, completion: nil)
            
            var strEvent = "Sent conference request."
            if type == "video" {
                strEvent = "Sent video conference request."
            }else if type == "audio" {
                strEvent = "Sent audio conference request."
            }else if type == "private_chat" || type == "group_chat" {
                strEvent = "Sent text chatting conference request."
            }
            
            self.callAPISaveReceiveEvent(strEvent, sender: fromUser)
        }
    }
    
    func receiveConferenceAcceptedSocket(_ data: NSDictionary) {
        if let userObj = convertAnyToObject(data["fromUser"]), let type = data["conferenceType"] as? String {
            let systemSoundID: SystemSoundID = 1315
            AudioServicesPlaySystemSound (systemSoundID)
            let fromUser = User(userObj)
            var strMsg = "\(fromUser.name) accepted your conference request."
            
            if type == "video" {
                strMsg = "\(fromUser.name) accepted your Video conference request."
            }else if type == "audio" {
                strMsg = "\(fromUser.name) accepted your Audio conference request."
            }else if type == "private_chat" || type == "group_chat" {
                strMsg = "\(fromUser.name) accepted your Text chatting conference request."
            }
            
            let vc = getCurrentViewController()
            vc?.showPromptAlert("Alert", message: strMsg)
            
            var strEvent = "Accepted conference request."
            if type == "video" {
                strEvent = "Accepted video conference request."
            }else if type == "audio" {
                strEvent = "Accepted audio conference request."
            }else if type == "private_chat" || type == "group_chat" {
                strEvent = "Accepted text chatting conference request."
            }
            
            self.callAPISaveReceiveEvent(strEvent, sender: fromUser)
        }
    }
    
    func receiveConferenceDeclinedSocket(_ data: NSDictionary) {
        if let userObj = convertAnyToObject(data["fromUser"]), let type = data["conferenceType"] as? String {
            let systemSoundID: SystemSoundID = 1315
            AudioServicesPlaySystemSound (systemSoundID)
            let fromUser = User(userObj)
            var strMsg = "\(fromUser.name) declined your conference request."
            
            if type == "video" {
                strMsg = "\(fromUser.name) declined your Video conference request."
            }else if type == "audio" {
                strMsg = "\(fromUser.name) declined your Audio conference request."
            }else if type == "private_chat" || type == "group_chat" {
                strMsg = "\(fromUser.name) declined your Text chatting conference request."
            }
            
            let vc = getCurrentViewController()
            vc?.showPromptAlert("Alert", message: strMsg)
            
            var strEvent = "Declined conference request."
            if type == "video" {
                strEvent = "Declined video conference request."
            }else if type == "audio" {
                strEvent = "Declined audio conference request."
            }else if type == "private_chat" || type == "group_chat" {
                strEvent = "Declined text chatting conference request."
            }
            
            self.callAPISaveReceiveEvent(strEvent, sender: fromUser)
        }
    }
    
    // Urgency
    func receiveUrgencyRequestSocket(_ data: NSDictionary) {
        if let userObj = convertAnyToObject(data["fromUser"]) {
            let systemSoundID: SystemSoundID = 1315
            AudioServicesPlaySystemSound (systemSoundID)
            let fromUser = User(userObj)
            let strMsg = "\(fromUser.name) sent you urgency alert."
            
            let alert = UIAlertController(title: "Alert", message: strMsg, preferredStyle: .alert)
            let accept = UIAlertAction(title: "OK", style: .default) { (action) in
                self.callAPIUrgencyAccept(fromUser.userId)
            }
            
            alert.addAction(accept)
            
            let vc = getCurrentViewController()
            vc?.present(alert, animated: true, completion: nil)
            
            self.callAPISaveReceiveEvent("Sent urgency alert.", sender: fromUser)
        }
    }
    
    func receiveUrgencyAcceptedSocket(_ data: NSDictionary) {
        if let userObj = convertAnyToObject(data["fromUser"]) {
            let systemSoundID: SystemSoundID = 1315
            AudioServicesPlaySystemSound (systemSoundID)
            let fromUser = User(userObj)
            let strMsg = "\(fromUser.name) received your urgency alert."
            
            let vc = getCurrentViewController()
            vc?.showPromptAlert("Alert", message: strMsg)
            
            self.callAPISaveReceiveEvent("Accepted your urgency alert.", sender: fromUser)
        }
    }
    
    func receivePhotoIDSocket(_ data: NSDictionary) {
        if let userObj = convertAnyToObject(data["fromUser"]) {
            let fromUser = User(userObj)
            let strMsg = "\(fromUser.name) sent you photo ID."
            
            let vc = getCurrentViewController()
            vc?.showPromptAlert("Alert", message: strMsg)
            
            self.callAPISaveReceiveEvent("Sent your photo ID.", sender: fromUser)
        }
    }
    
    func receiveIssueTicketSocket(_ data: NSDictionary) {
        if let userObj = convertAnyToObject(data["fromUser"]) {
            let fromUser = User(userObj)
            let strMsg = "\(fromUser.name) sent you issue ticket."
            
            let vc = getCurrentViewController()
            vc?.showPromptAlert("Alert", message: strMsg)
            
            self.callAPISaveReceiveEvent("Sent issue ticket.", sender: fromUser)
        }
    }
    
    func receiveIssueSummonSocket(_ data: NSDictionary) {
        if let userObj = convertAnyToObject(data["fromUser"]) {
            let fromUser = User(userObj)
            let strMsg = "\(fromUser.name) sent you issue summon."
            
            let vc = getCurrentViewController()
            vc?.showPromptAlert("Alert", message: strMsg)
            
            self.callAPISaveReceiveEvent("Sent issue summon.", sender: fromUser)
        }
    }
}


// API Calls
extension AppDelegate {
    
    func callAPIContactAccept(_ fromUserId: String) {
        
        let params = ["sender" : fromUserId]
        ServerHelper.shared.callPostAPIWithRecording(ApiEndpoint.CONTACT_ACCEPT, parameters: params, success: { [weak self] (json) in
            var contactUser: User? = nil
            if json != JSON.null {
                contactUser = User(json)
            }
            NotificationCenter.default.post(name: .AppRefreshContactList, object: contactUser)
            
        }) { [weak self] (errMsg, errCode) in
            
        }
    }
    
    func callAPIContactDecline(_ fromUserId: String) {
        
        let params = ["sender" : fromUserId]
        ServerHelper.shared.callPostAPIWithRecording(ApiEndpoint.CONTACT_DECLINE, parameters: params, success: { [weak self] (json) in
        }) { [weak self] (errMsg, errCode) in
            
        }
    }
    
    func callAPIConferenceAccept(_ conferenceId: String, sender: User,  type: String) {
        
        let params = ["sender" : sender.userId, "type": type, "conferenceId": conferenceId]
        
        ServerHelper.shared.callPostAPIWithRecording(ApiEndpoint.CONFERENCE_ACCEPT, parameters: params, success: { [weak self] (json) in
            
        }) { [weak self] (errMsg, errCode) in
            
        }
        
        if let topVC = getCurrentViewController(){
            if type == "private_chat" {
                ChatPrivateView.presentChatView(receiver: sender, groupId: conferenceId, viewController: topVC)
            }else if type == "group_chat" {
                ChatGroupView.presentChatView(groupId: conferenceId, viewController: topVC)
            }else {
                 if let vc = AVConferenceVC.getInstance() {
                    vc.conferenceType = type
                    vc.conferenceRoom = conferenceId
                    topVC.present(vc, animated: true, completion: nil)
                }
            }
        }
    }
    
    func callAPIConferenceDecline(_ conferenceId: String, sender: User,  type: String) {
        
        let params = ["sender" : sender.userId, "type": type, "conferenceId": conferenceId]
        
        ServerHelper.shared.callPostAPIWithRecording(ApiEndpoint.CONFERENCE_DECLINE, parameters: params, success: { [weak self] (json) in
        }) { [weak self] (errMsg, errCode) in
            
        }
    }
    
    func callAPIUrgencyAccept(_ fromUserId: String) {
        
        let params = ["sender" : fromUserId]
        ServerHelper.shared.callPostAPIWithRecording(ApiEndpoint.URGENCY_ALERT_ACCEPT, parameters: params, success: { [weak self] (json) in
        }) { [weak self] (errMsg, errCode) in
            
        }
    }
    
    func callAPISaveReceiveEvent(_ event: String, sender: User) {
        
        guard let recordingId = AppData.shared.recordingId, recordingId.isEmpty == false else {
            return
        }
        
        let params = ["sender" : sender.userId, "event": event, "recordingId": recordingId]
        
        ServerHelper.shared.callPostAPI(ApiEndpoint.RECORDING_SAVE_EVENT, parameters: params, success: {(json) in
        }) {(errMsg, errCode) in
            
        }
    }
}
