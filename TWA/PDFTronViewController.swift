//
//  PDFTronViewController.swift
//  TWA
//
//  Created by Mustafa Shaheen on 12/6/19.
//  Copyright © 2019 Telecare. All rights reserved.
//

import UIKit
import PDFNet
import Tools
class PDFTronViewController: UIViewController {

    var user: User?
    var isFromContactList: Bool = false
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    override func viewDidAppear(_ animated: Bool) {
        // Create a PTDocumentViewController
        let documentController = PTDocumentViewController()
        // The PTDocumentViewController must be in a navigation controller before a document can be opened
        var navigationController = UINavigationController(rootViewController: documentController)
        // Open a file from URL.
        documentController.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "nav-back"), style: .plain, target: self, action: #selector(backPressed))
        let fileURL: URL = URL(string:"http://35.161.249.208/springboot/test/pdf.pdf")!
        documentController.openDocument(with: fileURL)
        // Show navigation (and document) controller.
        self.present(navigationController, animated: true, completion: nil)
    }

    @objc func backPressed(){
        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "CustomerDetailVC") as? CustomerDetailVC {
            vc.user = user
            vc.isFromContactList = isFromContactList
            vc.isEmail = true
            self.pushVC(vc, animated: true)
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
