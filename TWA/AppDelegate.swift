//
//  AppDelegate.swift
//  TWA
//
//  Created by MascSoft on 6/26/19.
//  Copyright © 2019 Telecare. All rights reserved.
//

import UIKit
import AVFoundation
import Photos
import IQKeyboardManager
import SVProgressHUD
import Braintree
import Firebase
import Fabric
import Crashlytics
import PDFNet
import FirebaseMessaging

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, MessagingDelegate, UNUserNotificationCenterDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        UINavigationBar.appearance().setBackgroundImage(UIImage(named: "navbar")?.resizableImage(withCapInsets: UIEdgeInsets.zero, resizingMode: .stretch), for: .default)
//        UITabBar.appearance().tintColor = UIColor.red
        var params: [String : Any] = [:]
        if let location = LocationHelper.shared.currentLocation {
            params["longitude"] = location.coordinate.longitude
            params["latitude"] = location.coordinate.latitude
        }
        
        
        ServerHelper.shared.callPostAPI(ApiEndpoint.USER_UPDATE, parameters: params, success: { (json) in
            
        }) { (errMsg, errCode) in
            
        }
        BTAppSwitch.setReturnURLScheme("Com.Telecare.TWA.payments")
        PTPDFNet.initialize("Insert Commercial License Key Here After Purchase")
        IQKeyboardManager.shared().isEnabled = true
        SVProgressHUD.setDefaultMaskType(.black)
        
        LocationHelper.shared.enable()
        
        // Use Firebase library to configure APIs
        FirebaseApp.configure()
        Messaging.messaging().delegate = self
        
        Fabric.sharedSDK().debug = true
        
        enableRemoteNotification(application)
        
        SocketHelper.shared.delegate = self
        SocketHelper.shared.establishConnection()
        
        // Realm migration
        //realmMigrate()
        
        checkCameraAuthorization()
        checkPhotosAuthorization()
        
        if AppData.shared.isLogin() {
            AppData.shared.remoteNotificationData = launchOptions?[UIApplication.LaunchOptionsKey.remoteNotification] as? NSDictionary
            
            gotoHomeScreen()
        }else{
            gotoStartScreen()
        }
        
        return true
    }

    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        if(url.scheme?.localizedCaseInsensitiveCompare("Com.Telecare.TWA.payments") == .orderedSame){
            return BTAppSwitch.handleOpen(url,options: options)
        }
        return false
    }
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        var params: [String : Any] = [:]
        if let location = LocationHelper.shared.currentLocation {
            params["longitude"] = location.coordinate.longitude
            params["latitude"] = location.coordinate.latitude
        }
        
        
        ServerHelper.shared.callPostAPI(ApiEndpoint.USER_UPDATE, parameters: params, success: { (json) in
            
        }) { (errMsg, errCode) in
            
        }
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    func realmMigrate() {
        
        let config = RLMRealmConfiguration.default()
        config.schemaVersion = 5
        config.migrationBlock = { migration, oldSchemaVersion in
            if (oldSchemaVersion < 5) {
                
            }
        }
        RLMRealmConfiguration.setDefault(config)
        RLMRealm.default()
    }

    func gotoStartScreen(){
        let storyBoard = UIStoryboard.init(name: "Main", bundle: Bundle.main)
        let vc = storyBoard.instantiateViewController(withIdentifier: "AppNavigationController")
        
        self.window?.rootViewController = vc;
        self.window?.makeKeyAndVisible()
    }

    func gotoHomeScreen(){
        
        let vc = AppMainVC.getInstance()
        
        self.window?.rootViewController = vc;
        self.window?.makeKeyAndVisible()
    }
    
    func enableRemoteNotification(_ application: UIApplication) {
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()
    }
    
    // UserNotificationCenter Delegate
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        //completionHandler()
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        //completionHandler([.alert, .badge, .sound])
    }
    
    
    
   /* func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        
        print("remote message:", remoteMessage.appData)
        AppData.shared.remoteNotificationData = remoteMessage.appData as NSDictionary
        handleRemoteMessage()
    }*/
    
    // MessagingDelegate
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
        print("Firebase registration token: \(fcmToken)")
        
        AppData.shared.deviceFCMToken = fcmToken;
        
        //let dataDict:[String: String] = ["token": fcmToken]
        //NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
        // TODO: If necessary send token to application server.
        // Note: This callback is fired at each app startup and whenever a new token is generated.
    }
    
    // In order to get Camera Access permission
    func checkCameraAuthorization() {
        if AVCaptureDevice.authorizationStatus(for: .video) ==  .authorized {
            //already authorized
        } else {
            AVCaptureDevice.requestAccess(for: .video, completionHandler: { (granted: Bool) in
                if granted {
                    //access allowed
                } else {
                    //access denied
                }
            })
        }
    }
    
    // In order to get Photos(camera roll) permission to save videos(recordings)
    func checkPhotosAuthorization() {
        
        let photos = PHPhotoLibrary.authorizationStatus()
        if photos == .notDetermined {
            PHPhotoLibrary.requestAuthorization { (status) in
                if status == .authorized{
                    
                } else {}
            }
        }
    }
}
