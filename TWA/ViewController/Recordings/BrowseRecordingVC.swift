//
//  BrowseRecordingVC.swift
//  TWA
//
//  Created by MascSoft on 8/15/19.
//  Copyright © 2019 Telecare. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit

class BrowseRecordingVC: UIViewController {

    let selectedColor = UIColor(red: 147/255.0, green: 187/255.0, blue: 244/255.0, alpha: 1)
    
    @IBOutlet weak var lblUserName: UILabel!
    
    // Status & Events
    @IBOutlet weak var sectionEvents: UIView!
    @IBOutlet weak var viewEvents: UIView!
    @IBOutlet weak var tblEvents: UITableView!
    
    // Video
    @IBOutlet weak var sectionVideo: UIView!
    @IBOutlet weak var viewVideo: UIView!
    @IBOutlet weak var tblVideo: UITableView!
    
    // Audio
    @IBOutlet weak var sectionAudio: UIView!
    @IBOutlet weak var viewAudio: UIView!
    @IBOutlet weak var tblAudio: UITableView!
    
    // Chatting
    @IBOutlet weak var sectionChat: UIView!
    @IBOutlet weak var viewChatting: UIView!
    @IBOutlet weak var tblChat: UITableView!
    
    var userInfo: User!
    var recording: Recording!
    
    var eventList: [RecordingItem] = []
    var videoList: [RecordingItem] = []
    var audioList: [RecordingItem] = []
    var chatList: [RecordingItem] = []
    
    var chatMessageList: [DBMessage] = []
    var isLoadChatMessage: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        lblUserName.text = userInfo.name
        
        // Do any additional setup after loading the view.
        
        tblEvents.dataSource = self
        tblEvents.delegate = self
        tblEvents.tableFooterView = UIView(frame: .zero)
        
        tblVideo.dataSource = self
        tblVideo.delegate = self
        tblVideo.tableFooterView = UIView(frame: .zero)
        
        tblAudio.dataSource = self
        tblAudio.delegate = self
        tblAudio.tableFooterView = UIView(frame: .zero)
        
        tblChat.dataSource = self
        tblChat.delegate = self
        tblChat.tableFooterView = UIView(frame: .zero)
        
        selectEventsView()
        
        callAPIRecordingDetails()
    }
    
    
    func hideAllSections(){
        
        sectionEvents.backgroundColor = .white
        sectionVideo.backgroundColor = .white
        sectionAudio.backgroundColor = .white
        sectionChat.backgroundColor = .white
        
        viewEvents.isHidden = true
        viewVideo.isHidden = true
        viewAudio.isHidden = true
        viewChatting.isHidden = true
    }
    
    func reloadView() {
//        if eventList.isEmpty {
//            tblEvents.showBackgroundViewWhenNil(msg:"No Events")
//        }else{
//            tblEvents.showBackgroundViewWhenNil(msg: nil)
//        }
        tblEvents.reloadData()
        
        tblVideo.reloadData()
        
        if audioList.isEmpty {
            tblAudio.showBackgroundViewWhenNil(msg: "No Audio")
        }else{
            tblAudio.showBackgroundViewWhenNil(msg: nil)
        }
        tblAudio.reloadData()
        
        reloadChatList()
    }
    
    func reloadChatList(){
        if chatMessageList.isEmpty {
            tblChat.showBackgroundViewWhenNil(msg: "No chat history")
        }else{
            tblChat.showBackgroundViewWhenNil(msg: nil)
        }
        tblChat.reloadData()
    }
    
    func selectEventsView(){
        hideAllSections()
        
        sectionEvents.backgroundColor = selectedColor
        viewEvents.isHidden = false
    }
    
    //MARK: UIActions
    
    @IBAction func onBack(_ sender: UIButton) {
        self.popVC(true)
    }
    
    @IBAction func onEvents(_ sender: UIButton) {
        selectEventsView()
    }
    
    @IBAction func onVideo(_ sender: UIButton) {
        hideAllSections()
        
        sectionVideo.backgroundColor = selectedColor
        viewVideo.isHidden = false
    }
    
    @IBAction func onAudio(_ sender: UIButton) {
        hideAllSections()
        
        sectionAudio.backgroundColor = selectedColor
        viewAudio.isHidden = false
    }
    
    @IBAction func onChat(_ sender: UIButton) {
        hideAllSections()
        
        sectionChat.backgroundColor = selectedColor
        viewChatting.isHidden = false
        
        if let item = chatList.first {
            if !isLoadChatMessage {
                isLoadChatMessage = true
                callAPIChatMessages(item.content)
            }
        }
    }

    
    //MARK: Functions
    
    func playRecordVideo(_ url: String) {
        if let videoURL = URL(string: url) {
            let player = AVPlayer(url: videoURL)
            let playerViewController = AVPlayerViewController()
            playerViewController.player = player
            self.present(playerViewController, animated: true) {
                playerViewController.player!.play()
            }
        }else{
            showPromptAlert("Sorry!", message: "Invalid Video URL")
        }
    }
}

extension BrowseRecordingVC {
    func callAPIRecordingDetails() {
        
        self.showProgressHUD()
        
        let params = ["userId": userInfo.userId, "recordingId": recording.recordingId]
        
        ServerHelper.shared.callPostAPI(ApiEndpoint.RECORDING_DETAILS, parameters: params, success: { [weak self] (json) in
            
            for itemObj in json["event"].arrayValue {
                self?.eventList.append(RecordingItem(itemObj))
            }
            for itemObj in json["video"].arrayValue {
                self?.videoList.append(RecordingItem(itemObj))
            }
            for itemObj in json["audio"].arrayValue {
                self?.audioList.append(RecordingItem(itemObj))
            }
            for itemObj in json["chat"].arrayValue {
                self?.chatList.append(RecordingItem(itemObj))
            }
            
            self?.hideProgressHUD()
            self?.reloadView()
            
        }) { [weak self] (errMsg, errCode) in
            self?.hideProgressHUD()
            //self?.showPromptAlert("Sorry!", message: errMsg);
            
            self?.reloadView()
        }
    }
    
    func callAPIChatMessages(_ groupId: String?) {
        
        guard let idStr = groupId, idStr.isEmpty == false else {
             return
        }
        
        self.showProgressHUD()
        
        let params = ["groupId": idStr]
        
        ServerHelper.shared.callPostAPI(ApiEndpoint.GROUP_CHAT_HISTORY, parameters: params, success: { [weak self] (json) in
            
            for messageObj in json.arrayValue {
                self?.chatMessageList.append(DBMessage(messageObj))
            }
            
            self?.hideProgressHUD()
            self?.reloadChatList()
            
        }) { [weak self] (errMsg, errCode) in
            self?.hideProgressHUD()
            self?.reloadChatList()
        }
    }
}

//////////////////
/////////////////
// MAR: UITableview Datasource and Delegate

extension BrowseRecordingVC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var rows: Int = 0
        if tableView == tblEvents {
            rows = eventList.count + 1
        }else if tableView == tblVideo {
            rows = videoList.count + 1
        }else if tableView == tblAudio {
            rows = audioList.count
        }else if tableView == tblChat {
            rows = chatMessageList.count
        }
        
        return rows
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == tblEvents {
            if indexPath.row == 0 {
                if let cell = tableView.dequeueReusableCell(withIdentifier: "locationCell") as? ItemLocationCell {
                    
                    cell.initCell(recording, receiverItem: nil)
                    return cell
                }
            }else {
                if let cell = tableView.dequeueReusableCell(withIdentifier: "eventCell") as? ItemEventCell {
                    if indexPath.row - 1 < eventList.count {
                        let eventItem = eventList[indexPath.row - 1]
                        cell.initCell(eventItem)
                        cell.contentView.isHidden = false
                    }else{
                        cell.contentView.isHidden = true
                    }
                    
                    return cell
                }
            }
        }else if tableView == tblVideo {
            
            if let cell = tableView.dequeueReusableCell(withIdentifier: "videoCell") as? ItemVideoCell {
                if indexPath.row == 0 {
                    cell.controller = self
                    cell.initCell(self.recording)
                }else{
                    let videoItem = videoList[indexPath.row - 1]
                    cell.initCell(videoItem)
                }
                
                return cell
            }
            
        }else if tableView == tblAudio {
            
            if let cell = tableView.dequeueReusableCell(withIdentifier: "audioCell") as? ItemAudioCell {
                if indexPath.row < audioList.count {
                    let audioItem = audioList[indexPath.row]
                    cell.controller = self
                    cell.initCell(audioItem)
                    cell.contentView.isHidden = false
                }else{
                    cell.contentView.isHidden = true
                }
                
                return cell
            }
            
        }else if tableView == tblChat {
            
            if let cell = tableView.dequeueReusableCell(withIdentifier: "chatCell") as? ItemChatCell {
                if indexPath.row < chatMessageList.count {
                    let message = chatMessageList[indexPath.row]
                    let isDisplayDate = indexPath.row % 3 == 0
                    
                    cell.initCell(message, isDisplayDate: isDisplayDate)
                    cell.contentView.isHidden = false
                }else{
                    cell.contentView.isHidden = true
                }
                
                return cell
            }
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == tblEvents {
            
        }else if tableView == tblVideo {
            
        }else if tableView == tblAudio {
            
        }else if tableView == tblChat {
            
        }
    }
}


// MARK: Location Cell
class ItemLocationCell: UITableViewCell {
    
    @IBOutlet weak var viewSender: UIView!
    @IBOutlet weak var lblSenderLocation: UILabel!
    
    @IBOutlet weak var viewReceiver: UIView!
    @IBOutlet weak var lblReceiverLocation: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        selectionStyle = .none
    }
    
    func initCell(_ recording: Recording, receiverItem: RecordingItem?) {
        
        viewSender.isHidden = false
        viewReceiver.isHidden = false
        
        lblSenderLocation.text = String(format: "Longitude: %.6f\nLatitude: %.6f", recording.longitude, recording.latitude)
        
        if let strContent = receiverItem?.content {
            let receiverLocation = strContent.split(separator: ",")
            if receiverLocation.count == 2 , let longitude = Double(receiverLocation[0]), let latitude = Double(receiverLocation[1]) {
                lblReceiverLocation.text = String(format: "Longitude: %.6f\nLatitude: %.6f", longitude, latitude)
            }else{
                lblReceiverLocation.text = ""
            }
        }else{
            viewReceiver.isHidden = true
        }
    }
}

class ItemEventCell: UITableViewCell {
    
    @IBOutlet weak var lblSenderDate: UILabel!
    @IBOutlet weak var lblSenderMessage: UILabel!
    
    @IBOutlet weak var lblReceiverDate: UILabel!
    @IBOutlet weak var lblReceiverMessage: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        selectionStyle = .none
    }
    
    func initCell(_ item: RecordingItem) {
        
        lblSenderDate.isHidden = true
        lblSenderMessage.isHidden = true

        lblReceiverDate.isHidden = true
        lblReceiverMessage.isHidden = true
        
        if !item.isSender {
            lblSenderDate.isHidden = false
            lblSenderMessage.isHidden = false
            
            lblSenderDate.text = "-" + item.strCreatedAt
            lblSenderMessage.text = item.content
        }else{
            lblReceiverDate.isHidden = false
            lblReceiverMessage.isHidden = false
            
            lblReceiverDate.text = "-" + item.strCreatedAt
            lblReceiverMessage.text = item.content
        }
    }
}

class ItemVideoCell: UITableViewCell {
    
    @IBOutlet weak var viewSender: UIView!
    @IBOutlet weak var lblSenderDate: UILabel!
    @IBOutlet weak var viewSenderVideo: UIView!
    
    @IBOutlet weak var viewReceiver: UIView!
    @IBOutlet weak var lblReceiverDate: UILabel!
    @IBOutlet weak var viewReceiverVideo: UIView!
    
    weak var controller: BrowseRecordingVC?
    
    var senderVideoUrl: String = ""
    var receiverVideoUrl: String = ""
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        selectionStyle = .none
    }
    
    func initCell(_ item: RecordingItem) {
        
        viewSender.isHidden = false
        viewReceiver.isHidden = false
        
        lblSenderDate.text = "-" + item.strCreatedAt
        lblReceiverDate.text = "-" + item.strCreatedAt
        
        let conferenceId = item.content
        
        senderVideoUrl = ""
        receiverVideoUrl = ""
        
        if conferenceId.isEmpty == false, let appUser = AppData.shared.currentUser {
            senderVideoUrl = AppInfo.JitsiMeetServerURL + conferenceId + appUser.userId
            receiverVideoUrl = AppInfo.JitsiMeetServerURL + conferenceId + item.userId
        }
    }
    
    func initCell(_ recording: Recording) {
        
        viewSender.isHidden = false
        viewReceiver.isHidden = true
        
        lblSenderDate.text = recording.createdAt
        
        if let url = recording.videoURL {
            senderVideoUrl = url.absoluteString
        }else{
            viewSender.isHidden = true
        }
    }
    
    @IBAction func onPlaySenderVideo(_ sender: UIButton) {
        controller?.playRecordVideo(senderVideoUrl)
    }
    
    @IBAction func onPlayReceiverVideo(_ sender: UIButton) {
        controller?.playRecordVideo(receiverVideoUrl)
    }
}

class ItemAudioCell: UITableViewCell {
    
    @IBOutlet weak var viewSender: UIView!
    @IBOutlet weak var lblSenderDate: UILabel!
    @IBOutlet weak var viewSenderAudio: UIView!
    
    @IBOutlet weak var viewReceiver: UIView!
    @IBOutlet weak var lblReceiverDate: UILabel!
    @IBOutlet weak var viewReceiverAudio: UIView!
    
    weak var controller: BrowseRecordingVC?
    var senderAudioUrl: String = ""
    var receiverAudioUrl: String = ""
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        selectionStyle = .none
    }
    
    func initCell(_ item: RecordingItem) {
        
        viewSender.isHidden = false
        viewReceiver.isHidden = false
        
        lblSenderDate.text = "-" + item.strCreatedAt
        lblReceiverDate.text = "-" + item.strCreatedAt
        
        let conferenceId = item.content
        
        senderAudioUrl = ""
        receiverAudioUrl = ""
        
        if conferenceId.isEmpty == false, let appUser = AppData.shared.currentUser {
            senderAudioUrl = AppInfo.JitsiMeetServerURL + conferenceId + appUser.userId
            receiverAudioUrl = AppInfo.JitsiMeetServerURL + conferenceId + item.userId
        }
    }
    
    @IBAction func onPlaySenderAudio(_ sender: UIButton) {
        if senderAudioUrl.isEmpty == false {
            controller?.playRecordVideo(senderAudioUrl)
        }
    }
    @IBAction func onPlayReceiverAudio(_ sender: UIButton) {
        if receiverAudioUrl.isEmpty == false {
            controller?.playRecordVideo(receiverAudioUrl)
        }
    }
}

class ItemChatCell: UITableViewCell {
    
    @IBOutlet weak var viewSender: UIView!
    @IBOutlet weak var lblSenderDate: UILabel!
    @IBOutlet weak var lblSenderMessage: UILabel!
   
    
    @IBOutlet weak var viewReceiver: UIView!
    @IBOutlet weak var lblReceiverDate: UILabel!
    @IBOutlet weak var lblReceiverMessage: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        selectionStyle = .none
    }
    
    func initCell(_ message: DBMessage, isDisplayDate: Bool) {
        
        viewSender.isHidden = true
        viewReceiver.isHidden = true
        
        if message.currentUserIsSender() {
            viewSender.isHidden = false
            if isDisplayDate {
                lblSenderDate.text = "-" + Helper.timestampToDateString(timestamp: message.createdAt)
            }else{
                lblSenderDate.text = ""
            }
            
            lblSenderMessage.text = message.text
        }else{
            viewReceiver.isHidden = false
            
            if isDisplayDate {
                lblReceiverDate.text = "-" + Helper.timestampToDateString(timestamp: message.createdAt)
            }else{
                lblReceiverDate.text = ""
            }
            
            lblReceiverMessage.text = message.text
        }
    }
}

