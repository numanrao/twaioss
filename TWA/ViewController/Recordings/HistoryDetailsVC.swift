//
//  HistoryDetailsVC.swift
//  TWA
//
//  Created by MascSoft on 7/1/19.
//  Copyright © 2019 Telecare. All rights reserved.
//

import UIKit
import Photos
import AVFoundation
import AVKit

import Digger
import SVProgressHUD


class HistoryDetailsVC: UIViewController {

    // User Profile
    @IBOutlet weak var viewUserInfo: UIView!
    @IBOutlet weak var igvUserType: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var igvGender: UIImageView!
    
    @IBOutlet weak var btnDelete: UIButton!
    @IBOutlet weak var btnUserInfo: UIButton!
    
    @IBOutlet weak var tblDetails: UITableView!
    
    // Security View
    @IBOutlet weak var viewSecurity: UIView!
    
    @IBOutlet weak var lblQuestion1: UILabel!
    @IBOutlet weak var txfAnswer1: UITextField!
    
    @IBOutlet weak var lblQuestion2: UILabel!
    @IBOutlet weak var txfAnswer2: UITextField!
    
    var userInfo: User!
    var isAllMyRecordings: Bool = false
    
    var recordingList: [Recording] = []
    
    var selectedRecording: Recording?
    
    var txfPassword: UITextField?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        hideSecurityView()
        
        tblDetails.dataSource = self
        tblDetails.delegate = self
        tblDetails.tableFooterView = UIView(frame: .zero)
        
        initView()
//        self.navigationController?.navigationBar.setBackgroundImage(UIImage(named: "navbar"),
//        for: .default)
        
    }
    
    func initView() {
        if self.isAllMyRecordings || self.userInfo == nil {
            self.navigationItem.title = "All Recordings"
            viewUserInfo.isHidden = true
            
            callAPIAllRecordings()
        }else{
            self.navigationItem.title = "History Details"
            viewUserInfo.isHidden = false
            
            if userInfo.isProfessional {
                igvUserType.image = AppImages.professional
            }else{
                igvUserType.image = AppImages.customer
            }
            
            lblUserName.text = userInfo.name
            igvGender.image = userInfo.genderImage()
            
            if AppData.shared.isProfessionalMode() {
                btnUserInfo.isHidden = false
            }else{
                btnUserInfo.isHidden = true
            }
            
            callAPIUserRecordings()
        }
    }
    
    //MARK: User Actions
    @IBAction func onBack(_ sender: Any) {
        self.popVC(true)
    }
    
    @IBAction func onDelete(_ sender: Any) {
        let alert = UIAlertController(title: "Alert", message: "Do you want to clear communication history with \(userInfo.name)?", preferredStyle: .alert)
        let yes = UIAlertAction(title: "Yes", style: .default, handler: { action in
            self.callAPIRemoveHistory()
        })
        
        let no = UIAlertAction(title: "No", style: .default, handler: nil)
        
        alert.addAction(yes)
        alert.addAction(no)
        
        present(alert, animated: true, completion: nil)
    }
    
    @IBAction func onUserInfo(_ sender: Any) {
        if userInfo.isProfessional {
            if let vc = self.storyboard?.instantiateViewController(withIdentifier: "ProfessionalDetailVC") as? ProfessionalDetailVC {
                vc.user = userInfo
                vc.isFromContactList = false
                self.pushVC(vc, animated: true)
            }
        }else{
            if let vc = self.storyboard?.instantiateViewController(withIdentifier: "CustomerDetailVC") as? CustomerDetailVC {
                vc.user = userInfo
                vc.isFromContactList = false
                self.pushVC(vc, animated: true)
            }
        }
    }
    
    
    // Custom Fuctions
    
    func reloadRecordings(){
        if recordingList.isEmpty {
            tblDetails.showBackgroundViewWhenNil(msg: "No History")
        }else{
            tblDetails.showBackgroundViewWhenNil(msg: nil)
        }
        tblDetails.reloadData()
    }
    
    
    func playRecordingVideo(_ recording: Recording) {
        if let videoURL = recording.videoURL {
            let player = AVPlayer(url: videoURL)
            let playerViewController = AVPlayerViewController()
            playerViewController.player = player
            self.present(playerViewController, animated: true) {
                playerViewController.player!.play()
            }
        }else{
            showPromptAlert("Sorry!", message: "No Recording Video")
        }
    }
    
    // Delegation for Recording Cell
    func browseRecordingDetails(_ recording: Recording?) {
        guard let recordingObj = recording else {
            return
        }
        
        if isAllMyRecordings {
            playRecordingVideo(recordingObj)
        }else {
            if let vc = self.storyboard?.instantiateViewController(withIdentifier: "BrowseRecordingVC") as? BrowseRecordingVC {
                vc.userInfo = self.userInfo
                vc.recording = recordingObj
                
                self.pushVC(vc, animated: true)
            }
        }
        
    }
    func deleteRecording(_ recording: Recording?) {
        
        guard let recordingObj = recording else {
            return
        }
        
        if isAllMyRecordings {
            callAPIRemoveRecording(recordingObj)
        }else{
            callAPIRemoveUserRecording(recordingObj)
        }
    }
    
    func receiveRecording(_ recording: Recording?) {
        
        guard let recordingObj = recording else {
            return
        }
        
        self.selectedRecording = recording
        
        let alert = UIAlertController(title: "Alert", message: "Do you want to receive the recording(ID:\(recordingObj.recordingId)) to your local device?", preferredStyle: .alert)
        let yes = UIAlertAction(title: "Yes", style: .default, handler: { action in
            self.showInputPasswordAlert()
        })
        
        let no = UIAlertAction(title: "No", style: .default, handler: nil)
        
        alert.addAction(yes)
        alert.addAction(no)
        
        present(alert, animated: true, completion: nil)
    }
    
    func showInputPasswordAlert(){
        let alert = UIAlertController(title: "Alert", message: "Please input your password to receive recording.", preferredStyle: .alert)
        alert.addTextField { (textField) in
            textField.isSecureTextEntry = true
            textField.placeholder = "password"
            
            self.txfPassword = textField
        }
        
        let ok = UIAlertAction(title: "Ok", style: .default) { (action) in
            self.callAPICheckPassword(self.txfPassword?.text)
        }
        
        let cancel = UIAlertAction(title: "Cancel", style: .default, handler: nil)
        
        alert.addAction(cancel)
        alert.addAction(ok)
        
        self.present(alert, animated: true, completion: nil)
    }
    
    // MARK: Security View
    @IBAction func onSubmitAnswer(_ sender: Any) {
        if let answer1 = txfAnswer1.text, answer1.isEmpty == false, let answer2 = txfAnswer2.text, answer2.isEmpty == false {
            callAPICheckSecurity(answer1, answer2: answer2)
            self.view.endEditing(true)
        }else{
            self.showPromptAlert("Alert", message: "Please input answers.")
        }
    }
    
    @IBAction func onCancelAnswer(_ sender: Any) {
        hideSecurityView()
    }
    
    func showInputQuestionAlert(_ questions: [String]) {
        if questions.count == 0 {
            self.downloadRecording()
            return;
        }
        
        viewSecurity.isHidden = false
        lblQuestion1.text = questions[0]
        txfAnswer1.text = ""

        lblQuestion2.text = questions[1]
        txfAnswer2.text = ""
    }
    
    func hideSecurityView(){
        viewSecurity.isHidden = true
    }
    
    //MARK: Downloading Remote Recording Video from recording Id and Save it to Camera Roll
    func downloadRecording() {
        guard let videoUrl = self.selectedRecording?.videoURL else {
            return
        }
        
        download(videoUrl).progress({ (progress) in
            SVProgressHUD.showProgress(Float(progress.fractionCompleted), status: "Downloading...")
        }).speed({ (speed) in
            print(speed)
        }).completion { [weak self] (result) in
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
                switch result {
                case .success(let url) :
                    self?.saveVideoToCameraRoll(url)
                case .failure(let err):
                    self?.showPromptAlert("Alert", message: "Downloading was failed.")
                    SVProgressHUD.dismiss()
                    print(err)
                }
            }
        }
    }
    
    func saveVideoToCameraRoll(_ videoUrl: URL) {
        PHPhotoLibrary.shared().performChanges({
            PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: videoUrl)
        }) { (succes, err) in
            
            DispatchQueue.main.async {
                if(succes) {
                    self.showPromptAlert("Alert", message: "The recording was successful downloaded to Camera Roll.", okTitle: "OK")
                }else{
                    self.showPromptAlert("Alert", message: "Saving recording was failed.")
                    print(err)
                }
                SVProgressHUD.dismiss()
                DiggerCache.cleanDownloadFiles()
            }
        }
    }
}

extension HistoryDetailsVC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return recordingList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: "recordingCell") as? RecordingCell {
            
            cell.controller = self
            
            if indexPath.row < recordingList.count {
                let recording = recordingList[indexPath.row]
                cell.initCell(recording)
                cell.contentView.isHidden = false
            }else{
                cell.contentView.isHidden = true
            }
            
            return cell
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}

extension HistoryDetailsVC {
    
    func callAPIAllRecordings() {
        
        self.showProgressHUD()
        ServerHelper.shared.callPostAPI(ApiEndpoint.RECORDING_GET_ALL, parameters: [:], success: { [weak self] (json) in
            
            for recordingObj in json.arrayValue {
                self?.recordingList.append(Recording(recordingObj))
            }
            
            self?.hideProgressHUD()
            self?.reloadRecordings()
            
        }) { [weak self] (errMsg, errCode) in
            self?.hideProgressHUD()
            //self?.showPromptAlert("Sorry!", message: errMsg);
            
            self?.reloadRecordings()
        }
    }
    
    func callAPIUserRecordings() {
        
        self.showProgressHUD()
        
        let params = ["userId": userInfo.userId]
        
        ServerHelper.shared.callPostAPI(ApiEndpoint.RECORDING_RECORDINGS, parameters: params, success: { [weak self] (json) in
            
            for recordingObj in json.arrayValue {
                self?.recordingList.append(Recording(recordingObj))
            }
            
            self?.hideProgressHUD()
            self?.reloadRecordings()
            
        }) { [weak self] (errMsg, errCode) in
            self?.hideProgressHUD()
            //self?.showPromptAlert("Sorry!", message: errMsg);
            
            self?.reloadRecordings()
        }
    }
    
    func callAPIRemoveHistory() {
        
        self.showProgressHUD()
        let params = ["userId": userInfo.userId]
        
        ServerHelper.shared.callPostAPI(ApiEndpoint.RECORDING_REMOVE_USER, parameters: params, success: { [weak self] (json) in
            self?.hideProgressHUD()
            self?.recordingList.removeAll()
            self?.reloadRecordings()
            
            NotificationCenter.default.post(name: .AppRefreshRecordingList, object: nil)
        }) { [weak self] (errMsg, errCode) in
            self?.hideProgressHUD()
            self?.showPromptAlert("Sorry!", message: "Try again later.");
        }
    }
    
    func callAPIRemoveUserRecording(_ recObj: Recording?) {
        
        guard let recording = recObj else {
            return
        }
        
        self.showProgressHUD()
        let params = ["userId": userInfo.userId, "recordingId": recording.recordingId]
        
        ServerHelper.shared.callPostAPI(ApiEndpoint.RECORDING_REMOVE_USER_RECORDING, parameters: params, success: { [weak self] (json) in
            self?.hideProgressHUD()
            if let index = self?.recordingList.firstIndex(of: recording) {
                self?.recordingList.remove(at: index)
            }
            self?.reloadRecordings()
            
            self?.showPromptAlert("Alert", message: "Your recording was removed from your device")
        }) { [weak self] (errMsg, errCode) in
            self?.hideProgressHUD()
            self?.showPromptAlert("Sorry!", message: "Try again later.");
        }
    }
    
    func callAPIRemoveRecording(_ recObj: Recording?) {
        
        guard let recording = recObj else {
            return
        }
        
        self.showProgressHUD()
        let params = ["recordingId": recording.recordingId]
        
        ServerHelper.shared.callPostAPI(ApiEndpoint.RECORDING_REMOVE_RECORDING, parameters: params, success: { [weak self] (json) in
            self?.hideProgressHUD()
            if let index = self?.recordingList.firstIndex(of: recording) {
                self?.recordingList.remove(at: index)
            }
            self?.reloadRecordings()
            
            self?.showPromptAlert("Alert", message: "Your recording was removed from your device")
        }) { [weak self] (errMsg, errCode) in
            self?.hideProgressHUD()
            self?.showPromptAlert("Sorry!", message: "Try again later.");
        }
    }
    
    func callAPICheckPassword(_ password: String?) {
        guard let strPW = password, strPW.isEmpty == false else {
            self.showPromptAlert("Sorry!", message: "Password is empty")
            return
        }
        
        let params = ["password": strPW]
        
        showProgressHUD()
        ServerHelper.shared.callPostAPI(ApiEndpoint.USER_CHECK_PASSWORD, parameters: params, success: { [weak self] (json) in
            
            self?.hideProgressHUD()
            var questions: [String] = []
            for obj in json.arrayValue {
                if let question = obj.string {
                    questions.append(question)
                }
            }
            self?.showInputQuestionAlert(questions)
            
        }) { [weak self] (errMsg, errCode) in
            self?.hideProgressHUD()
            if case ErrorStatusCode.internalServer(let code) = errCode, code == 403 {
                self?.showPromptAlert("Sorry!", message: errMsg);
            }else{
                self?.showPromptAlert("Sorry!", message: "Server has error, try again later!");
            }
        }
    }
    
    func callAPICheckSecurity(_ answer1: String, answer2: String) {
        let params = ["answer1": answer1, "answer2": answer2]
        
        showProgressHUD()
        ServerHelper.shared.callPostAPI(ApiEndpoint.USER_CHECK_SECURITY, parameters: params, success: { [weak self] (json) in
            
            self?.hideProgressHUD()
            self?.hideSecurityView()
            self?.downloadRecording()
            
        }) { [weak self] (errMsg, errCode) in
            self?.hideProgressHUD()
            if case ErrorStatusCode.internalServer(let code) = errCode, code == 403 {
                self?.showPromptAlert("Sorry!", message: errMsg);
            }else{
                self?.showPromptAlert("Sorry!", message: "Server has error, try again later!");
            }
        }
    }
}

class RecordingCell: UITableViewCell {
    
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblRecordingID: UILabel!
    
    @IBOutlet weak var viewBrowse: UIView!
    @IBOutlet weak var viewReceive: UIView!
    @IBOutlet weak var viewExtend: UIView!
    @IBOutlet weak var viewDelete: UIView!
    
    weak var controller: HistoryDetailsVC?
    weak var recording: Recording?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    func initCell(_ recording: Recording) {
        
        self.recording = recording
        
        lblTime.text = recording.createdAt
        lblRecordingID.text = recording.recordingId
    }
    
    // MARK: User Actions
    @IBAction func onBrowse(_ sender: Any) {
        controller?.browseRecordingDetails(self.recording)
    }
    
    @IBAction func onReceive(_ sender: Any) {
        controller?.receiveRecording(self.recording)
    }
    
    @IBAction func onExtend(_ sender: Any) {
        
        let alert = UIAlertController(title: "Alert", message: "Your recording(ID:\(lblRecordingID.text!))'s storage term remain 2 days. Do you want to extend to store it?", preferredStyle: .alert)
        let yes = UIAlertAction(title: "Yes", style: .default, handler: { action in
            self.controller?.showPromptAlert("Alert", message: "Your extending require was sent successfully, please contact us for payment first.")
        })
        
        let no = UIAlertAction(title: "No", style: .default, handler: nil)
        
        alert.addAction(yes)
        alert.addAction(no)
        
        controller?.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func onDelete(_ sender: Any) {
        let alert = UIAlertController(title: "Alert", message: "The recording you selected will be removed from your device, Are you sure to delete it permanently?", preferredStyle: .alert)
        let yes = UIAlertAction(title: "Yes", style: .default, handler: { action in
            self.controller?.deleteRecording(self.recording)
        })
        
        let no = UIAlertAction(title: "No", style: .default, handler: nil)
        
        alert.addAction(yes)
        alert.addAction(no)
        
        controller?.present(alert, animated: true, completion: nil)
    }
    
}
