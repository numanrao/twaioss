//
//  RecordingsVC.swift
//  TWA
//
//  Created by MascSoft on 7/1/19.
//  Copyright © 2019 Telecare. All rights reserved.
//

import UIKit

class RecordingHistoryCell: UITableViewCell {
    
    @IBOutlet weak var igvPhoto: UIImageView!
    @IBOutlet weak var lblUserID: UILabel!
    @IBOutlet weak var igvGender: UIImageView!
    
    @IBOutlet weak var igvUserRole: UIImageView!
    @IBOutlet weak var lblUserRole: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        selectionStyle = .none
        
        igvPhoto.clipsToBounds = true
        igvPhoto.layer.cornerRadius = igvPhoto.bounds.width / 2
        igvPhoto.image = AppImages.avatarPlaceholder
    }
    
    func initCell(_ user: User) {
        
        igvPhoto?.sd_setImage(with: URL(string: user.photo), placeholderImage: AppImages.avatarPlaceholder, options: .refreshCached, context: nil)
        lblUserID.text = user.name
        igvGender.image = user.genderImage()
        
        if user.isProfessional {
            igvUserRole.image = AppImages.professional
            lblUserRole.textColor = AppColors.blue
            lblUserRole.text = "LEO"
        }else{
            igvUserRole.image = AppImages.customer
            lblUserRole.textColor = AppColors.red
            lblUserRole.text = "Citizen"
        }
    }
    
}

class RecordingsVC: UIViewController {

    @IBOutlet weak var tblHistory: UITableView!
    @IBOutlet weak var menuView: UIView!
    
    var userList: [User] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tblHistory.dataSource = self
        tblHistory.delegate = self
        tblHistory.tableFooterView = UIView(frame: .zero)
        
        tblHistory.addPullToRefresh {
            self.callAPIRecordingUsers(true)
        }
        
        showMenu(false)
        
        callAPIRecordingUsers(true)
        
        NotificationCenter.default.addObserver(self, selector: #selector(refreshRecordingList(_:)), name: .AppRefreshRecordingList, object: nil)
//        self.navigationController?.navigationBar.setBackgroundImage(UIImage(named: "navbar"),
//        for: .default)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    // Nofication Processing
    @objc func refreshRecordingList(_ notification: NSNotification) {
        callAPIRecordingUsers(false)
    }
    
    func showMenu(_ flag: Bool) {
        menuView.isHidden = !flag
    }

    @IBAction func onMore(_ sender: Any) {
        showMenu(menuView.isHidden)
    }
    
    @IBAction func onClearAll(_ sender: Any) {
        
        showMenu(false)
        
        let alert = UIAlertController(title: "Alert", message: "Do you want to clear all communication history?", preferredStyle: .alert)
        let yes = UIAlertAction(title: "Yes", style: .default, handler: { action in
            self.callAPIRemoveAll()
        })
        
        let no = UIAlertAction(title: "No", style: .default, handler: nil)
        
        alert.addAction(yes)
        alert.addAction(no)
        
        present(alert, animated: true, completion: nil)
    }
    
    @IBAction func onHideMenu(_ sender: Any) {
        showMenu(false)
    }
    
    func reloadUserList(){
        tblHistory.pullToRefreshView.stopAnimating()
        
        if userList.isEmpty {
            tblHistory.showBackgroundViewWhenNil(msg: "No History")
        }else{
            tblHistory.showBackgroundViewWhenNil(msg: nil)
        }
        tblHistory.reloadData()
    }
    
}

extension RecordingsVC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return userList.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var index = indexPath.row
        if index == 0 {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "myRecordingsCell") {
                return cell;
            }else{
                return UITableViewCell()
            }
        }
        
        index = index - 1
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: "historyCell") as? RecordingHistoryCell {
            if index < userList.count {
                let user = userList[index]
                cell.initCell(user)
                cell.contentView.isHidden = false
            }else{
                cell.contentView.isHidden = true
            }
            
            return cell
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "HistoryDetailsVC") as? HistoryDetailsVC {
            
            var index = indexPath.row
            if index == 0 {
                vc.userInfo = nil
                vc.isAllMyRecordings = true
            }else{
                index = index - 1
                vc.userInfo = userList[index]
            }
            
            self.pushVC(vc, animated: true)
        }
    }
}

extension RecordingsVC {
    
    func callAPIRecordingUsers(_ isHud: Bool) {
        
        if isHud {
            self.showProgressHUD()
        }
        
        ServerHelper.shared.callPostAPI(ApiEndpoint.RECORDING_USERS, parameters: [:], success: { [weak self] (json) in
            
            self?.userList.removeAll()
            
            for userObj in json.arrayValue {
                self?.userList.append(User(userObj))
            }
            
            self?.hideProgressHUD()
            self?.reloadUserList()
            
        }) { [weak self] (errMsg, errCode) in
            self?.hideProgressHUD()
            //self?.showPromptAlert("Sorry!", message: errMsg);
            
            self?.reloadUserList()
        }
    }
    
    func callAPIRemoveAll() {
        
        self.showProgressHUD()
        
        ServerHelper.shared.callPostAPI(ApiEndpoint.RECORDING_REMOVE_ALL, parameters: [:], success: { [weak self] (json) in
            self?.hideProgressHUD()
            
            self?.userList.removeAll()
            self?.reloadUserList()
            
        }) { [weak self] (errMsg, errCode) in
            self?.hideProgressHUD()
            self?.showPromptAlert("Sorry!", message: "Try again later.");
            
            self?.reloadUserList()
        }
    }
}
