//
//  AVConferenceVC.swift
//  TWA
//
//  Created by MascSoft on 7/29/19.
//  Copyright © 2019 Telecare. All rights reserved.
//

import UIKit
import JitsiMeet
class AVConferenceVC: UIViewController {

    static func getInstance() -> AVConferenceVC? {
        let storyboard = UIStoryboard.init(name: "Main", bundle: Bundle.main)
        return storyboard.instantiateViewController(withIdentifier: "AVConferenceVC") as? AVConferenceVC
    }
    
    var user: User? = AppData.shared.currentUser
    var conferenceType: String = "video" // audio or video
    var conferenceRoom: String? = nil
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var jitsiView: JitsiMeetView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        initView()
    }
    
    func initView() {
        
        jitsiView.delegate = self
        
        let isAudioOnly = (conferenceType == "video" ? true : false)
        let options = JitsiMeetConferenceOptions.fromBuilder { (builder) in
            builder.serverURL = URL(string: AppInfo.JitsiMeetServerURL)
            
            builder.audioOnly = isAudioOnly
            builder.audioMuted = false
            builder.videoMuted = false
            builder.welcomePageEnabled = false
        
            builder.userInfo = JitsiMeetUserInfo.init(displayName: self.user?.name, andEmail: self.user?.email, andAvatar: URL(string: self.user?.photo ?? ""))
            
            if let room = self.conferenceRoom {
                builder.room = room
            }else {
                builder.room = self.user?.userId
            }
        }
        
        jitsiView.join(options)
        
        if isAudioOnly {
            lblTitle.text = "Audio Conference"
        }else {
            lblTitle.text = "Video Conference"
        }
    }

    func goBack(){
        if let _ = self.navigationController {
            self.popVC(true)
        }else {
            self.dismiss(animated: true, completion: nil)
        }
    }
}

extension AVConferenceVC: JitsiMeetViewDelegate {
    
    func conferenceWillJoin(_ data: [AnyHashable : Any]!) {
        print("JitsiMeetView: Conference will join")
    }
    
    func conferenceJoined(_ data: [AnyHashable : Any]!) {
        print("JitsiMeetView: Conference joined")
    }
    
    func conferenceTerminated(_ data: [AnyHashable : Any]!) {
        print("JitsiMeetView: Conference terminated")
        
        self.goBack()
    }
    
}
