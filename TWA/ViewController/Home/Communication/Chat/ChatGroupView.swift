//
//  ChatVC.swift
//  TWA
//
//  Created by MascSoft on 8/9/19.
//  Copyright © 2019 Telecare. All rights reserved.
//

//-------------------------------------------------------------------------------------------------------------------------------------------------

import UIKit
import SDWebImage
import IQKeyboardManager

class ChatGroupView: RCMessagesView, UIGestureRecognizerDelegate {

    static func presentChatView(groupId: String, viewController: UIViewController) {
        let vc = ChatGroupView(groupId: groupId)
        let navVC = UINavigationController.init(rootViewController: vc)
        
        viewController.present(navVC, animated: true, completion: nil)
    }
    
    private var groupId = ""
	private var currentUser: User = AppData.shared.currentUser!
	
	private var dbmessages: [DBMessage] = []
	private var rcmessages: [String: RCMessage] = [:]
	private var avatarImages: [String: UIImage] = [:]
	private var avatarIds: [String] = []


	init(groupId groupId_: String) {

		super.init(nibName: "RCMessagesView", bundle: nil)

		groupId = groupId_
	}

	required init?(coder aDecoder: NSCoder) {

		super.init(coder: aDecoder)
	}

	override func viewDidLoad() {

		super.viewDidLoad()

        SocketHelper.shared.joinGroupChat(groupId: groupId, completion: nil)
        
		navigationController?.interactivePopGestureRecognizer?.delegate = self
        
        let backItem = UIBarButtonItem(image: UIImage(named: "nav-back"), style: .plain, target: self, action: #selector(actionBack))
        backItem.tintColor = UIColor.white
        
        navigationItem.leftBarButtonItem = backItem
        
        navigationController?.navigationBar.barTintColor = AppColors.blue
        labelTitle1.text = "Text Chatting"
        //labelTitle2.text = UserLastActive(dbuser: dbuser)
        
		NotificationCenterX.addObserver(target: self, selector: #selector(didReceiveNewMessage(_:)), name: NOTIFICATION_RECEIVE_GROUP_MESSAGE)

		loadMessages()
		refreshTableView2()
	}

    deinit {
        NotificationCenterX.removeObserver(target: self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        IQKeyboardManager.shared().isEnabled = false
        IQKeyboardManager.shared().isEnableAutoToolbar = false
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        IQKeyboardManager.shared().isEnabled = true
        IQKeyboardManager.shared().isEnableAutoToolbar = true
    }

	func loadMessages() {
        self.showProgressHUD()
        SocketHelper.shared.getGroupChatHistory(groupId: groupId, messageDate: nil) { (messages) in
            if let temp = messages {
                self.dbmessages = temp.reversed()
            }
            self.refreshTableView1()
            self.hideProgressHUD()
        }
	}
    
    //MARK: Notification Process
    @objc func didReceiveNewMessage(_ notification: Notification) {
        
        if let message = notification.object as? DBMessage, message.groupId == groupId {
            self.dbmessages.append(message)
            self.refreshTableView2()
        }
    }

	func dbmessage(_ indexPath: IndexPath) -> DBMessage {

		let index = indexPath.section
		return dbmessages[index]
	}

	func dbmessageAbove(_ indexPath: IndexPath) -> DBMessage? {

		if (indexPath.section > 0) {
			let indexAbove = IndexPath(row: 0, section: indexPath.section-1)
			return dbmessage(indexAbove)
		}
		return nil
	}

	// MARK: - Message methods
	override func rcmessage(_ indexPath: IndexPath) -> RCMessage {

		let dbmessage = self.dbmessage(indexPath)
		let messageId = dbmessage.messageId

		if (rcmessages[messageId] == nil) {

			var rcmessage: RCMessage!
			let incoming = (dbmessage.senderId != currentUser.userId)

			if (dbmessage.type == MESSAGE_STATUS) {
				rcmessage = RCMessage(status: dbmessage.text)
			}

			if (dbmessage.type == MESSAGE_TEXT) {
				rcmessage = RCMessage(text: dbmessage.text, incoming: incoming)
			}

			rcmessages[messageId] = rcmessage
		}

		return rcmessages[messageId]!
	}

	// MARK: - Avatar methods
	override func avatarInitials(_ indexPath: IndexPath) -> String {

		let dbmessage = self.dbmessage(indexPath)
        if dbmessage.senderId == currentUser.userId {
            return currentUser.nameInitials()
        }
        
		return dbmessage.sender?.nameInitials() ?? ""
	}

	override func avatarImage(_ indexPath: IndexPath) -> UIImage? {

		let dbmessage = self.dbmessage(indexPath)

		if (avatarImages[dbmessage.senderId] == nil) {
			loadAvatarImage(dbmessage)
		}

		return avatarImages[dbmessage.senderId]
	}

	func loadAvatarImage(_ dbmessage: DBMessage) {

		let userId = dbmessage.senderId

		if (avatarIds.contains(userId)) { return } else { avatarIds.append(userId) }

        var url: URL?
        
        if dbmessage.senderId == currentUser.userId {
            url = URL(string: currentUser.photo)
        }else if let photo = dbmessage.sender?.photo {
            url = URL(string: photo)
        }
        
        SDWebImageManager.shared.loadImage(with: url, options: [], progress: nil) { (img, data, err, cacheTyp, finished, url) in
            if let image = img {
                self.avatarImages[userId] = image
                self.tableView.reloadData()
            }
        }
	}

	// MARK: - Header, Footer methods
	override func textSectionHeader(_ indexPath: IndexPath) -> String? {

		if (indexPath.section % 3 == 0) {
			let dbmessage = self.dbmessage(indexPath)
			let date = Date.date(timestamp: dbmessage.createdAt)
			let dateFormatter = DateFormatter()
			dateFormatter.dateFormat = "dd MMMM, HH:mm"
			return dateFormatter.string(from: date)
		} else {
			return nil
		}
	}

	override func textBubbleHeader(_ indexPath: IndexPath) -> String? {

		let rcmessage = self.rcmessage(indexPath)
		if (rcmessage.incoming) {
			let dbmessage = self.dbmessage(indexPath)
			if let dbmessageAbove = self.dbmessageAbove(indexPath) {
				if (dbmessage.senderId == dbmessageAbove.senderId) {
					return nil
				}
			}
            if dbmessage.senderId == currentUser.userId {
                return currentUser.name
            }
            
			return dbmessage.sender?.name
		}
		return nil
	}

	override func textBubbleFooter(_ indexPath: IndexPath) -> String? {

		return nil
	}

	override func textSectionFooter(_ indexPath: IndexPath) -> String? {

		return nil
	}

	// MARK: - Menu controller methods

	override func menuItems(_ indexPath: IndexPath) -> [Any]? {
		return nil
	}

	override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {

		return false
	}


	override func typingIndicatorUpdate() {

	}

	// MARK: - Refresh methods
	@objc func refreshTableView1() {
		refreshTableView2()
		scroll(toBottom: true)
	}

	@objc func refreshTableView2() {

		loadEarlierShow(true)
        
		tableView.reloadData()
	}

	// MARK: - Message send methods
	func messageSend(_ text: String?, picture: UIImage?, video: URL?, audio: String?) {

        if let messageText = text {
            SocketHelper.shared.sendGroupMessage(messageText, group: groupId) { (message) in
                if let temp = message {
                    self.dbmessages.append(temp)
                    self.refreshTableView2()
                }
            }
        }
	}

	// MARK: - User actions
	@objc func actionBack() {
        SocketHelper.shared.leaveGroupChat(groupId: groupId, completion: nil)
        
        self.navigationController?.dismiss(animated: true, completion: nil)
		//navigationController?.popViewController(animated: true)
	}
    
	override func actionTitle() {

//        let groupView = GroupView(groupId: groupId)
//        navigationController?.pushViewController(groupView, animated: true)
	}

	override func actionAttachMessage() {
	}

	override func actionSendMessage(_ text: String) {

		messageSend(text, picture: nil, video: nil, audio: nil)
	}

	
	// MARK: - User actions (load earlier)
	override func actionLoadEarlier() {

        var messageDate: Int64? = nil
        if let message = dbmessages.first {
            messageDate = message.createdAt
        }
        SocketHelper.shared.getGroupChatHistory(groupId: groupId, messageDate: messageDate) { (messages) in
            
            if let temp = messages {
                self.dbmessages.insert(contentsOf: temp.reversed(), at: 0)
            }
            
            self.refreshTableView2()
        }
	}

	// MARK: - User actions (bubble tap)
	override func actionTapBubble(_ indexPath: IndexPath) {

	}

	// MARK: - User actions (avatar tap)
	override func actionTapAvatar(_ indexPath: IndexPath) {

		let dbmessage = self.dbmessage(indexPath)
		let senderId = dbmessage.senderId

		if (senderId != currentUser.userId) {
//            let profileView = ProfileView(userId: senderId, chat: false)
//            navigationController?.pushViewController(profileView, animated: true)
		}
	}

	// MARK: - Table view data source
	override func numberOfSections(in tableView: UITableView) -> Int {

		return dbmessages.count
	}

}
