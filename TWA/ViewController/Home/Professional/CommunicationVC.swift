//
//  CommunicationVC.swift
//  TWA
//
//  Created by MascSoft on 7/2/19.
//  Copyright © 2019 Telecare. All rights reserved.
//

import UIKit
import MessageUI

class CommunicationVC: UIViewController {

    var user: User?
    var isFromContactList: Bool = false
    
    @IBOutlet weak var igvContactLink: UIImageView!
    
    @IBOutlet weak var igvPhoto: UIImageView!
    @IBOutlet weak var lblUserID: UILabel!
    @IBOutlet weak var igvGender: UIImageView!
    
    @IBOutlet weak var menuView: UIView!
    
    @IBOutlet weak var menuContactDeleteView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        initView()
        showMenu(false)
    }
    
    func initView(){
        igvPhoto.clipsToBounds = true
        igvPhoto.layer.cornerRadius = igvPhoto.bounds.width / 2
        
        igvContactLink.isHidden = true
        
        if let tempUser = user {
            igvPhoto?.sd_setImage(with: URL(string: tempUser.photo), placeholderImage: AppImages.avatarPlaceholder, options: .refreshCached, context: nil)
        
            lblUserID.text = tempUser.username
            igvGender.image = tempUser.genderImage()
            
            igvContactLink.isHidden = false
            if tempUser.isContacted {
                igvContactLink.image = UIImage(named: "user-link")
            }else{
                igvContactLink.image = UIImage(named: "user-unlink")
            }
        }
        
        menuContactDeleteView.isHidden = true
        if isFromContactList {
            menuContactDeleteView.isHidden = false
        }
    }
    func showAlert() -> Bool{
        let refreshAlert = UIAlertController(title: "Notification", message: "Do you want to send the notification?", preferredStyle: UIAlertController.Style.alert)

        refreshAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
            //return true
        }))

        refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action: UIAlertAction!) in
           // return false
        }))

        present(refreshAlert, animated: true, completion: nil)
        return false
    }
    func showMenu(_ flag: Bool) {
        menuView.isHidden = !flag
    }
    
    @IBAction func onBack(_ sender: Any) {
        self.popVC(true)
    }
    
    @IBAction func onMore(_ sender: Any) {
        showMenu(menuView.isHidden)
    }
    
    @IBAction func onHideMenu(_ sender: Any) {
        showMenu(false)
    }
    
    @IBAction func onContact(_ sender: Any) {
        callAPIContactRequest()
    }
    
    @IBAction func onEmail(_ sender: Any) {
        sendEmail()
    }
    
    @IBAction func onAlert(_ sender: Any) {
        callAPIUrgencyRequest()
    }
    
    @IBAction func onVideo(_ sender: Any) {
        callAPIConferenceRequest("video")
    }
    
    @IBAction func onAudio(_ sender: Any) {
        callAPIConferenceRequest("audio")
    }
    
    @IBAction func onText(_ sender: Any) {
        callAPIConferenceRequest("private_chat")
    }
    
    /// Menu Actions
    
    @IBAction func onDeleteContact(_ sender: Any) {
        callAPIContactDelete()
    }
    
    func gotoConferencePage(_ type: String, conferenceId: String) {
        if type == "private_chat" {
            if let receiver = user {
                ChatPrivateView.presentChatView(receiver: receiver, groupId: conferenceId, viewController: self)
                //self.navigationController?.pushViewController(vc, animated: true)
            }
        }else {
            if let vc = AVConferenceVC.getInstance() {
                vc.conferenceType = type
                vc.conferenceRoom = conferenceId
                self.present(vc, animated: true, completion: nil)
                //self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
}

extension CommunicationVC {
    
    func callAPIContactRequest() {
       
        guard let receiverId = user?.userId else { return }
        
        let params = ["receivers": receiverId]
        
        self.showProgressHUD()
        ServerHelper.shared.callPostAPIWithRecording(ApiEndpoint.CONTACT_REQUEST, parameters: params, success: { [weak self] (json) in
            self?.hideProgressHUD()
            self?.showAlertForSuccess("Sent Contact Request!")
            
        }) { [weak self] (errMsg, errCode) in
            self?.hideProgressHUD()
            self?.showPromptAlert("Sorry!", message: errMsg);
        }
    }
    
    func callAPIContactDelete() {
        
        guard let userId = user?.userId else { return }
        
        let params = ["contactUser": userId]
        
        self.showProgressHUD()
        ServerHelper.shared.callPostAPI(ApiEndpoint.CONTACT_DELETE, parameters: params, success: { [weak self] (json) in
            self?.hideProgressHUD()
            //self?.showAlertForSuccess("Deleted from contact list.")
            
            NotificationCenter.default.post(name: .AppRefreshContactList, object: nil)
            self?.popVC(true)
        }) { [weak self] (errMsg, errCode) in
            self?.hideProgressHUD()
            self?.showPromptAlert("Sorry!", message: errMsg);
        }
    }
    
    func callAPIConferenceRequest(_ type: String) {
        
        guard let receiverId = user?.userId else { return }
        
        let params = ["receivers": receiverId, "type": type]
        
        self.showProgressHUD()
        ServerHelper.shared.callPostAPIWithRecording(ApiEndpoint.CONFERENCE_REQUEST, parameters: params, success: { [weak self] (json) in
            self?.hideProgressHUD()
            if let conferenceId = json["conferenceId"].string {
                self?.gotoConferencePage(type, conferenceId: conferenceId)
            }else {
                self?.showAlertForSuccess("Sorry! You have some errors in sending conference request. Try again later.")
            }
        }) { [weak self] (errMsg, errCode) in
            self?.hideProgressHUD()
            self?.showPromptAlert("Sorry!", message: errMsg);
        }
    }
    
    func callAPIUrgencyRequest() {
        
        guard let receiverId = user?.userId else { return }
        
        let params = ["receivers": receiverId]
        
        self.showProgressHUD()
        ServerHelper.shared.callPostAPIWithRecording(ApiEndpoint.URGENCY_ALERT_REQUEST, parameters: params, success: { [weak self] (json) in
            self?.hideProgressHUD()
            self?.showAlertForSuccess("Sent urgency alert!")
            
        }) { [weak self] (errMsg, errCode) in
            self?.hideProgressHUD()
            self?.showPromptAlert("Sorry!", message: errMsg);
        }
    }
    
}

extension CommunicationVC: MFMailComposeViewControllerDelegate {
    func sendEmail() {
        
        guard let emailAddress = user?.email else { return }
        
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients([emailAddress])
           // mail.setMessageBody("<p>You're so awesome!</p>", isHTML: true)
            
            present(mail, animated: true)
        } else {
            // show failure alert
            self.showPromptAlert("Sorry!", message: "You can't send email on this device. Please check your E-mail App.")
        }
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
}
