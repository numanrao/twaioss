//
//  ProfessionalDetailVC.swift
//  TWA
//
//  Created by MascSoft on 7/2/19.
//  Copyright © 2019 Telecare. All rights reserved.
//

import UIKit

class ProfessionalDetailVC: CommunicationVC {
    
    @IBOutlet weak var lblBadgeNumber: UILabel!
    @IBOutlet weak var lblRank: UILabel!
    @IBOutlet weak var lblPhoneNumber: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
        
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lblBadgeNumber.text = user?.badgeNumber
        lblRank.text = user?.rank
        lblPhoneNumber.text = user?.phoneNumber
        lblEmail.text = user?.email
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(named: "navbar"),
        for: .default)
    }
}
