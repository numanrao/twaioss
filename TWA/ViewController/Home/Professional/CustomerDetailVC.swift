//
//  CustomerDetailVC.swift
//  TWA
//
//  Created by MascSoft on 7/2/19.
//  Copyright © 2019 Telecare. All rights reserved.
//

import UIKit
import MessageUI
import PDFNet
class CustomerDetailVC: CommunicationVC {

    @IBOutlet weak var menuIssueView: UIView!
    var isEmail = false
    override func viewDidLoad() {
        super.viewDidLoad()

        menuIssueView.isHidden = true
        if !isFromContactList {
            menuIssueView.isHidden = false
        }
        if(isEmail){
            self.sendEmail()
        }
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(named: "navbar"),
        for: .default)
    }
    
    @IBAction func onSendPhotoID(_ sender: Any) {
        callAPISendPhotoID()
        self.sendEmail()
    }
    
    @IBAction func onIssueTicket(_ sender: Any) {
        self.sendEmail()
        if(showAlert()){
            callAPIIssueTicket()
        }
        
    }
    
    @IBAction func onIssueSummon(_ sender: Any) {
        
        self.sendEmail()
        if(showAlert()){
            callAPIIssueSummon()
        }
    }
    
}

extension CustomerDetailVC {
    func callAPISendPhotoID() {
        
        guard let receiverId = user?.userId else { return }
        
        let params = ["receiver": receiverId]
        
        self.showProgressHUD()
        ServerHelper.shared.callPostAPIWithRecording(ApiEndpoint.SEND_PHOTO_ID, parameters: params, success: { [weak self] (json) in
            self?.hideProgressHUD()
            self?.showAlertForSuccess("Sent photo ID.")
            
        }) { [weak self] (errMsg, errCode) in
            self?.hideProgressHUD()
            self?.showPromptAlert("Sorry!", message: errMsg);
        }
    }
    
    func callAPIIssueTicket() {
        
        guard let receiverId = user?.userId else { return }
        
        let params = ["receiver": receiverId]
        
        self.showProgressHUD()
        ServerHelper.shared.callPostAPIWithRecording(ApiEndpoint.ISSUE_TICKET, parameters: params, success: { [weak self] (json) in
            self?.hideProgressHUD()
            self?.showAlertForSuccess("Issue ticket.")
            
        }) { [weak self] (errMsg, errCode) in
            self?.hideProgressHUD()
            self?.showPromptAlert("Sorry!", message: errMsg);
        }
    }
    
    func callAPIIssueSummon() {
        
        guard let receiverId = user?.userId else { return }
        
        let params = ["receiver": receiverId]
        
        self.showProgressHUD()
        ServerHelper.shared.callPostAPIWithRecording(ApiEndpoint.ISSUE_SUMMON, parameters: params, success: { [weak self] (json) in
            self?.hideProgressHUD()
            self?.showAlertForSuccess("Issue summon.")
            
        }) { [weak self] (errMsg, errCode) in
            self?.hideProgressHUD()
            self?.showPromptAlert("Sorry!", message: errMsg);
        }
    }
}

