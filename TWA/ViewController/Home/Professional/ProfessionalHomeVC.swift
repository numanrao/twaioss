//
//  HomeVC.swift
//  TWA
//
//  Created by MascSoft on 7/1/19.
//  Copyright © 2019 Telecare. All rights reserved.
//

import UIKit
import SDWebImage
import FontAwesome_swift
import SVPullToRefresh
import MessageUI
enum UserFilterType {
    case all
    case customer
    case professional
    case contact
}

class ProfessionalHomeVC: UIViewController {
    
    @IBOutlet weak var tblUsers: UITableView!
    @IBOutlet weak var menuView: UIView!
    
    @IBOutlet weak var conferenceModeView: UIView!
    
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var headerActionsView: UIStackView!
    
    @IBOutlet weak var igvCheck: FontAwesomeImageView!
    @IBOutlet weak var lblCheck: UILabel!
    @IBOutlet weak var btnCheck: UIButton!
    
    @IBOutlet weak var layoutHeightHeader: NSLayoutConstraint!
    
    var offset: Int = 0
    var limitPerPage: Int = 30
    
    var userList: [User] = []
    var selectedList: [User] = []
    var filterType: UserFilterType = .all
    var isMultiSelection: Bool = false
    var isAllSelection: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        hideHeader(true)
        
        tblUsers.dataSource = self
        tblUsers.delegate = self
        tblUsers.tableFooterView = UIView(frame: .zero)
        
        tblUsers.addInfiniteScrolling { [weak self] in
            if self?.filterType != .contact {
                self?.loadNearbyUsers()
            }
        }
        tblUsers.showsInfiniteScrolling = true
        
        conferenceModeView.isHidden = true
        showMenu(false)
        
        loadInitialNearbyUsers()
        
        NotificationCenter.default.addObserver(self, selector: #selector(refreshContactList(_:)), name: .AppRefreshContactList, object: nil)
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(named: "navbar"),
        for: .default)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    // Nofication Processing
    @objc func refreshContactList(_ notification: NSNotification) {
        if filterType == .contact {
            if let user = notification.object as? User {
                userList.insert(user, at: 0)
                reloadUserList()
            }else {
                loadContactList(false)
            }
        }
    }
    
    func showMenu(_ flag: Bool) {
        menuView.isHidden = !flag
    }
    
    func reloadUserList(){
        if userList.isEmpty {
            var msg = "No Nearby users"
            if filterType == .customer {
                msg = "No Nearby Customers"
            }else if filterType == .professional {
                msg = "No Nearby Professionals"
            }else if filterType == .contact {
                msg = "No ContactList"
            }
            
            tblUsers.showBackgroundViewWhenNil(msg: msg)
        }else{
            tblUsers.showBackgroundViewWhenNil(msg: nil)
        }
        tblUsers.reloadData()
    }
    
    @IBAction func onBlast(_ sender: UIButton) {
        
        filterType = .professional
       // loadInitialNearbyUsers()
        var receivers: [String] = []
        
       
        for user in userList{
            if(user.isProfessional){
                receivers.append(user.userId)
            }
        }
        
        let params = ["receivers": receivers]
        
        self.showProgressHUD()
        ServerHelper.shared.callPostAPIWithRecording(ApiEndpoint.URGENCY_ALERT_REQUEST, parameters: params, success: { [weak self] (json) in
            self?.hideProgressHUD()
            self?.showAlertForSuccess("Sent urgency alert!")
            
        }) { [weak self] (errMsg, errCode) in
            self?.hideProgressHUD()
            self?.showPromptAlert("Sorry!", message: errMsg);
        }
        
        filterType = .all
        loadInitialNearbyUsers()
    }
    
    @IBAction func onMore(_ sender: Any) {
        showMenu(menuView.isHidden)
    }
    
    @IBAction func onFilterAll(_ sender: Any) {
        showMenu(false)
        if filterType == .all {
            return;
        }
        
        filterType = .all
        updateHeader()
        
        loadInitialNearbyUsers()
    }
    
    @IBAction func onFilterCustomers(_ sender: Any) {
        showMenu(false)
        if filterType == .customer {
            return;
        }
        
        filterType = .customer
        updateHeader()
        
        loadInitialNearbyUsers()
    }
    
    @IBAction func onFilterProfessionals(_ sender: Any) {
        showMenu(false)
        if filterType == .professional {
            return;
        }
        
        filterType = .professional
        
        updateHeader()
        
        loadInitialNearbyUsers()
    }
    
    @IBAction func onFilterContactList(_ sender: Any) {
        showMenu(false)
        if filterType == .contact {
            return;
        }
        
        filterType = .contact
        updateHeader()
        
        loadContactList(true)
    }
    
    @IBAction func onHideMenu(_ sender: Any) {
        showMenu(false)
    }
    
    
 // Header Actions & update
    
    func updateHeader(){
        if filterType == .professional {
            tblUsers.allowsMultipleSelection = false
            isMultiSelection = false
            isAllSelection = false
            hideHeader(false)
        }else{
            tblUsers.allowsMultipleSelection = false
            isMultiSelection = false
            hideHeader(true)
        }
        
        var strTitle = "Nearby Users"
        if filterType == .customer {
            strTitle = "Nearby Citizens"
        }else if filterType == .professional {
            strTitle = "Nearby LEOs"
        }else if filterType == .contact {
            strTitle = "Contact List"
        }
        
        self.navigationItem.title = strTitle
    }
    
    func hideHeader(_ flag: Bool) {
        layoutHeightHeader.constant = flag ? 0 : 50
        headerView.isHidden = flag
        
        updateHeaderActions()
    }

    func updateHeaderActions(){
        headerActionsView.isHidden = !isMultiSelection
        
        if isMultiSelection {
            if isAllSelection {
                igvCheck.cssCode = "fa-check-square"
            }else{
                igvCheck.cssCode = "fa-square"
            }
            igvCheck.prepareForInterfaceBuilder()
            lblCheck.text = "All"
        }else{
            igvCheck.cssCode = "fa-check-circle"
            igvCheck.prepareForInterfaceBuilder()
            lblCheck.text = "Multi"
        }
    }
    
    func updateAllChecked(index: Int,select: Bool) {
        if let selectionCount = tblUsers.indexPathsForSelectedRows?.count , selectionCount == userList.count {
            igvCheck.cssCode = "fa-check-square"
            isAllSelection = true
            selectedList = userList
        }else{
            igvCheck.cssCode = "fa-square"
            isAllSelection = false
            if(select){
                selectedList.append(userList[index])
            }else{
                let currentId = userList[index].userId
                var count = 0
                for user in selectedList{
                    if(user.userId == currentId){
                        selectedList.remove(at: count)
                    }
                    count += 1
                }
            }
        }
        
        igvCheck.prepareForInterfaceBuilder()
        lblCheck.text = "All"
    }
    
    @IBAction func onSelectMulti(_ sender: Any) {
        if isMultiSelection == false {
            isMultiSelection = true
            tblUsers.allowsMultipleSelection = true
            tblUsers.reloadData()
        }else if isAllSelection == false {
            isAllSelection = true
            
            let totalRows = self.tblUsers.numberOfRows(inSection: 0)
            for row in 0..<totalRows {
                self.tblUsers.selectRow(at: IndexPath(row: row, section: 0), animated: false, scrollPosition: .none)
            }
        }else {
            isMultiSelection = false
            isAllSelection = false
            
            tblUsers.allowsMultipleSelection = false
            tblUsers.reloadData()
        }
        
        updateHeaderActions()
    }
    
    @IBAction func onContact(_ sender: Any) {
        callAPIContactRequest()
    }
    
    @IBAction func onConference(_ sender: Any) {
        conferenceModeView.isHidden = false
    }
    
    @IBAction func onMail(_ sender: Any) {
        sendEmail()
    }
    
    @IBAction func onUrgent(_ sender: Any) {
        callAPIUrgencyRequest()
    }
    
    /// Conference Mode View
    
    @IBAction func onCloseConferenceMode(_ sender: Any) {
        conferenceModeView.isHidden = true
    }
    @IBAction func onVideoConference(_ sender: Any) {
        callAPIConferenceRequest("video")
    }
    @IBAction func onAudioConference(_ sender: Any) {
        callAPIConferenceRequest("audio")
    }
    @IBAction func onTextConference(_ sender: Any) {
        callAPIConferenceRequest("group_chat")
    }
    
    func gotoConferencePage(_ type: String, conferenceId: String) {
        if type == "group_chat" {
            ChatGroupView.presentChatView(groupId: conferenceId, viewController: self)
            //self.navigationController?.pushViewController(vc, animated: true)
        }else {
            if let vc = AVConferenceVC.getInstance() {
                vc.conferenceType = type
                vc.conferenceRoom = conferenceId
                self.present(vc, animated: true, completion: nil)
                //self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
}

extension ProfessionalHomeVC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return userList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: "userCell") as? UserCell {
            
            cell.isEnableMultiSelection = isMultiSelection
            
            if indexPath.row < userList.count {
                let user = userList[indexPath.row]
                cell.initCell(user)
                cell.contentView.isHidden = false
            }else{
                cell.contentView.isHidden = true
            }
            
            return cell
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row < userList.count {
            let user = userList[indexPath.row]
            
            if isMultiSelection {
                updateAllChecked(index: indexPath.row, select: true)
            }else {
                if user.isProfessional {
                    if let vc = self.storyboard?.instantiateViewController(withIdentifier: "ProfessionalDetailVC") as? ProfessionalDetailVC {
                        vc.user = user
                        vc.isFromContactList = (filterType == .contact ? true : false)
                        self.pushVC(vc, animated: true)
                    }
                }else{
                    if let vc = self.storyboard?.instantiateViewController(withIdentifier: "CustomerDetailVC") as? CustomerDetailVC {
                        vc.user = user
                        vc.isFromContactList = (filterType == .contact ? true : false)
                        self.pushVC(vc, animated: true)
                    }
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        if isMultiSelection {
            updateAllChecked(index: indexPath.row, select: false)
        }
    }
}



extension ProfessionalHomeVC {
    
    func loadInitialNearbyUsers(){
        userList.removeAll()
        offset = 0
        
        loadNearbyUsers()
    }
    
    func loadNearbyUsers(){
        
        var params: [String: Any] = ["offset": offset, "limit": limitPerPage]
        
        if filterType == .customer {
            params["role"] = "customer"
        }else if filterType == .professional {
            params["role"] = "professional"
        }
        
        if offset == 0 {
            showProgressHUD()
        }
        
        ServerHelper.shared.callPostAPI(ApiEndpoint.NEARBY_USERS, parameters: params, success: { [weak self] (json) in
            self?.hideProgressHUD()
            
            for userObj in json.arrayValue {
                self?.userList.append(User(userObj))
            }
            
            self?.offset += json.arrayValue.count
            
            self?.reloadUserList()
            
            self?.tblUsers.infiniteScrollingView.stopAnimating()
            
        }) { [weak self] (errMsg, errCode) in
            self?.hideProgressHUD()
            self?.showPromptAlert("Sorry!", message: errMsg);
            self?.tblUsers.infiniteScrollingView.stopAnimating()
            
            self?.reloadUserList()
        }
    }
    
    func loadContactList(_ isHUD: Bool) {
        userList.removeAll();
        
        if isHUD {
            self.showProgressHUD()
        }
        
        ServerHelper.shared.callPostAPI(ApiEndpoint.CONTACT_LIST, parameters: [:], success: { [weak self] (json) in
            
            for userObj in json.arrayValue {
                self?.userList.append(User(userObj))
            }
            
            self?.hideProgressHUD()
            self?.reloadUserList()
            
        }) { [weak self] (errMsg, errCode) in
            self?.hideProgressHUD()
            //self?.showPromptAlert("Sorry!", message: errMsg);
            
            self?.reloadUserList()
        }
    }
    
    func callAPIContactRequest() {
        
        guard let selectedRows = tblUsers.indexPathsForSelectedRows, selectedRows.count != 0 else { return }
        
        var receivers: [String] = []
        selectedRows.forEach { (indexPath) in
            if userList.count > indexPath.row {
                let userId = userList[indexPath.row].userId
                receivers.append(userId)
            }
        }
        
        let params = ["receivers": receivers]
        
        self.showProgressHUD()
        ServerHelper.shared.callPostAPIWithRecording(ApiEndpoint.CONTACT_REQUEST, parameters: params, success: { [weak self] (json) in
            self?.hideProgressHUD()
            self?.showAlertForSuccess("Sent Contact Request!")
            
        }) { [weak self] (errMsg, errCode) in
            self?.hideProgressHUD()
            self?.showPromptAlert("Sorry!", message: errMsg);
        }
    }
    
    func callAPIConferenceRequest(_ type: String) {
        
        guard let selectedRows = tblUsers.indexPathsForSelectedRows, selectedRows.count != 0 else { return }
        
        var receivers: [String] = []
        selectedRows.forEach { (indexPath) in
            if userList.count > indexPath.row {
                let userId = userList[indexPath.row].userId
                receivers.append(userId)
            }
        }
        
        let params: [String: Any] = ["receivers": receivers, "type": type]
        
        self.showProgressHUD()
        ServerHelper.shared.callPostAPIWithRecording(ApiEndpoint.CONFERENCE_REQUEST, parameters: params, success: { [weak self] (json) in
            self?.hideProgressHUD()
            if let conferenceId = json["conferenceId"].string {
                self?.gotoConferencePage(type, conferenceId: conferenceId)
            }else {
                self?.showAlertForSuccess("Sorry! You have some errors in sending conference request. Try again later.")
            }
        }) { [weak self] (errMsg, errCode) in
            self?.hideProgressHUD()
            self?.showPromptAlert("Sorry!", message: errMsg);
        }
    }
    
    func callAPIUrgencyRequest() {
        
        guard let selectedRows = tblUsers.indexPathsForSelectedRows, selectedRows.count != 0 else { return }
        
        var receivers: [String] = []
        selectedRows.forEach { (indexPath) in
            if userList.count > indexPath.row {
                let userId = userList[indexPath.row].userId
                receivers.append(userId)
            }
        }
        
        let params = ["receivers": receivers]
        
        self.showProgressHUD()
        ServerHelper.shared.callPostAPIWithRecording(ApiEndpoint.URGENCY_ALERT_REQUEST, parameters: params, success: { [weak self] (json) in
            self?.hideProgressHUD()
            self?.showAlertForSuccess("Sent urgency alert!")
            
        }) { [weak self] (errMsg, errCode) in
            self?.hideProgressHUD()
            self?.showPromptAlert("Sorry!", message: errMsg);
        }
    }
}



class UserCell: UITableViewCell {
    
    @IBOutlet weak var igvPhoto: UIImageView!
    @IBOutlet weak var lblUserID: UILabel!
    @IBOutlet weak var igvGender: UIImageView!
    
    @IBOutlet weak var igvUserRole: UIImageView!
    @IBOutlet weak var lblUserRole: UILabel!
    
    @IBOutlet weak var lblDistance: UILabel!
    @IBOutlet weak var igvArrow: UIImageView!
    
    var isEnableMultiSelection: Bool = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.selectionStyle = .none
        
        igvPhoto.clipsToBounds = true
        igvPhoto.layer.cornerRadius = igvPhoto.bounds.width / 2
    }
    
    func initCell(_ user: User) {
        
        igvPhoto?.sd_setImage(with: URL(string: user.photo), placeholderImage: AppImages.avatarPlaceholder, options: .refreshCached, context: nil)
        lblUserID.text = user.name
        igvGender.image = user.genderImage()
        
        if user.isProfessional {
            igvUserRole.image = AppImages.professional
            lblUserRole.textColor = AppColors.blue
            lblUserRole.text = "LEO"
        }else{
            igvUserRole.image = AppImages.customer
            lblUserRole.textColor = AppColors.red
            lblUserRole.text = "Citizen"
        }
        
        lblDistance.text = String(format: "%.1fm", user.distance)
        
        if isEnableMultiSelection {
            igvArrow.isHidden = true
        }else{
            igvArrow.isHidden = false
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        if isEnableMultiSelection && selected {
            self.contentView.backgroundColor = AppColors.userSelected
        }else{
            self.contentView.backgroundColor = .white
        }
    }
}
extension ProfessionalHomeVC: MFMailComposeViewControllerDelegate {
    func sendEmail() {
        
        var emailAddress = [String]()
        for user in self.selectedList{
            emailAddress.append(user.email)
        }
        
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients(emailAddress)
           // mail.setMessageBody("<p>You're so awesome!</p>", isHTML: true)
            
            present(mail, animated: true)
        } else {
            // show failure alert
            self.showPromptAlert("Sorry!", message: "You can't send email on this device. Please check your E-mail App.")
        }
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
}
