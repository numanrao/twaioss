//
//  CustomerHomeVC.swift
//  TWA
//
//  Created by MascSoft on 7/8/19.
//  Copyright © 2019 Telecare. All rights reserved.
//

import UIKit
import SVPullToRefresh
import MessageUI
import Braintree
class ProfessionalCell: UITableViewCell {
    
    @IBOutlet weak var igvPhoto: UIImageView!
    @IBOutlet weak var lblUserID: UILabel!
    @IBOutlet weak var igvGender: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.selectionStyle = .none
        
        igvPhoto.clipsToBounds = true
        igvPhoto.layer.cornerRadius = igvPhoto.bounds.width / 2
    }
    
    func initCell(_ user: User) {
        
        igvPhoto?.sd_setImage(with: URL(string: user.photo), placeholderImage: AppImages.avatarPlaceholder, options: .refreshCached, context: nil)
        lblUserID.text = user.name
    }
}

class CustomerHomeVC: UIViewController {
    
    
    
    
    @IBOutlet weak var tblUsers: UITableView!
   
    var offset: Int = 0
    var limitPerPage: Int = 30
    
    var userList: [User] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        tblUsers.dataSource = self
        tblUsers.delegate = self
        tblUsers.tableFooterView = UIView(frame: .zero)
        
        tblUsers.addPullToRefresh {
            self.loadProfessionalContacts(false)
        }
        tblUsers.showsPullToRefresh = true
        
        loadProfessionalContacts(true)
        
        NotificationCenter.default.addObserver(self, selector: #selector(refreshContactList(_:)), name: .AppRefreshContactList, object: nil)
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    // Nofication Processing
    @objc func refreshContactList(_ notification: NSNotification) {
        if let user = notification.object as? User {
            userList.insert(user, at: 0)
            reloadUsers()
        }else {
            loadProfessionalContacts(false)
        }
    }
    
    func reloadUsers(){
        if userList.isEmpty {
            tblUsers.showBackgroundViewWhenNil(msg: "You don't have any contacted LEOs")
        }else{
            tblUsers.showBackgroundViewWhenNil(msg: nil)
            tblUsers.reloadData()
        }
    }
}

extension CustomerHomeVC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return userList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: "professionalCell") as? ProfessionalCell {
            
            if indexPath.row < userList.count {
                let user = userList[indexPath.row]
                cell.initCell(user)
            }
            
            return cell
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row < userList.count {
            let user = userList[indexPath.row]
            sendEmail(user)
        }
    }
}



extension CustomerHomeVC {

    func loadProfessionalContacts(_ isProgress: Bool) {
        userList.removeAll();
        
        if isProgress {
            showProgressHUD()
        }
        
        ServerHelper.shared.callPostAPI(ApiEndpoint.CONTACT_LIST, parameters: [:], success: { [weak self] (json) in
            
            for userObj in json.arrayValue {
                self?.userList.append(User(userObj))
            }
            
            self?.hideProgressHUD()
            self?.tblUsers.pullToRefreshView.stopAnimating()
            
            self?.reloadUsers()
            
        }) { [weak self] (errMsg, errCode) in
            self?.hideProgressHUD()
            self?.tblUsers.pullToRefreshView.stopAnimating()
            
            //self?.showPromptAlert("Sorry!", message: errMsg);
            
            self?.reloadUsers()
        }
    }
    
}

extension CustomerHomeVC: MFMailComposeViewControllerDelegate {
    func sendEmail(_ user: User) {
        
        guard user.email.isEmpty == false else { return }
        
        let emailAddress = user.email
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients([emailAddress])
            // mail.setMessageBody("<p>You're so awesome!</p>", isHTML: true)
            
            present(mail, animated: true)
        } else {
            // show failure alert
            self.showPromptAlert("Sorry!", message: "You can't send email on this device. Please check your E-mail App.")
        }
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
}
