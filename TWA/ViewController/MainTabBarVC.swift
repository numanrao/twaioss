//
//  MainTabBarVC.swift
//  TWA
//
//  Created by MascSoft on 7/1/19.
//  Copyright © 2019 Telecare. All rights reserved.
//

import UIKit

class MainTabBarVC: UITabBarController {

    static func getInstance() -> MainTabBarVC? {
        let storyboard = UIStoryboard.init(name: "Main", bundle: Bundle.main)
        return storyboard.instantiateViewController(withIdentifier: "MainTabBarVC") as? MainTabBarVC
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        UITabBar.appearance().tintColor = .white
        
        self.tabBar.tintColor = AppColors.tabTint
        self.tabBar.unselectedItemTintColor = .white
        
        if let navVC = self.viewControllers?[1] as? UINavigationController {
            if AppData.shared.isProfessionalMode() {
                if let vc = self.storyboard?.instantiateViewController(withIdentifier: "ProfessionalHomeVC") {
                    navVC.setViewControllers([vc], animated: true)
                }
            }else{
                if let vc = self.storyboard?.instantiateViewController(withIdentifier: "CustomerHomeVC") {
                    navVC.setViewControllers([vc], animated: true)
                }
            }
        }
        
        self.selectedIndex = 1
    }
    
}
