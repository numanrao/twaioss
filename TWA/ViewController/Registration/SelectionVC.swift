//
//  SelectionVC.swift
//  TWA
//
//  Created by MascSoft on 7/7/19.
//  Copyright © 2019 Telecare. All rights reserved.
//

import UIKit

enum SelectionType {
    case rank
    case question1
    case question2
}

protocol SelectionVCDelegate: NSObject {
    func selectionVC(_ type: SelectionType, selected: String)
}

class SelectionVC: UIViewController {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var tblOptions: UITableView!
    
    var strTitle: String? = "Select One"
    var options: [String] = []
    var selectionType: SelectionType = .rank
    
    var delegate: SelectionVCDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        lblTitle.text = strTitle
        
        tblOptions.dataSource = self
        tblOptions.delegate = self
        tblOptions.tableFooterView = UIView(frame: .zero)
    }
    
    @IBAction func onBack(_ sender: Any) {
        self.popVC(true)
    }
}

extension SelectionVC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return options.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier:
            "selectionCell") {
            cell.textLabel?.text = options[indexPath.row]
            return cell
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.delegate?.selectionVC(selectionType, selected: options[indexPath.row])
        self.popVC(true)
    }
}
