//
//  LoginVC.swift
//  TWA
//
//  Created by MascSoft on 7/1/19.
//  Copyright © 2019 Telecare. All rights reserved.
//

import UIKit

class LoginVC: UIViewController {

    @IBOutlet weak var txfUserID: UITextField!
    @IBOutlet weak var txfPassword: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        initView()
    }
    
    func initView(){
        txfUserID.delegate = self
        txfPassword.delegate = self
    }
    static func getInstance() -> LoginVC? {
        let storyboard = UIStoryboard.init(name: "Main", bundle: Bundle.main)
        return storyboard.instantiateViewController(withIdentifier: "LoginVC") as? LoginVC
    }
    @IBAction func forgotPasswordPressed(_ sender: RoundButton) {
        
        let alertController = UIAlertController(title: "Enter Email", message: "Please enter your associated email address", preferredStyle: UIAlertController.Style.alert)
        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = "Enter Email:"
        }
        let saveAction = UIAlertAction(title: "Submit", style: UIAlertAction.Style.default, handler: { alert -> Void in
            let firstTextField = alertController.textFields![0] as UITextField
            
        })
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: {
            (action : UIAlertAction!) -> Void in })
        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = "Enter Email:"
        }

        alertController.addAction(saveAction)
        alertController.addAction(cancelAction)

        self.present(alertController, animated: true, completion: nil)
    }
    //MARK: User Actions
    @IBAction func onSubmit(_ sender: Any) {
        let params = ["username": txfUserID.text!, "password": txfPassword.text!];
        showProgressHUD()
        ServerHelper.shared.callPostAPI(ApiEndpoint.LOGIN, parameters: params, success: { (json) in
            self.hideProgressHUD()
            
            let user = User.init(json["user"])
            let token = json["token"].stringValue
            
            AppData.shared.userToken = token
            AppData.shared.currentUser = user
            AppData.shared.registerDevice()
            
            if let vc = AppMainVC.getInstance() {
                UserDefaults.standard.set(self.txfPassword.text, forKey: "password")
                self.pushVC(vc, animated: true)
            }
        }) { (errMsg, errCode) in
            self.hideProgressHUD()
            self.showPromptAlert("Sorry!", message: errMsg);
        }
    }
    
    @IBAction func onCancel(_ sender: Any) {
        self.popVC(true)
    }
}

extension LoginVC: UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if(textField == txfPassword) {
            textField.resignFirstResponder()
        }
        
        return true
    }
}
