//
//  AppStartVC.swift
//  TWA
//
//  Created by MascSoft on 7/7/19.
//  Copyright © 2019 Telecare. All rights reserved.
//

import UIKit

class AppStartVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func onLogin(_ sender: Any) {
    }
    
    @IBAction func onRegisterProfessional(_ sender: Any) {
        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "RegisterVC") as? RegisterVC {
            vc.isProfessional = true
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func onRegisterCustomer(_ sender: Any) {
        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "RegisterVC") as? RegisterVC {
            vc.isProfessional = false
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
}
