//
//  RegisterVC.swift
//  TWA
//
//  Created by MascSoft on 7/1/19.
//  Copyright © 2019 Telecare. All rights reserved.
//

import UIKit
import iOSDropDown
import Braintree
class RegisterVC: UIViewController {

    var isProfessional: Bool = false
    var userPhoto: UIImage? = nil
    
    @IBOutlet weak var txfUserID: UITextField!
    @IBOutlet weak var txfName: UITextField!
    
    @IBOutlet weak var btnMale: UIButton!
    @IBOutlet weak var btnFemale: UIButton!
    
    @IBOutlet weak var btnPhoto: RoundButton!
    @IBOutlet weak var igvPhoto: UIImageView!
    
    @IBOutlet weak var txfPassword: UITextField!
    @IBOutlet weak var txfConfirmPassword: UITextField!
    
    @IBOutlet weak var txfPhoneNumber: UITextField!
    
    @IBOutlet weak var txfEmail: UITextField!
    
    @IBOutlet weak var badgeView: UIView!
    @IBOutlet weak var txfBadge: UITextField!
    
    @IBOutlet weak var rankView: UIView!
    @IBOutlet weak var rankDropDown: UITextField!
    
    @IBOutlet weak var questionDropDown1: UITextField!
    @IBOutlet weak var txfAnswer1: UITextField!
    @IBOutlet weak var btnAddQuestion1: UIButton!
    
    @IBOutlet weak var questionDropDown2: UITextField!
    @IBOutlet weak var txfAnswer2: UITextField!
    @IBOutlet weak var btnAddQuestion2: UIButton!
    var braintreeClient: BTAPIClient?
    var isSelectedAddQuestion1: Bool = false
    
    var questions = AppData.shared.defaultQuestions
    
    override func viewDidLoad() {
        super.viewDidLoad()

        initView()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.view.endEditing(true)
    }
    
    func initView(){
        
        igvPhoto.layer.cornerRadius = igvPhoto.bounds.width / 2
        igvPhoto.clipsToBounds = true
        
        rankDropDown.placeholder = "Select your rank"
        questionDropDown1.placeholder = "Select question"
        questionDropDown2.placeholder = "Select question"
        
        if isProfessional {
            badgeView.isHidden = false
            rankView.isHidden = false
        }else {
            badgeView.isHidden = true
            rankView.isHidden = true
        }
        btnMale.tintColor = UIColor.white
        btnFemale.tintColor = UIColor.white
        btnAddQuestion1.tintColor = UIColor.white
        btnAddQuestion2.tintColor = UIColor.white
        showPromptAlert("", message: "We encourage you to register with TWA even prior to all the potential services being immediately available.  By registering early,Law Enforcement will be able to see your TWA decal on your vehicle and know that you have taken the pledge to cooperate and respect their authority.  You will also, be able to record and receive a certified copy of your interaction with law enforcement.To maximize all of TWA's benefits for safety, transparency, and accountability, we are working with governments and local law enforcement to register with TWA.Law enforcement will receive training, education, and support on how to use the TWA technology.   This training will help police reciprocate the public's pledge and gesture to respect and cooperate with them.We are in the process of communicating with governments, community leaders, organizations, and internet service providers, etc. to encourage their full participation and cooperation to provide the best service possible.  Our primary objective is to reduce the risk of violence and use of excessive and fatal force between police and the public.We believe that reducing the violence is a fundamental necessity for real progress in the relationship between police and the community.   It will allow for the creation of a foundation to have meaningful discussions and establish relationships of understanding and mutual respect between police and the public.As a consequence of pursuing these preparations, there may be a delay in the full benefit of all of the TWA services in your community.  However, you are encouraged to register immediately with TWA to obtain founding member status.As a founding member, while preparations are being completed, you will (a) have the use of TWA services for FREE (i.e., use of video recording, certified copies of recordings, illuminated decal identification, access to the TWA/Third Witness website, blog, and newsletter); (b) a lifetime membership and special discounted monthly subscription rate of $4.95 a month; and (c) one month of free service after preparations are completed and full service is available.Your account will not be charged during the preparation free trial period.  We will provide you with a formal 30 day notice in advance of the beginning of your monthly subscription payment.  You can cancel your subscription at any time with prior 30 day notice.")
    }
    
//    func initDropDownView(){
//
//
//        rankDropDown.placeholder = "Select your rank"
//        rankDropDown.optionArray = ["5","4","3","2","1"]
//        //Its Id Values and its optional
//        //rankDropDown.optionIds = [1,23,54,22]
//        // The the Closure returns Selected Index and String
//        rankDropDown.didSelect{(selectedText , index ,id) in
//            //let text = "Selected String: \(selectedText) \n index: \(index)"
//        }
//
//        questionDropDown1.placeholder = "Select question"
//        questionDropDown1.optionArray = questions
//
//        // The the Closure returns Selected Index and String
//        questionDropDown1.didSelect{(selectedText , index ,id) in
//            //let text = "Selected String: \(selectedText) \n index: \(index)"
//        }
//
//        questionDropDown2.placeholder = "Select question"
//        questionDropDown2.optionArray = questions
//
//        // The the Closure returns Selected Index and String
//        questionDropDown2.didSelect{(selectedText , index ,id) in
//            //let text = "Selected String: \(selectedText) \n index: \(index)"
//        }
//
//    }
    
    //MARK: UIActions
    @IBAction func onBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onCancel(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onSubmit(_ sender: Any) {
        if checkValidation() {
            self.view.endEditing(true)
            if userPhoto != nil {
            callAPIRegisterUser()
            }
            else {
                showPromptAlert("Alert", message: "Please Select Profile Pic", okTitle: "OK")
            }
        }
    }
    
   
    @IBAction func onMale(_ sender: Any) {
        btnMale.isSelected = true
        btnFemale.isSelected = false
    }
    @IBAction func onFemale(_ sender: Any) {
        btnMale.isSelected = false
        btnFemale.isSelected = true
    }

    @IBAction func onPhoto(_ sender: Any) {
        selectPhoto()
    }
    
    @IBAction func onRank(_ sender: Any) {
        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "SelectionVC") as? SelectionVC {
            vc.strTitle = "Select your rank"
            vc.options = ["5","4","3","2","1"]
            vc.delegate = self
            vc.selectionType = .rank
            
            self.pushVC(vc, animated: true)
        }
    }
    
    @IBAction func onQuestion1(_ sender: Any) {
        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "SelectionVC") as? SelectionVC {
            vc.strTitle = "Select question"
            vc.options = questions
            vc.delegate = self
            vc.selectionType = .question1
            
            self.pushVC(vc, animated: true)
        }
    }
    
    @IBAction func onQuestion2(_ sender: Any) {
        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "SelectionVC") as? SelectionVC {
            vc.strTitle = "Select question"
            vc.options = questions
            vc.delegate = self
            vc.selectionType = .question2
            
            self.pushVC(vc, animated: true)
        }
    }
    
    @IBAction func onAddQuestion(_ sender: UIButton) {
        
        if sender == btnAddQuestion1 {
            isSelectedAddQuestion1 = true
        }else{
            isSelectedAddQuestion1 = false
        }
        
        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddQuestionVC") as? AddQuestionVC {
            vc.delegate = self
            vc.modalTransitionStyle = .crossDissolve
            vc.modalPresentationStyle = .overCurrentContext

            self.present(vc, animated: true, completion: nil)
        }
    }
    
    //MARK:
    func checkValidation() -> Bool {
        
        var errMsg: String? = nil
        
        if Helper.isEmptyString(txfUserID.text) {
            errMsg = "Username is empty"
        }else if Helper.isEmptyString(txfName.text) {
            errMsg = "Name is empty"
        }else if Helper.isEmptyString(txfPassword.text) {
            errMsg = "Password is empty"
        }else if !Helper.isValidPassword(txfPassword.text!) {
            errMsg = "Password is Invalid"
        }else if Helper.isEmptyString(txfConfirmPassword.text) {
            errMsg = "Confirm Password is empty"
        }else if txfPassword.text! != txfConfirmPassword.text! {
            errMsg = "Password does not match"
        }else if Helper.isEmptyString(txfPhoneNumber.text) {
            errMsg = "Phone number is empty"
        }else if Helper.isEmptyString(txfEmail.text) {
            errMsg = "Email is empty"
        }else if !Helper.isValidEmailAddress(txfEmail.text!) {
            errMsg = "Email is Invalid"
        }else if Helper.isEmptyString(questionDropDown1.text) {
            errMsg = "Please set up security Question1"
        }else if Helper.isEmptyString(txfAnswer1.text) {
            errMsg = "Please set up security Answer1"
        }else if Helper.isEmptyString(questionDropDown2.text) {
            errMsg = "Please set up security Question2"
        }else if Helper.isEmptyString(txfAnswer2.text) {
            errMsg = "Please set up security Answer2"
        }
        
        if errMsg != nil && isProfessional {
            if Helper.isEmptyString(txfBadge.text) {
                errMsg = "Badge number is empty"
            }else if Helper.isEmptyString(rankDropDown.text) {
                errMsg = "Rank is empty"
            }
        }
        
        
        if let str = errMsg {
            showPromptAlert("Sorry,", message: str)
            return false
        }
        
        return true
    }
    
    //MARK : Select User Photo
    func selectPhoto() {
        
        let alert = UIAlertController(title: "Select User Photo", message: "", preferredStyle: .actionSheet)
        
        let photos = UIAlertAction(title: "Photo Gallery", style: .default) { (action) in
            self.openPhotoGallery()
        }
        
        let camera = UIAlertAction(title: "Camera", style: .default) { (action) in
            self.openCamera()
        }
        
        let cancel = UIAlertAction(title: "Cancel", style: .destructive, handler: nil)
        
        alert.addAction(photos)
        alert.addAction(camera)
        alert.addAction(cancel)
        
        alert.popoverPresentationController?.sourceView = self.btnPhoto
        
        self.present(alert, animated: true, completion: nil)
    }
    
    let imagePicker = UIImagePickerController()
    
    func openCamera(){
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            imagePicker.delegate = self
            imagePicker.allowsEditing = false
            imagePicker.sourceType = .camera
            
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func openPhotoGallery(){
        imagePicker.delegate = self
        imagePicker.allowsEditing = true
        imagePicker.sourceType = .photoLibrary
        
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    func callAPIRegisterUser(){
        var params: [String : String] = ["username": txfUserID.text!,
                      "name": txfName.text!,
                      "password": txfPassword.text!,
                      "confirmPassword": txfConfirmPassword.text!,
                      "phoneNumber": txfPhoneNumber.text!,
                      "email": txfEmail.text!,
                      "question1": questionDropDown1.text!,
                      "answer1": txfAnswer1.text!,
                      "question2": questionDropDown2.text!,
                      "answer2": txfAnswer2.text!,
                      "precinct": "none"
                    ];
        
        if self.btnMale.isSelected {
            params["gender"] = "0"
        }else{
            params["gender"] = "1"
        }
        
        if isProfessional {
            params["badgeNumber"] = txfBadge.text!
            params["rank"] = rankDropDown.text!
            params["role"] = "professional"
        }else{
            params["role"] = "customer"
        }
        
        if let location = LocationHelper.shared.currentLocation {
            params["longitude"] = String(format: "%.6f", location.coordinate.longitude)
            params["latitude"] = String(format: "%.6f", location.coordinate.latitude)
        }
        
        showProgressHUD()
        ServerHelper.shared.callAPIForUploadImage(apiName: ApiEndpoint.SIGNUP, image: userPhoto, imageName: "photo", parameters: params, success: { (json) in
            self.hideProgressHUD()
            
            let user = User.init(json["user"])
            let token = json["token"].stringValue
            
            AppData.shared.userToken = token
            AppData.shared.currentUser = user
            AppData.shared.registerDevice()
            
            self.goNext()
        }) { (errMsg, errCode) in
            self.hideProgressHUD()
            self.showPromptAlert("Sorry!", message: errMsg);
        }
    }
    func callBraintree() -> Bool{
        var success = false
        braintreeClient = BTAPIClient(authorization: "sandbox_4x87qxmf_7zybkwtgqb26mwvz")!
        let payPalDriver = BTPayPalDriver(apiClient: braintreeClient!)
        payPalDriver.viewControllerPresentingDelegate = self
        payPalDriver.appSwitchDelegate = self // Optional

        // Specify the transaction amount here. "2.32" is used in this example.
        let request = BTPayPalRequest(amount: "2.32")
        request.currencyCode = "USD" // Optional; see BTPayPalRequest.h for more options

        payPalDriver.requestOneTimePayment(request) { (tokenizedPayPalAccount, error) in
            if let tokenizedPayPalAccount = tokenizedPayPalAccount {
                print("Got a nonce: \(tokenizedPayPalAccount.nonce)")

                // Access additional information
                let email = tokenizedPayPalAccount.email
                let firstName = tokenizedPayPalAccount.firstName
                let lastName = tokenizedPayPalAccount.lastName
                let phone = tokenizedPayPalAccount.phone

                // See BTPostalAddress.h for details
                let billingAddress = tokenizedPayPalAccount.billingAddress
                let shippingAddress = tokenizedPayPalAccount.shippingAddress
                success = true
            } else if let error = error {
                // Handle error here...
                print(error)
                success = false
            } else {
                success = false
                // Buyer canceled payment approval
            }
        }
        return success
    }
    func goNext(){
        
        let alert = UIAlertController(title: "Welcome", message: "You registered successfully!", preferredStyle: .alert)
        let ok = UIAlertAction(title: "OK", style: .default) { (action) in
            if let vc = AppMainVC.getInstance() {
                UserDefaults.standard.set(self.txfPassword.text, forKey: "password")
                if(!self.isProfessional){
                    if(self.callBraintree()){
                        self.pushVC(vc, animated: true)
                    }else{
                        let alert = UIAlertController(title: "ERROR", message: "Payment from PayPal is required in order to login", preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                        let vc2 = LoginVC.getInstance()
                        self.pushVC(vc2!, animated: true)
                    }
                }else{
                self.pushVC(vc, animated: true)
                }
            }
        }
        
        alert.addAction(ok)
        self.present(alert, animated: true, completion: nil)
    }
    
}

extension RegisterVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            igvPhoto.image = image
            userPhoto = image
        }
        
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
}

extension RegisterVC: AddQuestionVCDelegate {
    func didAddQuestion(_ question: String) {
        questions.append(question)
        
        if isSelectedAddQuestion1 {
            questionDropDown1.text = question
        }else{
            questionDropDown2.text = question
        }
        
    }
}

extension RegisterVC: SelectionVCDelegate {
    func selectionVC(_ type: SelectionType, selected: String) {
        if type == .rank {
            rankDropDown.text = selected
        }else if type == .question1 {
            questionDropDown1.text = selected
        }else if type == .question2 {
            questionDropDown2.text = selected
        }
    }
}

extension RegisterVC: BTAppSwitchDelegate, BTViewControllerPresentingDelegate{
    func appSwitcherWillPerformAppSwitch(_ appSwitcher: Any) {
        
    }
    
    func appSwitcher(_ appSwitcher: Any, didPerformSwitchTo target: BTAppSwitchTarget) {
        
    }
    
    func appSwitcherWillProcessPaymentInfo(_ appSwitcher: Any) {
        
    }
    
    func paymentDriver(_ driver: Any, requestsPresentationOf viewController: UIViewController) {
        
    }
    
    func paymentDriver(_ driver: Any, requestsDismissalOf viewController: UIViewController) {
        
    }
}
