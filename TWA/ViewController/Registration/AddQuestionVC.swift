//
//  AddQuestionVC.swift
//  TWA
//
//  Created by MascSoft on 7/1/19.
//  Copyright © 2019 Telecare. All rights reserved.
//

import UIKit

protocol AddQuestionVCDelegate: NSObject {
    func didAddQuestion(_ question: String)
}

class AddQuestionVC: UIViewController {

    weak var delegate: AddQuestionVCDelegate?
    
    @IBOutlet weak var txfQuestion: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func onAdd(_ sender: Any) {
        if Helper.isEmptyString(txfQuestion.text) {
            showPromptAlert("Sorry!", message: "Question is empty now.")
        }else{
            self.delegate?.didAddQuestion(txfQuestion.text!)
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func onCancel(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
