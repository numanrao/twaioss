//
//  ProfileVC.swift
//  TWA
//
//  Created by MascSoft on 7/1/19.
//  Copyright © 2019 Telecare. All rights reserved.
//

import UIKit

class ProfileVC: UIViewController {
    
    
    var user: User = AppData.shared.currentUser!
    var userPhoto: UIImage? = nil
    
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var txfUserID: UITextField!
    @IBOutlet weak var txfName: UITextField!
    
    @IBOutlet weak var btnMale: UIButton!
    @IBOutlet weak var btnFemale: UIButton!
    
    @IBOutlet weak var btnPhoto: RoundButton!
    @IBOutlet weak var igvPhoto: UIImageView!
    
    @IBOutlet weak var txfPassword: UITextField!
    @IBOutlet weak var txfConfirmPassword: UITextField!
    
    @IBOutlet weak var txfPhoneNumber: UITextField!
    
    @IBOutlet weak var txfEmail: UITextField!
    
    @IBOutlet weak var badgeView: UIView!
    @IBOutlet weak var txfBadge: UITextField!
    
    @IBOutlet weak var rankView: UIView!
    @IBOutlet weak var rankDropDown: UITextField!
    
    @IBOutlet weak var questionDropDown1: UITextField!
    @IBOutlet weak var txfAnswer1: UITextField!
    @IBOutlet weak var btnAddQuestion1: UIButton!
    
    @IBOutlet weak var questionDropDown2: UITextField!
    @IBOutlet weak var txfAnswer2: UITextField!
    @IBOutlet weak var btnAddQuestion2: UIButton!
    
    var isSelectedAddQuestion1: Bool = false
    
    var questions = AppData.shared.defaultQuestions
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initView()
//        self.navigationController?.navigationBar.setBackgroundImage(UIImage(named: "navbar"),
//        for: .default)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.view.endEditing(true)
    }
    
    func initView(){
        
        igvPhoto.layer.cornerRadius = igvPhoto.bounds.width / 2
        igvPhoto.clipsToBounds = true
        
        rankDropDown.placeholder = "Select your rank"
        questionDropDown1.placeholder = "Select question"
        questionDropDown2.placeholder = "Select question"
        
        igvPhoto?.sd_setImage(with: URL(string: user.photo), placeholderImage: AppImages.avatarPlaceholder, options: .refreshCached, context: nil)
        
        txfUserID.text = user.username
        txfName.text = user.name
        txfPhoneNumber.text = user.phoneNumber
        txfEmail.text = user.email
        
        if user.isProfessional {
            badgeView.isHidden = false
            rankView.isHidden = false
            userNameLabel.text = "Username"
            txfBadge.text = user.badgeNumber
            rankDropDown.text = user.rank
        }else {
            badgeView.isHidden = true
            rankView.isHidden = true
            userNameLabel.text = "License Plate #"
        }
        
        questionDropDown1.text = user.security["question1"] ?? ""
        txfAnswer1.text = user.security["answer1"] ?? ""
        
        questionDropDown2.text = user.security["question2"] ?? ""
        txfAnswer2.text = user.security["answer2"] ?? ""
    }
    
    //MARK: UIActions
    @IBAction func onBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onCancel(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onSubmit(_ sender: Any) {
        if checkValidation() {
            self.view.endEditing(true)
            callAPIUpdateUser()
        }
    }
    
    
    @IBAction func onMale(_ sender: Any) {
        btnMale.isSelected = true
        btnFemale.isSelected = false
    }
    @IBAction func onFemale(_ sender: Any) {
        btnMale.isSelected = false
        btnFemale.isSelected = true
    }
    
    @IBAction func onPhoto(_ sender: Any) {
        selectPhoto()
    }
    
    @IBAction func onRank(_ sender: Any) {
        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "SelectionVC") as? SelectionVC {
            vc.strTitle = "Select your rank"
            vc.options = ["5","4","3","2","1"]
            vc.delegate = self
            vc.selectionType = .rank
            
            self.pushVC(vc, animated: true)
        }
    }
    
    @IBAction func onQuestion1(_ sender: Any) {
        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "SelectionVC") as? SelectionVC {
            vc.strTitle = "Select question"
            vc.options = questions
            vc.delegate = self
            vc.selectionType = .question1
            
            self.pushVC(vc, animated: true)
        }
    }
    
    @IBAction func onQuestion2(_ sender: Any) {
        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "SelectionVC") as? SelectionVC {
            vc.strTitle = "Select question"
            vc.options = questions
            vc.delegate = self
            vc.selectionType = .question2
            
            self.pushVC(vc, animated: true)
        }
    }
    
    @IBAction func onAddQuestion(_ sender: UIButton) {
        
        if sender == btnAddQuestion1 {
            isSelectedAddQuestion1 = true
        }else{
            isSelectedAddQuestion1 = false
        }
        
        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddQuestionVC") as? AddQuestionVC {
            vc.delegate = self
            vc.modalTransitionStyle = .crossDissolve
            vc.modalPresentationStyle = .overCurrentContext
            
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    //MARK:
    func checkValidation() -> Bool {
        
        var errMsg: String? = nil
        
//        else if Helper.isEmptyString(txfPassword.text) {
//            errMsg = "Password is empty"
//        }else if !Helper.isValidPassword(txfPassword.text!) {
//            errMsg = "Password is Invalid"

        if(txfPassword.text != txfConfirmPassword.text){
            errMsg = "Password fields don't match"
        }
        if Helper.isEmptyString(txfName.text) {
            errMsg = "Name is empty"
        }else if Helper.isEmptyString(txfPhoneNumber.text) {
            errMsg = "Phone number is empty"
        }else if Helper.isEmptyString(txfEmail.text) {
            errMsg = "Email is empty"
        }else if !Helper.isValidEmailAddress(txfEmail.text!) {
            errMsg = "Email is Invalid"
        }else if Helper.isEmptyString(questionDropDown1.text) {
            errMsg = "Please set up security Question1"
        }else if Helper.isEmptyString(txfAnswer1.text) {
            errMsg = "Please set up security Answer1"
        }else if Helper.isEmptyString(questionDropDown2.text) {
            errMsg = "Please set up security Question2"
        }else if Helper.isEmptyString(txfAnswer2.text) {
            errMsg = "Please set up security Answer2"
        }
        
        if errMsg != nil && user.isProfessional {
            if Helper.isEmptyString(txfBadge.text) {
                errMsg = "Badge number is empty"
            }else if Helper.isEmptyString(rankDropDown.text) {
                errMsg = "Rank is empty"
            }
        }
        
        
        if let str = errMsg {
            showPromptAlert("Sorry,", message: str)
            return false
        }
        
        return true
    }
    
    //MARK : Select User Photo
    func selectPhoto() {
        
        let alert = UIAlertController(title: "Select User Photo", message: "", preferredStyle: .actionSheet)
        
        let photos = UIAlertAction(title: "Photo Gallery", style: .default) { (action) in
            self.openPhotoGallery()
        }
        
        let camera = UIAlertAction(title: "Camera", style: .default) { (action) in
            self.openCamera()
        }
        
        let cancel = UIAlertAction(title: "Cancel", style: .destructive, handler: nil)
        
        alert.addAction(photos)
        alert.addAction(camera)
        alert.addAction(cancel)
        
        alert.popoverPresentationController?.sourceView = self.btnPhoto
        
        self.present(alert, animated: true, completion: nil)
    }
    
    let imagePicker = UIImagePickerController()
    
    func openCamera(){
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            imagePicker.delegate = self
            imagePicker.allowsEditing = false
            imagePicker.sourceType = .camera
            
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func openPhotoGallery(){
        imagePicker.delegate = self
        imagePicker.allowsEditing = true
        imagePicker.sourceType = .photoLibrary
        
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    func callAPIUpdateUser(){
        var params: [String : String] = [
                                         "name": txfName.text!,
                                         "phoneNumber": txfPhoneNumber.text!,
                                         "email": txfEmail.text!,
                                         "question1": questionDropDown1.text!,
                                         "answer1": txfAnswer1.text!,
                                         "question2": questionDropDown2.text!,
                                         "answer2": txfAnswer2.text!,
                                         "password": txfPassword.text!
        ];
        
        if user.isProfessional {
            params["badgeNumber"] = txfBadge.text!
            params["rank"] = rankDropDown.text!
        }
        
        if let location = LocationHelper.shared.currentLocation {
            params["longitude"] = String(format: "%.6f", location.coordinate.longitude)
            params["latitude"] = String(format: "%.6f", location.coordinate.latitude)
        }
        
        showProgressHUD()
        ServerHelper.shared.callAPIForUploadImage(apiName: ApiEndpoint.USER_UPDATE, image: userPhoto, imageName: "photo", parameters: params, success: { (json) in
            self.hideProgressHUD()
            
            let user = User.init(json["user"])
            AppData.shared.currentUser = user
            
            self.goNext()
        }) { (errMsg, errCode) in
            self.hideProgressHUD()
            self.showPromptAlert("Sorry!", message: errMsg);
        }
    }
    
    func goNext(){
        
        let alert = UIAlertController(title: "Welcome", message: "You updated successfully!", preferredStyle: .alert)
        let ok = UIAlertAction(title: "OK", style: .default) { (action) in
            self.popVC(true)
        }
        
        alert.addAction(ok)
        self.present(alert, animated: true, completion: nil)
    }
    
}

extension ProfileVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            igvPhoto.image = image
            userPhoto = image
        }
        
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
}

extension ProfileVC: AddQuestionVCDelegate {
    func didAddQuestion(_ question: String) {
        questions.append(question)
        
        if isSelectedAddQuestion1 {
            questionDropDown1.text = question
        }else{
            questionDropDown2.text = question
        }
        
    }
}

extension ProfileVC: SelectionVCDelegate {
    func selectionVC(_ type: SelectionType, selected: String) {
        if type == .rank {
            rankDropDown.text = selected
        }else if type == .question1 {
            questionDropDown1.text = selected
        }else if type == .question2 {
            questionDropDown2.text = selected
        }
    }
}

