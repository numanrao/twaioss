//
//  SettingsVC.swift
//  TWA
//
//  Created by MascSoft on 7/1/19.
//  Copyright © 2019 Telecare. All rights reserved.
//

import UIKit

class SettingsVC: UIViewController {

    
    @IBOutlet weak var viewAutoGreeting: UIView!
    @IBOutlet weak var swAutoRecording: UISwitch!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if AppData.shared.isProfessionalMode() {
            viewAutoGreeting.isHidden = false
        }else{
            viewAutoGreeting.isHidden = true
        }
        
        if let user = AppData.shared.currentUser {
            swAutoRecording.isOn = user.autoRecording
        }else{
            swAutoRecording.isOn = false
        }
//        self.navigationController?.navigationBar.setBackgroundImage(UIImage(named: "navbar"),
//                                                                    for: .default)
    }
    
    
    @IBAction func onProfile(_ sender: Any) {
        
    }
    
    @IBAction func onGreeting(_ sender: Any) {
    }
    
    @IBAction func onAutoRecording(_ sender: Any) {
        if swAutoRecording.isOn {
            callAPIUpdateSetting()
        }else{
            swAutoRecording.isOn = true
            showAlertForAutoRecording()
        }
    }
    
    @IBAction func onLogout(_ sender: Any) {
        AppData.shared.logout()
    }

    func showAlertForAutoRecording(){
        
        let alertVC = UIAlertController.init(title: "Alert!", message: "We strongly object to your setting this option! Will you off auto recording function anyway?", preferredStyle: .alert)
        
        let yes = UIAlertAction.init(title: "Yes", style: .default) { (action) in
            
            self.swAutoRecording.isOn = false
            self.callAPIUpdateSetting()
        }
        
        let no = UIAlertAction.init(title: "No", style: .default) { (action) in
            self.swAutoRecording.isOn = true
        }
        
        alertVC.addAction(yes)
        alertVC.addAction(no)
        
        present(alertVC, animated: true, completion: nil)
    }
    
    func callAPIUpdateSetting(){
        let params: [String : String] = [
            "autoRecording": String(swAutoRecording.isOn)
        ];
        
        //showProgressHUD()
        ServerHelper.shared.callPostAPI(ApiEndpoint.USER_UPDATE, parameters: params, success: { (json) in
            //self.hideProgressHUD()
            
            let user = User.init(json["user"])
            AppData.shared.currentUser?.autoRecording = user.autoRecording
            AppData.shared.saveCurrentUser()
            
        }) { (errMsg, errCode) in
            //self.hideProgressHUD()
            //self.showPromptAlert("Sorry!", message: errMsg);
        }
    }
}
