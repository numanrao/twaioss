//
//  AutoGreetingVC.swift
//  TWA
//
//  Created by MascSoft on 7/1/19.
//  Copyright © 2019 Telecare. All rights reserved.
//

import UIKit

class AutoGreetingVC: UIViewController {

    let user: User? = AppData.shared.currentUser
    
    @IBOutlet weak var txtGreeting: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        txtGreeting.text = user?.greeting
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(named: "navbar"),
        for: .default)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.view.endEditing(true)
    }
    
    @IBAction func onBack(_ sender: Any) {
        self.popVC(true)
    }
    
    @IBAction func onSubmit(_ sender: Any) {
        if let strGreeting = txtGreeting.text, strGreeting.isEmpty == false {
            self.view.endEditing(true)
            callAPIUpdateGreeting(strGreeting)
        }
    }
    
    func callAPIUpdateGreeting(_ greeting: String){
        let params: [String : String] = [
            "greeting": greeting
        ];
        
        showProgressHUD()
        ServerHelper.shared.callPostAPI(ApiEndpoint.USER_UPDATE, parameters: params, success: { (json) in
            self.hideProgressHUD()
            
            let user = User.init(json["user"])
            AppData.shared.currentUser = user
            
            self.showPromptAlert("Success!", message: "Submitted auto greeting");
        }) { (errMsg, errCode) in
            self.hideProgressHUD()
            self.showPromptAlert("Sorry!", message: errMsg);
        }
    }
 
    func goNext(){
        self.popVC(true)
    }
}
