//
//  AppMainVC.swift
//  TWA
//
//  Created by MascSoft on 7/22/19.
//  Copyright © 2019 Telecare. All rights reserved.
//

import UIKit
import LFLiveKit
import Digger
import SVProgressHUD
import Photos

class AppMainVC: UIViewController, UITabBarControllerDelegate {

    static func getInstance() -> AppMainVC? {
        let storyboard = UIStoryboard.init(name: "Main", bundle: Bundle.main)
        return storyboard.instantiateViewController(withIdentifier: "AppMainVC") as? AppMainVC
    }
    
    // My Recording View
    @IBOutlet weak var recordingView: UIView!
    @IBOutlet weak var livePreview: UIView!
    //@IBOutlet weak var cameraView: AACameraView!
    
    // Start & Stop
    @IBOutlet weak var startView: UIView!
    @IBOutlet weak var stopView: UIView!
    
    weak var txfPassword: UITextField?
    var liveSession: LFLiveSession?
    
    var isVisibleHomePage: Bool = false
    var isRecordingMode: Bool = false
    var localRecordingURL: URL?
    var countGlobal = 0
    var voluntary:Bool = true
    var time = 3600
    var count = 0
    var currentUser = AppData.shared.currentUser
    // Main Tabbar controller
    var mainTabBarVC: UITabBarController?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // initCameraView()
        initLiveSession()
        
        isVisibleHomePage = true
        hideStartRec(false)
                
        if let user = AppData.shared.currentUser, user.autoRecording == true {
           callAPIStartRecording()
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
            appDelegate.handleRemoteMessage()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "embedMainTabBarVC" {
            self.mainTabBarVC = segue.destination as? UITabBarController
            self.mainTabBarVC?.delegate = self
        }
    }
    
    //MARKL: InitView
    
    func initLiveSession(){
        let audioConfig = LFLiveAudioConfiguration.defaultConfiguration(for: .medium)
        let videoConfig = LFLiveVideoConfiguration.defaultConfiguration(for: .medium1)
        
        liveSession = LFLiveSession(audioConfiguration: audioConfig, videoConfiguration: videoConfig)
        liveSession?.delegate = self
        liveSession?.captureDevicePosition = .front
        liveSession?.preView = livePreview
        
        localRecordingURL = Helper.getLocalRecordingVideoURL()
        liveSession?.saveLocalVideoPath = localRecordingURL
        liveSession?.saveLocalVideo = true
        
    }
    
//    func initCameraView(){
//        cameraView.cameraPosition = .back
//        cameraView.zoomEnabled = false
//        cameraView.focusEnabled = false
//        cameraView.flashMode = .off
//        cameraView.outputMode = .video
//        cameraView.quality = .medium
//        
//        cameraView.startSession()
//    }
    
    //MARK: UITabBarController delegate
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        
        if tabBarController.selectedIndex == 1 { // Home VC
            isVisibleHomePage = true
        }else {
            isVisibleHomePage = false
        }
        
        updateRecordingView()
    }
    
    // MARK: User Actions
    @IBAction func onStart(_ sender: Any) {
        callAPIStartRecording()
    }
    
    @IBAction func onStop(_ sender: Any) {
        let alert = UIAlertController(title: "Alert", message: "Please input your password to stop recording.", preferredStyle: .alert)
        alert.addTextField { (textField) in
            textField.isSecureTextEntry = true
            textField.placeholder = "password"
            
            self.txfPassword = textField
        }
        
        let ok = UIAlertAction(title: "Ok", style: .default) { (action) in
            self.callAPIStopRecording(true, password: self.txfPassword?.text)
        }
        
        let cancel = UIAlertAction(title: "Cancel", style: .default, handler: nil)
        
        alert.addAction(cancel)
        alert.addAction(ok)
        
        self.present(alert, animated: true, completion: nil)
    }
    
    
    // Functions
    func updateRecordingView() {
        if(isVisibleHomePage  && isRecordingMode ) {
            recordingView.isHidden = false
        }else{
            recordingView.isHidden = true
        }
    }
    
    func hideStartRec(_ flag: Bool) {
        
        isRecordingMode = flag
        
        if flag {
            startView.isHidden = true
            stopView.isHidden = false
        }else {
            startView.isHidden = false
            stopView.isHidden = true
        }
        
        updateRecordingView()
    }
    
    func startLiveBroadcast(_ recordingId: String) {
        let streamInfo = LFLiveStreamInfo.init()
        streamInfo.streamId = recordingId
        if(currentUser!.isProfessional){
        streamInfo.url = AppInfo.RtmpServerLiveURLLEO + recordingId
        }else{
            streamInfo.url = AppInfo.RtmpServerLiveURLCITIZEN + recordingId
        }
        
        removeLocalRecordingVideo() //// Remove Previous Recording File
        
        liveSession?.running = true
        liveSession?.startLive(streamInfo)
    }
    
    
    func stopLiveBroadcast() {
        
        liveSession?.stopLive()
        
        liveSession?.running = false
        liveSession?.saveLocalVideo = false
    }
    
    func removeLocalRecordingVideo() {
        if let url = localRecordingURL {
            let manager = FileManager.default
            if manager.fileExists(atPath: url.path) {
                do{
                    try manager.removeItem(at: url)
                }catch let err {
                    print(err.localizedDescription)
                }
            }
        }
    }
    
    //MARK: Show Download alert after you stop recording
    func showDownloadAlert(_ recordingId: String?){
        let alert = UIAlertController(title: "Alert", message: "Do you want to download recording to your device?", preferredStyle: .alert)

        let ok = UIAlertAction(title: "Yes", style: .default) { (action) in
            self.saveLocalRecordingToCameraRoll()
        }
        
        let cancel = UIAlertAction(title: "No", style: .default) { (action) in
            self.showUploadingSuccessAlert(recordingId)
        }
        
        alert.addAction(cancel)
        alert.addAction(ok)
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func saveLocalRecordingToCameraRoll() {
        if let url = localRecordingURL {
            if FileManager.default.fileExists(atPath: url.path) {
                SVProgressHUD.show(withStatus: "Downloading...")
                DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                    self.saveVideoToCameraRoll(url)
                }
                return
            }
        }
        
        self.showPromptAlert("Alert", message: "Downloading the recording was failed.")
    }
    
    
//    //MARK: Downloading Remote Recording Video from recording Id and Save it to Camera Roll
//    func downloadRecording(_ recordingId: String?) {
//        guard let idStr = recordingId else {
//            return
//        }
//
//        let videoUrl = AppInfo.RtmpServerHttpURL + "\(idStr).mp4"
//
//
//        download(videoUrl).progress({ (progress) in
//            SVProgressHUD.showProgress(Float(progress.fractionCompleted), status: "Downloading...")
//        }).speed({ (speed) in
//            print(speed)
//        }).completion { [weak self] (result) in
//
//            DispatchQueue.main.async {
//                switch result {
//                case .success(let url) :
//                    self?.saveVideoToCameraRoll(url)
//                case .failure(let err):
//                    self?.showPromptAlert("Alert", message: "Downloading was failed.")
//                    SVProgressHUD.dismiss()
//                    print("failure")
//                }
//            }
//        }
//    }
    
    func saveVideoToCameraRoll(_ videoUrl: URL) {
        PHPhotoLibrary.shared().performChanges({
            PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: videoUrl)
        }) { (succes, err) in
            
            DispatchQueue.main.async {
                if(succes) {
                    self.showDownloadingSuccessAlert()
                }else{
                    self.showPromptAlert("Alert", message: "Saving recording was failed.")
                }
                SVProgressHUD.dismiss()
                DiggerCache.cleanDownloadFiles()
            }
        }
    }
    
    func showUploadingSuccessAlert(_ recordingId: String?) {
        
        if let idStr = recordingId {
            
            let alert = UIAlertController(title: "Alert", message: "Your recording was uploaded to TWA server. The recording ID is \(idStr). You can browse the detail on RECORDINGS page.", preferredStyle: .alert)
            
            let ok = UIAlertAction(title: "OK", style: .default) { (action) in
                self.gotoRecordingsPage()
            }
            
            alert.addAction(ok)
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func showDownloadingSuccessAlert() {
        
        let alert = UIAlertController(title: "Alert", message: "The recording was successful downloaded to Camera Roll. You can browse the detail on RECORDINGS page.", preferredStyle: .alert)
        
        let ok = UIAlertAction(title: "OK", style: .default) { (action) in
            self.gotoRecordingsPage()
        }
        
        alert.addAction(ok)
        self.present(alert, animated: true, completion: nil)
    }
    
    func gotoRecordingsPage(){
        self.mainTabBarVC?.selectedIndex = 0
    }
}

// MARK: LFLiveSession Delegate
extension AppMainVC: LFLiveSessionDelegate {

    func liveSession(_ session: LFLiveSession?, liveStateDidChange state: LFLiveState) {
        var msg = ""
        
        switch state {
        case .error:
            msg = "Live Streaming Error"
        case .pending:
            msg = "Live Streaming Pending"
        case .ready:
            msg = "Live Streaming Ready"
        case .start:
            msg = "Live Streaming Start"
        case .stop:
            msg = "Live Streaming Stop"
        default:
            msg = ""
        }
        
        print(msg)
    }
    
    func liveSession(_ session: LFLiveSession?, debugInfo: LFLiveDebug?) {
        
    }
    
    func liveSession(_ session: LFLiveSession?, errorCode: LFLiveSocketErrorCode) {
        
    }
}

extension AppMainVC {
    
    
    @objc func update() {
        if(time > 0) {
            time -= 1
        }else{
            var password = UserDefaults.standard.value(forKey: "password") as! String
            voluntary = false
            callAPIStopRecording(false, password: password)
        }
    }
    func callAPIStartRecording() {
        
        
        var params: [String : Any] = [:]
        if let location = LocationHelper.shared.currentLocation {
            params["longitude"] = location.coordinate.longitude
            params["latitude"] = location.coordinate.latitude
        }
        
        
        
        params["username"] = currentUser?.username
        params["precinct"] = currentUser?.precinct
        if(currentUser!.isProfessional){
                
                params["recording_server"] = "http://66.185.23.114/videos/leo/"
                
        }else{
                params["recording_server"] = "http://66.185.23.114/videos/citizen/"
            
        }
        ServerHelper.shared.callPostAPI(ApiEndpoint.RECORDING_START, parameters: params, success: { [weak self] (json) in
            
            self!.time = 3600
            var timer = Timer.scheduledTimer(timeInterval: 0.4, target: self, selector: #selector(AppMainVC.update), userInfo: nil, repeats: true)
            if let recordingId = json.string, recordingId.isEmpty == false {
                AppData.shared.recordingId = recordingId
                
                self?.hideStartRec(true)
                self?.startLiveBroadcast(recordingId)
            }else{
                self?.hideStartRec(false)
                self?.showPromptAlert("Sorry!", message: "Can't start recording. Try again later!");
            }
        }) { [weak self] (errMsg, errCode) in
            self?.hideStartRec(false)
            self?.showPromptAlert("Sorry!", message: "Can't start recording. Try again later!");
        }
    }
    
    func callAPIStopRecording(_ isByUser: Bool, password: String?) {
        
        guard let recordingId = AppData.shared.recordingId else {
            return
        }
        
        var params: [String: Any] = [
            "isByUser": isByUser,
            "recordingId": recordingId
        ]
        
        if isByUser {
            if let temp = password, temp.isEmpty == false {
                params["password"] = temp
            }else{
                //self.showPromptAlert("Alert", message: "It is missing password")
                return
            }
        }
        if(currentUser!.isProfessional){
            if(voluntary){
                params["recording_server"] = "http://66.185.23.114/videos/leo/"
                params["isByUser"] = true
            }else{
                params["recording_server"] = "http://66.185.23.114/videos/leo/"
                params["isByUser"] = false
            }
        }else{
            if(voluntary){
                params["recording_server"] = "http://66.185.23.114/videos/citizen/"
                params["isByUser"] = true
            }else{
                params["recording_server"] = "http://66.185.23.114/videos/citizen/"
                params["isByUser"] = false
            }
        }
        ServerHelper.shared.callPostAPI(ApiEndpoint.RECORDING_STOP, parameters: params, success: { [weak self] (json) in
            
            self?.hideStartRec(false)
            self?.stopLiveBroadcast()
            self?.showDownloadAlert(AppData.shared.recordingId)
            self!.count = 0
            self!.voluntary = true
            NotificationCenter.default.post(name: .AppRefreshRecordingList, object: nil)
            
            AppData.shared.recordingId = nil
        }) { [weak self] (errMsg, errCode) in
            
            if case ErrorStatusCode.internalServer(let code) = errCode, code == 403 {
                self!.count += 1
                if(self!.count == 3){
                    self!.count = 0
                    self?.voluntary = false
                    let password = UserDefaults.value(forKey: "password") as! String
                    self!.callAPIStopRecording(false, password: password)
                }
                self?.showPromptAlert("Sorry!", message: errMsg);
            }else{
                self?.showPromptAlert("Sorry!", message: "Can't stop recording. Try again later!");
            }
        }
    }
    
}
