

//---------------------------------------------------------------------------------
#define		VIDEO_LENGTH						5
#define		INSERT_MESSAGES						10
#define		DOWNLOAD_TIMEOUT					300
//---------------------------------------------------------------------------------
#define		STATUS_LOADING						1
#define		STATUS_SUCCEED						2
#define		STATUS_MANUAL						3
//---------------------------------------------------------------------------------
#define		MEDIA_IMAGE							1
#define		MEDIA_VIDEO							2
#define		MEDIA_AUDIO							3
//---------------------------------------------------------------------------------
#define		NETWORK_MANUAL						1
#define		NETWORK_WIFI						2
#define		NETWORK_ALL							3
//---------------------------------------------------------------------------------
#define		KEEPMEDIA_WEEK						1
#define		KEEPMEDIA_MONTH						2
#define		KEEPMEDIA_FOREVER					3
//---------------------------------------------------------------------------------
#define		CALLHISTORY_AUDIO					@"audio"
#define		CALLHISTORY_VIDEO					@"video"
//---------------------------------------------------------------------------------
#define		MESSAGE_STATUS						@"status"
#define		MESSAGE_TEXT						@"text"
#define		MESSAGE_EMOJI						@"emoji"
#define		MESSAGE_PICTURE						@"picture"
#define		MESSAGE_VIDEO						@"video"
#define		MESSAGE_AUDIO						@"audio"
#define		MESSAGE_LOCATION					@"location"

//---------------------------------------------------------------------------------
#define		TEXT_QUEUED							@"Queued"
#define		TEXT_SENT							@"Sent"
#define		TEXT_READ							@"Read"

//---------------------------------------------------------------------------------
#define		NOTIFICATION_APP_STARTED			@"NotificationAppStarted"
#define		NOTIFICATION_USER_LOGGED_IN			@"NotificationUserLoggedIn"
#define		NOTIFICATION_USER_LOGGED_OUT		@"NotificationUserLoggedOut"
//---------------------------------------------------------------------------------

#define		NOTIFICATION_REFRESH_CHATS			@"NotificationRefreshChats"
#define		NOTIFICATION_REFRESH_MESSAGES1		@"NotificationRefreshMessages1"
#define		NOTIFICATION_REFRESH_MESSAGES2		@"NotificationRefreshMessages2"
#define		NOTIFICATION_REFRESH_STATUSES		@"NotificationRefreshStatuses"
#define		NOTIFICATION_REFRESH_USERS			@"NotificationRefreshUsers"
//---------------------------------------------------------------------------------
#define		NOTIFICATION_CLEANUP_CHATVIEW		@"NotificationCleanupChatView"

#define     NOTIFICATION_RECEIVE_MESSAGE        @""
#define     NOTIFICATION_RECEIVE_GROUP_MESSAGE  @"NotificationReceiveNewGroupMessage"

//-------------------------------------------------------------------------------------------------------------------------------------------------

#define        RC_TYPE_STATUS                      1
#define        RC_TYPE_TEXT                        2
#define        RC_TYPE_EMOJI                       3
#define        RC_TYPE_PICTURE                     4
#define        RC_TYPE_VIDEO                       5
#define        RC_TYPE_AUDIO                       6
#define        RC_TYPE_LOCATION                    7
//---------------------------------------------------------------------------------
#define        RC_STATUS_LOADING                   1
#define        RC_STATUS_SUCCEED                   2
#define        RC_STATUS_MANUAL                    3
//---------------------------------------------------------------------------------
#define        RC_AUDIOSTATUS_STOPPED              1
#define        RC_AUDIOSTATUS_PLAYING              2
