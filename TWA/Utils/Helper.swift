 //
//  Helper.swift
 //  TWA
 //
 //  Created by MascSoft on 7/1/19.
 //  Copyright © 2019 Telecare. All rights reserved.
 //

import Foundation
import UIKit

import SystemConfiguration

 
 public class Reach {
    
    static func isInternetAvailable() -> Bool  {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        return (isReachable && !needsConnection)
    }
    
 }
 

 class Helper : NSObject {
    
    class func showAlertDialog(_ viewController:UIViewController, message:String, title: String, clickAction:@escaping ()->() ) {
        
		let alertView = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alertView.message = message.isEmpty ? nil : message
        alertView.title = title.isEmpty ? nil : title
		alertView.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action: UIAlertAction!) in
			clickAction()
		}))
		viewController.present(alertView, animated: true, completion: nil)
    }
    
    //MARK:- Custom methods
    
    class func showActivityIndicator(_ activityIndicator:UIActivityIndicatorView){
        activityIndicator.isHidden = false
        activityIndicator.startAnimating()
    }
    
    class func hideActivityIndicator(_ activityIndicator:UIActivityIndicatorView){
        activityIndicator.isHidden = true
        activityIndicator.stopAnimating()
    }
    
    class func addActivityView() -> UIActivityIndicatorView{
        var pagingSpinner:UIActivityIndicatorView?
        pagingSpinner = UIActivityIndicatorView(style: .gray)
        pagingSpinner!.color = UIColor.darkGray
        pagingSpinner!.hidesWhenStopped = true
        return pagingSpinner!
    }
    
    class func removeNavigationBorder(_ nav:UINavigationController,searchBar:UISearchBar){
        for parent in (nav.navigationBar.subviews){
            for child in parent.subviews{
                if(child is UIImageView){
                    child.removeFromSuperview()
                }
            }}
            searchBar.isTranslucent = false
            searchBar.backgroundImage = UIImage()
    }
    
 	//MARK:- Load URl on safari
    
 	class func loadURLOnSafari(_ url:String) {
		
        var linkUrl = url
        if !linkUrl.hasPrefix("https://") && !linkUrl.hasPrefix("http://"){
            linkUrl = "http://" + url
        }
        
        guard let loadURL = URL(string: linkUrl) else {
            return //be safe
        }
        
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(loadURL, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(loadURL)
        }
	}
	
	//MARK:- Load URL
	
	class func loadURL(url:String , webview:UIWebView) {
        guard let loadURL = URL(string: url) else {
            return
        }
        
		let request = URLRequest(url: loadURL)
		webview.loadRequest(request)
	}
    
    //MARK:- validation methods
    
    //Validate email
    
    class func isValidEmailAddress(_ email: String)->Bool {
        let emailRegEx =  "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: email)
    }
    
    class func isValidPassword(_ password: String)-> Bool {
        //"^(?=.*[a-z])(?=.*\\d)(?=.*[^a-zA-Z])((?=.*?[#?!@$%^&*-]).{5,30})$" "(?=.*[A-Z])(?=.*[0-9])(?=.*[a-z]).{8,}"
        let passwordRegEx = "^(?=.*[A-Z])(?=.*[0-9])(?=.*[a-z]).{6,}$"
        let passwordTest = NSPredicate(format:"SELF MATCHES %@", passwordRegEx)
        return passwordTest.evaluate(with: password)
        
    }
    
    class func isValidUserName(_ username: String)-> Bool {
        // username is 3-15 characters long
        // no _ or . at the beginning
        // no __ or _. or ._ or .. inside
        // allowed characters
        //no _ or . at the end
        //"^(?=.{6,15}$)(?![_.])(?!.*[_.]{2})[a-z0-9._]+(?<![_.])$"
        //"^(?=\\S{8})[a-zA-Z]\\w*(?:\\.\\w+)*(?:@\\w+\\.\\w{2,4})?$"
        let userRegEx =   "^[a-zA-Z0-9_]{4,30}$"
        let userName = NSPredicate(format:"SELF MATCHES %@", userRegEx)
        return userName.evaluate(with: username)
    }
    
    static func isValidURL(_ url: String) -> Bool {
        
        //        let regex = "((?:http|https)://)?(?:www\\.)?[\\w\\d\\-_]+\\.\\w{2,3}(\\.\\w{2})?(/(?<=/)(?:[\\w\\d\\-./_]+)?)?"
        let regex = "^(http://www\\.|Http://www\\.|https://www\\.|Https://www\\.|http://|Http://|https://|Https://)[a-z0-9]+([\\-\\.]{1}[a-z0-9]+)*\\.[a-z]{2,5}(:[0-9]{1,5})?(/.*)?$"
        
        let URLTest = NSPredicate(format:"SELF MATCHES %@", regex)
        return URLTest.evaluate(with: url)
    }
    
    class func isEmptyString(_ str: String?) -> Bool {
        if let temp = str, temp.isEmpty == false {
            return false
        }
        return true
    }
    
    
    // MARK: - DeviceDimension Method
    
    class func getDeviceDimensions()->(CGFloat, CGFloat) {
        let screenSize: CGRect = UIScreen.main.bounds
        
        let screenWidth = screenSize.width
        let screenHeight = screenSize.height
        return (screenWidth, screenHeight)
    }
    
    // MARK: - Animation Method
    
    class func setPopUpAnimationTimeZero(_ viewController:UIViewController) {
        
        UIView.animate(withDuration: 0.0, animations: {
            viewController.view.center = CGPoint(x: viewController.view.center.x + (viewController.view.center.x * 2 ), y: viewController.view.center.y)
            
        }, completion: { (finished) in
            
            viewController.removeFromParent()
            viewController.view.removeFromSuperview()
            
        }) 
        
    }
    
    class func setPopUpAnimation(_ viewController:UIViewController) {
        
        UIView.animate(withDuration: 0.3, animations: {
            viewController.view.center = CGPoint(x: viewController.view.center.x + (viewController.view.center.x * 2 ), y: viewController.view.center.y)
            
        }, completion: { (finished) in
            
            viewController.removeFromParent()
            viewController.view.removeFromSuperview()
            
        }) 
        
    }
    
    class func setPresentAnimation(_ view: UIView) {
        let deviceWidth = Helper.getDeviceDimensions().0
        let deviceHeight = Helper.getDeviceDimensions().1
        
        view.frame = CGRect(x: deviceWidth, y: 0, width: deviceWidth, height: deviceHeight)
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseIn, animations: {
            view.center = CGPoint(x: deviceWidth/2, y: deviceHeight/2)
            }, completion: { finished in
                return
        })
    }
    
    class func timestampToDateString(timestamp: Int64) -> String {
        
        let date = Date.date(timestamp: timestamp)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy HH:mm:ss"
        
        return dateFormatter.string(from: date)
    }
    
    class func getDocumentPath() -> String {
        let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0];
        
        return documentsPath
    }
    
    class func getLocalRecordingVideoURL() -> URL? {
        let documentPath = Helper.getDocumentPath()
        let videoPath = documentPath + "/temp_recording.mp4"
        return URL(fileURLWithPath: videoPath)
    }
    
}

 public struct PopupHelper {
    func showPopup(_ popup: UIView, inView superView: UIView){
        
        popup.alpha = 0.0
        superView.addSubview(popup)
        
        popup.layer.shouldRasterize = true;
        popup.layer.rasterizationScale = UIScreen.main.scale
        
        UIView.animate(withDuration: 0.25, animations: {
            popup.alpha = 1.0
        }, completion: {
            (value: Bool) in
            popup.layer.shouldRasterize = false
        })
        popup.layer.add(self.popupAnimation(), forKey: "popup")
    }
    
    func hidePopup(_ popup: UIView){
        popup.layer.shouldRasterize = true;
        popup.layer.rasterizationScale = UIScreen.main.scale
        
        UIView.animate(withDuration: 0.25, animations: {
            popup.alpha = 0.0
        }, completion: {
            (value: Bool) in
            popup.layer.shouldRasterize = false
            popup.removeFromSuperview()
        })
    }
    
    private func popupAnimation() -> CAKeyframeAnimation {
        
        let animation: CAKeyframeAnimation = CAKeyframeAnimation.init(keyPath: "transform")
        
        let scale1: CATransform3D = CATransform3DMakeScale(0.0, 0.0, 1);
        let scale2: CATransform3D = CATransform3DMakeScale(1.1, 1.1, 1);
        let scale3: CATransform3D = CATransform3DMakeScale(0.9, 0.9, 1);
        let scale4: CATransform3D = CATransform3DMakeScale(1.0, 1.0, 1);
        
        let frameValues = NSArray.init(array: [NSValue(caTransform3D:scale1),NSValue(caTransform3D:scale2),NSValue(caTransform3D:scale3),NSValue(caTransform3D:scale4)])
        animation.values = frameValues as? [Any]
        
        let frameTimes = NSArray.init(array: [NSNumber.init(value: 0.0),NSNumber.init(value: 0.5),NSNumber.init(value: 0.75),NSNumber.init(value: 1.0)])
        animation.keyTimes = frameTimes as? [NSNumber]
        animation.fillMode = CAMediaTimingFillMode.forwards;
        animation.isRemovedOnCompletion = false;
        animation.duration = 0.4;
        return animation;
    }
 }
 
 public func getCurrentViewController()-> UIViewController?{
    var topVC = UIApplication.shared.keyWindow?.rootViewController
    while ((topVC?.presentedViewController) != nil) {
        topVC = topVC?.presentedViewController
    }
    
    return topVC
 }
