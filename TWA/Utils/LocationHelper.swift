//
//  LocationHelper.swift
//  Benchmrk
//
//  Created by Star on 6/4/18.
//  Copyright © 2018 Benchmrk.com.au. All rights reserved.
//

import UIKit
import CoreLocation

class LocationHelper: NSObject, CLLocationManagerDelegate {

    static let shared = LocationHelper()
    
    let settingsTitle: String = "You can accept the permission via Settings > Location Accessees"
    var currentLocation:CLLocation?
    var locationManager: CLLocationManager!
    
    override init() {
        super.init()
        setupLocationManager()
    }
    
    func enable(){
        
    }
    
    private func setupLocationManager(){
        
        if locationManager == nil {
            locationManager = CLLocationManager()
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.distanceFilter = kCLDistanceFilterNone
            locationManager.delegate = self
            
            locationManager.requestWhenInUseAuthorization()
        }
    }
    
    //---------------------------------------------------------------------------------------------------
    // MARK:- CLLocationManagerDelegate Methods
    //---------------------------------------------------------------------------------------------------
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        
        switch status {
        case .notDetermined: fallthrough
        case .denied: fallthrough
        case .restricted:
            manager.requestWhenInUseAuthorization()
        case .authorizedAlways: fallthrough
        case .authorizedWhenInUse:
            manager.requestLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        let status = CLLocationManager.authorizationStatus()
        if status == .denied || status == .notDetermined || status == .restricted {
            openSettingIfDontAllowClick()
        } else if status == .authorizedAlways ||  status == .authorizedWhenInUse {
            manager.requestLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if locations.count > 0 {
            //manager.stopUpdatingLocation()
            
            guard let _userLocation = locations.last else {
                return
            }
            currentLocation = _userLocation
            AppData.shared.updateCurrentUserLocation(_userLocation)
        }
    }
    
    func openSettingIfDontAllowClick() {
        
        let alertController = UIAlertController (title: "", message: settingsTitle, preferredStyle: .alert)
        let settingsAction = UIAlertAction(title: "Settings", style: .default) { (_) -> Void in
            guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                return
            }
            
            if UIApplication.shared.canOpenURL(settingsUrl) {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                        //print("Settings opened: \(success)") // Prints true
                    })
                } else {
                    UIApplication.shared.openURL(settingsUrl as URL)
                }
            }
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: { (_)-> Void in
            
        })
        alertController.addAction(cancelAction)
        alertController.addAction(settingsAction)
        
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        appDelegate?.window?.rootViewController?.present(alertController, animated: true, completion: nil)
    }
}
